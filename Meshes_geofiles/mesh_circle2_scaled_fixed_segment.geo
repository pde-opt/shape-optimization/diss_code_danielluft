delta = 0.03;
size_factor = 1.;
radius = 0.35;
cl__1 = 1;
lc = 1;
lc2 = 1;
lc3 = 1;
lc_fine = 0.3;
lc_point = 0.125;

// Domain
Point(1) = {0.0, 0.0, 0.0, lc};
Point(2) = {1.0, 0.0, 0.0, lc};
Point(3) = {0.0, 1.0 + size_factor, 0.0, lc};
Point(4) = {1.0, 1.0 + size_factor, 0.0, lc};

Line(1) = {1, 2};
Line(2) = {2, 4};
Line(3) = {4, 3};
Line(4) = {3, 1};

Physical Line(1) = {1};
Physical Line(2) = {2};
Physical Line(3) = {3};
Physical Line(4) = {4};


// Shape mit LineSegment fuer konstante Dirichlet Bed.
phi = 0.05;
Point(5) = {0.5 + radius*Cos(1.5*Pi-phi), 0.5 + radius*Sin(1.5*Pi-phi), 0., lc_fine};
Point(6) = {0.5 + radius*Cos(1.5*Pi+phi), 0.5 + radius*Sin(1.5*Pi+phi), 0., lc_fine};
Point(7) = {0.5, 0.5, 0.0, lc};
Point(8) = {0.5, 0.5 + radius, 0.0, lc_fine};


Circle(5) = {5, 7, 8};
Circle(6) = {8, 7, 6};
Circle(7) = {6, 7, 5};

Physical Line(5) = {5, 6};
Physical Line(6) = {7};

Line Loop(1) = {1, 2, 3, 4};
Line Loop(2) = {5, 6, 7};

// Surfaces
Plane Surface(1) = {1, 2};
Plane Surface(2) = {2};

Physical Surface(1) = {1};
Physical Surface(2) = {2};

// ATTRACTORS

// For coarsening mesh around midpoint
Point(999) = {0.5, 0.5, 0.0, lc_point};
Point{999} In Surface {2};

// INTERFACE
Field[1] = Attractor;
Field[1].EdgesList = {5,6};
Field[1].NNodesByEdge = 1000;

Field[2] = Threshold;
Field[2].IField = 1;
Field[2].LcMin = lc_fine;// element size inside DistMin
Field[2].LcMax = lc;  // element size outside DistMax
Field[2].DistMin = 0.0; // gives margin around Field, for which Threshold is LcMin ( Field[IField] <= Distmin --> F = LcMin)
Field[2].DistMax = 0.0; // gives margin around Field, for which Threshold is LcMin ( Field[IField] >= Distmax --> F = LcMax)

// Define minimum of threshold and function field
Field[5] = Min;
Field[5].FieldsList = {2};


// Use the min as the background field
Background Field = 5;



