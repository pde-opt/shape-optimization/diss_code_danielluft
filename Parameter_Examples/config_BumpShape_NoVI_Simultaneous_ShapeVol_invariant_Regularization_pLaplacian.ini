[shp_main]
# String of initial FEniCS mesh file name in Meshes folder.
start_mesh_file = mesh_circle2_scaled_fixed_segment_009_locref_04
# String of target FEniCS mesh file name in Meshes folder.
target_mesh_file = mesh_bump2_scaled_004_locref_08
# Stopping criterion for main routine:
#     (gradient_norm <= tol_shopt_abs or gradient_norm_rel <= tol_shopt_rel or iteration > max_iter_main)
#     and (J_main_rel <= tol_shopt_Jrel or counter > max_iter_main)
# Absolute tolerance for norm of combined (pre-)shape gradient (main + regularizers).
tol_shopt_abs = 1.e-5
# Relative tolerance for norm of combined  (pre-)shape shape gradient (main + regularizers).
tol_shopt_rel = 1.e-3
# Relative tolerance for main shape optimization objective.
tol_shopt_Jrel = 5.e-4
# Maximum number of main routine iterations.
max_iter_main = 1000
# Scaling factor of perimeter regularization.
perimeter_regu = 0.00001
# First coefficient of shape dependent jumping source term of state equation.
# Controls the value on the outer hold-all component.
pde_source_term_1 = -1000
# Second coefficient of shape dependent jumping source term of state equation.
# Controls the value on the hold-all component inside the shape.
pde_source_term_2 = 1000

[shp_VI]
# If True, elliptic variational inequality of obstacle type is used as state equation.
# Otherwise, elliptic PDE is used as state equation.
Vari_Ineq = False
# Obstacle for variational inequality, i.e. y_state <= vi_obstacle.
# Uses a FEniCS suitable string, e.g. '0.3 + pow(10.*(x[1]-0.5), 6)'
vi_obstacle = 0.5
# Regularization parameter for regularized varitional inequality:
# a(y, v) + max(0, lambda_bar + c_regu_VI*(y - vi_obstacle)) = f
c_regu_VI = 10.
# Regularization parameter for smoothed varitional inequality:
# a(y, v) + max_gamma(lambda_bar + c_regu_VI*(y - vi_obstacle)) = f,
# where smoothed max function is given by:
# max_gamma(x) = 0, if x <= -1./gamma_regu_VI
#              = 0.25*gamma_regu_VI*x**2 + 0.5*x + 0.25/gamma_regu_VI, if -1./gamma_regu_VI < x < 1./gamma_regu_VI
#              = 1, if x >= 1./gamma_regu_VI
gamma_regu_VI = 1.
# Absolute tolerance for solution of (regularized) VI state equation.
eps_tol_abs_VI = 1.e-7
# Relative tolerance for solution of (regularized) VI state equation.
eps_tol_rel_VI = 1.e-6

[shp_grad_repr]
# (Pre-)shape gradient representation/Steklov-Poincaré metrics.
# For right-hand-sides (RHS), see section prshp_main, Mode.
# LHS_Mode:
#    LinElas: Gradient system uses linear elasticity with zero order terms:
#        StekP_Viscosity*linelas(U, V) + StekP_L2*(U, V)_l2 = RHS
#    pLaplacian: Gradient system uses stabilized p-Laplacian with
#        local weighting mu_elas:
#        (eps_pLapl**2 + grad(U)^T grad(U))**(0.5*p_pLapl-1.) * mu_elas*grad(U)^T grad(V) = RHS
LHS_Mode = pLaplacian
StekP_Viscosity = 1.
StekP_L2 = 0.
p_pLapl = 6
eps_pLapl = 8.
# Constants used as outer and innter Dirichlet conditions
# for Laplace system in calc_lame_par for Lamé-parameters.
mu_min = 0.05
mu_max = 1.

[shp_lbfgs]
# If True: Uses shape limited memory BFGS method.
# Otherwise: Uses steepest shape gradient descent method.
L_BFGS = False
# If True: If L-BFGS curvature condition is violated, routine exits.
curv_break = False
# Memory/history length of L-BFGS method.
memory_length = 5

[shp_linesearch]
# If True: Backtracking line search is employed.
Linesearch = True
# Initial search direction is normalized via L2-Norm before search.
Linesearch_normed = True
# Rescale factor after unsuccessful search attempt.
shrinkage = 0.5
# Factor for Armijo condition in line search.
armijo_factor = 0.0
# Initial scaling factor applied to search direction.
start_scale = 1.e0

[prshp_main]
# Mode: Defines the preshape regularizations employed via
#    right-hand-sides of preshape gradient systems. The following modes
#    are permissible, where J is the main shape objective, J_perim the
#    perimeter regularization, J_shp the shape mesh quality regularizer,
#    and J_vol the volume mesh quality regularizer.
#    Total:
#        Uses full preshape derivatives, hence shapes are distorted by regularization (Caution!).
#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_shape*DJ_shp[V] + alpha_vol*DJ_vol[V]
#    Total_ShpPrm_tang_VolPrm_inv:
#        All regularizations employed, where shape regularization uses only
#        tangential components, and volume regularization uses invariant shape (The method to go!).
#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_shape*DJ_shp_tangential[V] + alpha_vol*DJ_vol[Shp_proj[V]]
#    Total_ShpPrm_tang_VolPrm_inv_freetangential_HoldAll:
#        Outer hold-all-domain boundaries are allowed to move tangentially.
#        Same as Total_ShpPrm_tang_VolPrm_inv, but instead of 0-Dirichlet
#        condition on outer boundaries, the following Dirichlet boundary
#        condition holds in tangential (!) outer hold-all boundary directions:
#        gradient = boundary_magnification * tangential_components(DoFs(DJ_vol[Shp_proj[V]]))
#    ShapeObj:
#        Ordinary shape optimization.
#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V]
#    Simult_ShapeObj_VolPrm:
#        Simultaneous shape optimization with volume regularization (Caution: No invariant shapes!).
#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_vol*DJ_vol[V]
#    Simult_ShapeObj_VolPrm_inv:
#        Simultaneous shape optimization with volume regularization and invariant shapes.
#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_vol*DJ_vol[Shp_proj[V]]
#    Simult_ShapeObj_VolPrm_inv_freetangential_HoldAll:
#        Additionally, outer hold-all-domain boundaries are allowed to move tangentially,
#        via Dirichlet boundary condition as in Total_ShpPrm_tang_VolPrm_inv_freetangential_HoldAll.
#    Simult_ShapeObj_ShpPrm:
#        Simultaneous shape optimization with shape regularization (Caution: No invariant shapes!).
#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_shape*DJ_shp[V]
#    Simult_ShapeObj_ShpPrm_tang:
#        Simultaneous shape optimization with tangential shape regularization.
#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_shape*DJ_shp_tangential[V]
#    ShapePrm:
#        Solo shape parameterization tracking functional, no shape objective.
#        RHS = alpha_shape*DJ_shp[V]
#    ShapePrm_normal:
#        Solo shape parameterization tracking functional with normal components only, no shape objective.
#        RHS = alpha_shape*DJ_shp_normal[V]
#    ShapePrm_tang:
#        Solo shape parameterization tracking functional with tangential components only, no shape objective.
#        RHS = alpha_shape*DJ_shp_tangential[V]
#    ShapeVol_ShapeInv_freetangential_HoldAll:
#        Solo volume parameterization tracking functional with invariant shapes
#        and free tangential outer hold-all boundaries, no shape objective.
#        RHS = alpha_vol*DJ_vol[Shp_proj[V]]
#    ShapeVol_ShapeInv:
#        Solo volume parameterization tracking functional with invariant shapes, no shape objective.
#    ShapeVol:
#        Solo volume parameterization tracking functional, no shape objective.
#        RHS = alpha_vol*DJ_vol[V]
#    ShapeVol_scalarized_uniform_potential:
#        Experimantal mode, exploiting structure for uniform volume mesh target, and
#        solving scalar valued gradient equations instead of vector valued ones.
#    ShapeVol_scalarized_uniform_potential_shpinv:
#        Shapes are left invariant.
#    ShapeVol_scalarized_uniform_potential_shpinv_tangouter:
#        Shapes are left invariant and outer hold-all is tangentially free.
Mode = Total_ShpPrm_tang_VolPrm_inv
alpha_main = 1.
alpha_shape = 1.e3
alpha_volume = 1.e2
boundary_magnification = 250.
# Target function for shape regularization, controls the shape mesh node density. Needs FEniCS suitable function
# string, e.g. '1. + pow(1.+x[1], 10.)' or '1. + .1*sin(100.*x[0])'.
q_ext_str = 1.
# Target function for volume regularization, controls the volume mesh node density. Needs FEniCS suitable function
# string, e.g. '0.2 + pow(x[2]-0.5, 10)' or '55. - pow(10.*(x[0]-0.5), 2) - pow(10.*(x[1]-0.5), 2)'.
q_vol_str = 1.

[prshp_sign_dist]
# If True: Preshape volume regularization uses target constructed via
# signed distance function from a stabilized Eikonal equation
# (grad(s_dist)^T grad(s_dist))**(1./2.)*v + eps_stab*(grad(s_dist)^T grad(v)) - 1.*v = 0
# with Dirichlet condition s_dist = 0 on the active set boundary dA of the VI.
VolTr_signed_dist = False
eps_stab = 10.
# If True: Dynamical computation of stabilization parameter
# eps_stab_fac = eps_stab*h_max, with h_max being the maximum edge length of the mesh.
eps_stab_dyn = True
# Transformation function F(T(x)) used to generate volume regularization target from signed distance:
# F(x) = scaling_target_tr*beta_target_tr/(2.*alpha_target_tr*sp.gamma(1./beta_target_tr))
#       * exp(-|(x - location_target_tr)/alpha_target_tr|**beta_target_tr) + eps_target_tr,
# where sp.gamma is the gamma function, and T(x): [min_val, max_val] -> [0, 1] linearly.
alpha_target_tr = 0.45
beta_target_tr = 8.
location_target_tr = 0.1
scaling_target_tr = 2.
eps_target_tr = 1.

[prshp_subr]
# If True: Preshape volume regularization subroutine is employed.
# It optimizes the volume parameterization tracking functional, if:
# J_vol_rel <= tol_vol_subr_trigger
# Then the volume mesh is optimized without shape objective until:
# J_vol_prm_intermediate_rel <= tol_vol_subr_baseline or subr_iteration >= max_iter_vol
VolOpt_substep = False
tol_vol_subr_trigger = 5.e-1
tol_vol_subr_baseline = 3.e-2
max_iter_vol = 10
# The following volume mesh optimization routines are permissible
# (for descriptions see section prshp_main):
#    ShapeVol_ShapeInv
#    ShapeVol
#    ShapeVol_scalarized_uniform_potential
#    ShapeVol_scalarized_uniform_potential_shpinv
#    ShapeVol_scalarized_uniform_potential_shpinv_tangouter
Mode_vol_subr = ShapeVol_ShapeInv

[prshp_subr_grad_repr]
# Preshape gradient representation for subroutine,
# only linear elasticity is valid, see shp_grad_repr.
# mu_min and mu_max are used as defined in shp_grad_repr.
StekP_Viscosity_vol = 1.
StekP_L2_vol = 0.

[prshp_subr_linesearch]
# Line search parameters for volume mesh optimization subroutine, see shp_linesearch.
Linesearch_vol = True
Linesearch_vol_normed = False
shrinkage_vol = 0.5
armijo_factor_vol = 0.
start_scale_vol = 0.25

[prshp_subr_lbfgs]
# L-BFGS parameters for volume mesh optimization subroutine, see shp_lbfgs.
L_BFGS_vol = True
curv_break_vol = False
memory_length_vol = 3

