# -*- coding: utf-8 -*-
"""Bibliography for PDE/VI constrained shape optimization routines via preshape calculus.

Shape optimization problems are of tracking type, and can be constrained by
PDE or variational inequality (VI) constraints.
Implements finite element and preshape calculus techniques using FEniCS 2016.

All functions and classes are documented, see their documentation below for
individual usage and behaviors.

References:
    Pre-Shape Calculus - a Unified Framework for Mesh Quality and Shape Optimization, Daniel Luft, 2021, Dissertation, Trier University
    Efficient Techniques for Shape Optimization with Variational Inequalities using Adjoints, Daniel Luft, Volker Schulz, Kathrin Welker, 2020, arXiv:1904.08650
    Pre-Shape Calculus: Foundations and Application to Mesh Quality Optimization, Daniel Luft, Volker Schulz, 2021, arXiv:2012.09124
    Simultaneous Shape and Mesh Quality Optimization using Pre-Shape Calculus, Daniel Luft, Volker Schulz, 2021, arXiv:2103.15109
"""
import os
import configparser as confp
import typing as tp
from datetime import datetime
import numpy as np
import errno

import scipy.special as sp
from shutil import copyfile

import fenics as fe
import ufl as ufl

__author__ = "Daniel Luft"
__copyright__ = "Dissertation, Daniel Luft, Trier University, 2022"

__this_files_path = os.path.realpath(__file__)
__this_files_dir = os.path.dirname(__this_files_path)

# Pfad zum Ordner mit den Gitter Dateien
DATA_DIR = os.path.abspath(os.path.join(__this_files_dir, 'Meshes'))


class MeshData:
    """Class to operate FEniCS meshes and associated objects.


    Attributes:
        mesh: FEniCS.mesh object representing the mesh.
        subdomains: FEniCS.MeshFunction with subdomain tags.
        boundaries: FEniCS.MeshFunction with facet tags for interfaces and
            boundaries.
        ind_update: If True, initializes index lists with 0, which can then be
            updated manually without necessary ordering and computations.
            If False, lists are computed in __init__.
        indBoundary: np.array acting as list of indices of nodes participating
            in interior boundary (shape).
        indBoundaryFaces: np.array acting as list of indices of facets
            participating in interior boundary (shape).
        indNotIntBoundary: np.array acting as list of entries of assembled
            FEniCS vector PDE system, which correspond to nodes without support
            on the interior boundary (shape).
        indCornerNodes: np.array acting as list of indices of nodes of corners
            of hold-all-domain.
        indOuterBoundary: np.array acting as list of indices of nodes of outer
            boundaries of hold-all-domain.
        indVerticesBySubdomain: list of np.arrays corresponding to node indices
            of subdomain tags indexed by tag. E.g.
            indVerticesBySubdomain = [indices_subd_0, indices_subd_1, ...].
    """
    def __init__(self, mesh: fe.Mesh, subdomains: fe.MeshFunction, boundaries: fe.MeshFunction, ind_update: bool = False) -> None:
        # FEniCS mesh
        self.mesh = mesh
        # FEniCS subdomains
        self.subdomains = subdomains
        # FEniCS boundaries
        self.boundaries = boundaries

        if not ind_update:
            # Orientation of cells of shape as SurfaceMesh for CG elements:
            # self.init_Shape_Faces_Orientation()
            # Boolean list: True for nodes with support not at the interior
            # boundary: self.indNotIntBoundary
            self.__get_index_not_interior_boundary_class()
            self.__get_outer_boundary_corners()
            self.__get_vertex_domain_list()
        elif ind_update:
            self.indBoundary = 0
            self.indBoundaryFaces = 0
            # self.indFacesOrientation = 0
            self.indNotIntBoundary = 0
            self.indCornerNodes = 0
            self.indOuterBoundary = 0
            self.indVerticesBySubdomain = 0

    def __get_index_not_interior_boundary_class(self) -> None:
        """
        Returns np.arrays indBoundary, indBoundaryFaces and indNotIntBoundary.
        Refer to attributes section of class docstring for their descriptions.
        """
        # Get node indices for vertices of the interior boundaries
        ind_interior_boundary_vertices = []
        ind_interior_boundary_facets = []
        for f in fe.facets(self.mesh):
            if self.boundaries[f] == 5 or self.boundaries[f] == 6:
                ind_interior_boundary_facets.append(f.index())
                for v in fe.vertices(f):
                    ind_interior_boundary_vertices.append(v.index())

        ind_interior_boundary_vertices = list(set(ind_interior_boundary_vertices))
        ind_interior_boundary_facets = list(set(ind_interior_boundary_facets))

        # Get element indices of interior boundaries
        ind_around_interior_boundary_cells = []
        for c in fe.cells(self.mesh):
            ind = False
            for v in fe.vertices(c):
                if v.index() in ind_interior_boundary_vertices:
                    ind = True
            if ind:
                ind_around_interior_boundary_cells.append(c.index())

        # Define new subdomains
        new_sub = fe.MeshFunction("size_t", self.mesh, 2)
        new_sub.set_all(0)
        for i in ind_around_interior_boundary_cells:
            if self.subdomains[i] == 1:
                new_sub[i] = 1
            else:
                new_sub[i] = 2

        # Define a test problem to get indices corresponding to nodes without
        # support on interior boundaries.
        V = fe.VectorFunctionSpace(self.mesh, "CG", 1, dim=2)
        v = fe.TestFunction(V)

        dx_int = fe.Measure('dx', domain=self.mesh, subdomain_data=new_sub)

        dummy_y = fe.Constant((1.0, 1.0))

        f_elas_int_1 = fe.inner(dummy_y, v)*dx_int(1)
        F_elas_int_1 = fe.assemble(f_elas_int_1)
        f_elas_int_2 = fe.inner(dummy_y, v)*dx_int(2)
        F_elas_int_2 = fe.assemble(f_elas_int_2)

        # Get indices, where values do not influence the system constructed on subdomains
        ind1 = (F_elas_int_1.get_local() == 0.0)
        ind2 = (F_elas_int_2.get_local() == 0.0)

        ind = ind1 | ind2

        self.indBoundary = np.array(ind_interior_boundary_vertices)
        self.indBoundaryFaces = np.array(ind_interior_boundary_facets)
        self.indNotIntBoundary = np.array(ind)

    def __get_outer_boundary_corners(self) -> None:
        """
        Returns np.arrays indCornerNodes and indOuterBoundary.
        Refer to attributes section of class docstring for their descriptions.
        """
        ind_corner_nodes = []
        ind_outer_boundary = []

        for f in fe.facets(self.mesh):
            if self.boundaries[f] in [1, 2, 3, 4]:
                for v in fe.vertices(f):
                    ind_outer_boundary.append(v.index())

        ind_outer_boundary = list(set(ind_outer_boundary))

        # If nodes are at an intersections of at minimum 2 subdomains, then
        # they are tagged as corners. (Caution: 0 is always there as tag!)
        for i in ind_outer_boundary:
            facet_domain_tags = []
            for f in fe.facets(fe.Vertex(self.mesh, i)):
                facet_domain_tags.append(self.boundaries[f])
            facet_domain_tags = list(set(facet_domain_tags))
            if len(facet_domain_tags) > 2:
                ind_corner_nodes.append(i)

        ind_corner_nodes = list(set(ind_corner_nodes))

        self.indCornerNodes = np.array(ind_corner_nodes)
        self.indOuterBoundary = np.array(ind_outer_boundary)

    def __get_index_not_interior_boundary_class_no_support(self) -> None:
        """
        Returns np.array indNotIntBoundaryNoSupp, which is an array of indices
        corresponding to interior boundary entries of FEniCS vector PDE systems.
        """
        # Get indices of entries without support on interior boundary via test problem.
        V = fe.VectorFunctionSpace(self.mesh, "CG", 1, dim=2)
        v = fe.TestFunction(V)
        dS = fe.Measure('dS', domain=self.mesh, subdomain_data=self.boundaries)
        dummy_y = fe.Constant((1.0, 1.0))

        f_elas_int_1 = fe.inner(dummy_y('+'), v('+'))*(dS(5)+dS(6))
        F_elas_int_1 = fe.assemble(f_elas_int_1)

        ind = (F_elas_int_1.get_local() == 0.0)

        self.indNotIntBoundaryNoSupp = ind

    def __get_vertex_domain_list(self) -> None:
        """
        Returns np.array indVerticesBySubdomain.
        Refer to attributes section of class docstring for its description.
        """
        vert_inds_subd1 = []
        vert_inds_subd2 = []

        for c in fe.cells(self.mesh):
            if self.subdomains[c.index()] == 1:
                for v in fe.vertices(c):
                    vert_inds_subd1.append(v.index())
            elif self.subdomains[c.index()] == 2:
                for v in fe.vertices(c):
                    vert_inds_subd2.append(v.index())

        vert_inds_subd1 = set(vert_inds_subd1)
        vert_inds_subd2 = set(vert_inds_subd2)

        vert_inds_subd1 = vert_inds_subd1.difference(self.indBoundary)
        vert_inds_subd2 = vert_inds_subd2.difference(self.indBoundary)

        self.indVerticesBySubdomain = [np.array(list(vert_inds_subd1)), np.array(list(vert_inds_subd2))]


class BFGSMemory:
    """Class containing complete information for the shape L-BFGS method.


    Attributes:
        gradient: np.array consisting of self.length FEniCS DoF value arrays
            of previously computed (pre-)shape gradients. Entries are sorted
            in increasing order for older gradients (first in last out, queue).
            Initialized with zero arrays in main routine.
        deformation: np.array consisting of self.length FEniCS DoF value arrays
            of previously applied mesh deformations. Entries are sorted in
            increasing order for older deformations (first in last out, queue).
            Initialized with zero arrays in main routine.
        length: Memory length of L-BFGS method.
        step_nr: Step counter, which is incremented after each successful
            L-BFGS step in main routine. Is set to zero after memory reboot.
    """
    def __init__(self, gradient: np.ndarray, deformation: np.ndarray, length: int, step_nr: int) -> None:
        # List of gradient vector fields DoFs
        if len(gradient) == length:
            self.gradient = gradient
        else:
            raise SystemExit("Error: Number of gradients does not match MEMORY_LENGTH!")

        # List of deformation vector fields DoFs
        if len(deformation) == length:
            self.deformation = deformation
        else:
            raise SystemExit("Error: number of deformations does not match MEMORY_LENGTH!")

        # L-BFGS memory length
        self.length = length

        # Counter of previously successfully computed L-BFGS steps
        if (step_nr >= 0 and isinstance(step_nr, int)):
            self.step_nr = step_nr
        else:
            raise SystemExit("Error: step_nr needs to be an integer >= 0.")

    def update_grad(self, upd_grad: np.ndarray) -> None:
        """
        Performs update of gradient memory (newest element gets 0, first in last out).
        """
        for i in range(self.length-1):
            self.gradient[-(i+1)] = self.gradient[-(i+2)]
        self.gradient[0] = upd_grad

    def update_defo(self, upd_defo: np.ndarray) -> None:
        """
        Performs update of deformation memory (newest element gets 0, first in last out).
        """
        for i in range(self.length-1):
            self.deformation[-(i+1)] = self.deformation[-(i+2)]
        self.deformation[0] = upd_defo

    def initialize_grad(self, meshdata: MeshData, i: int) -> fe.Function:
        """
        Returns FEniCS function of gradient vector field on meshdata from
        saved gradient memory entry i. Enables transport of vector fields via
        push forwards (values and their indices are independent of coordinates).
        """
        if isinstance(meshdata, MeshData):
            pass
        else:
            raise SystemExit("initialize_grad requires instance of MeshData class!")

        V = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1, dim=2)
        f = fe.Function(V)
        f.vector()[:] = self.gradient[i]
        return f

    def initialize_defo(self, meshdata: MeshData, i: int) -> fe.Function:
        """
        Returns FEniCS function of deformation vector field on meshdata from
        saved gradient memory entry i. Enables transport of vector fields via
        push forwards (values and their indices are independent of coordinates).
        """
        if isinstance(meshdata, MeshData):
            pass
        else:
            raise SystemExit("initialize_defo benoetigt Objekt der MeshData-Klasse als Input!")

        V = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1, dim=2)
        f = fe.Function(V)
        f.vector()[:] = self.deformation[i]
        return f


def write_config_parameters(forced: bool = False) -> None:
    """Writes config.ini file of main parameters in current working directory.


    The written config.ini file is used to set most of the parameters of the
    shape optimization routine and its regularizations. The config.ini also
    consists of comments, which describe the effects of each parameter.


    Args:
        forced: If True, config.ini will be overwritten if existent.
    """
    if not forced and os.path.isfile(os.path.join(__this_files_dir, "config.ini")):
        raise SystemExit(str(os.path.join(__this_files_dir, "config.ini")) + " already exists. Either remove it manually, or use forced=True option.")

    config = confp.ConfigParser(allow_no_value=True)
    config.optionxform = str

    # collections module for ordered dictionaries, gives ordered entries in .ini
    from collections import OrderedDict as collOrderedDict

    # Main Problem Parameters
    config['shp_main'] = collOrderedDict((
        ("# String of initial FEniCS mesh file name in Meshes folder.", None),
        ("start_mesh_file", "mesh_fine_smallercircle"),
        ("# String of target FEniCS mesh file name in Meshes folder.", None),
        ("target_mesh_file", "mesh_fine_broken_donut"),
        ("# Stopping criterion for main routine:", None),
        ("#     (gradient_norm <= tol_shopt_abs or gradient_norm_rel <= tol_shopt_rel or iteration > max_iter_main)", None),
        ("#     and (J_main_rel <= tol_shopt_Jrel or counter > max_iter_main)", None),
        ("# Absolute tolerance for norm of combined (pre-)shape gradient (main + regularizers).", None),
        ("tol_shopt_abs", "8.e-10"),
        ("# Relative tolerance for norm of combined  (pre-)shape shape gradient (main + regularizers).", None),
        ("tol_shopt_rel", "1.e-4"),
        ("# Relative tolerance for main shape optimization objective.", None),
        ("tol_shopt_Jrel", "3.8e-3"),
        ("# Maximum number of main routine iterations.", None),
        ("max_iter_main", "1000"),
        ("# Scaling factor of perimeter regularization.", None),
        ("perimeter_regu", "0.00001"),
        ("# First coefficient of shape dependent jumping source term of state equation.", None),
        ("# Controls the value on the outer hold-all component.", None),
        ("pde_source_term_1", "-100"),
        ("# Second coefficient of shape dependent jumping source term of state equation.", None),
        ("# Controls the value on the hold-all component inside the shape.", None),
        ("pde_source_term_2", "100"),
        ))

    # Variational inequality Parameters
    config['shp_VI'] = collOrderedDict((
        ("# If True, elliptic variational inequality of obstacle type is used as state equation.", None),
        ("# Otherwise, elliptic PDE is used as state equation.", None),
        ("Vari_Ineq", "True"),
        ("# Obstacle for variational inequality, i.e. y_state <= vi_obstacle.", None),
        ("# Uses a FEniCS suitable string, e.g. '0.3 + pow(10.*(x[1]-0.5), 6)'", None),
        ("vi_obstacle", "0.5"),
        # ("vi_regu", "unregu"),  # "regu"  # "smoothed"  # TODO: option for different regularizations
        ("# Regularization parameter for regularized varitional inequality:", None),
        ("# a(y, v) + max(0, lambda_bar + c_regu_VI*(y - vi_obstacle)) = f", None),
        ("c_regu_VI", "10."),
        ("# Regularization parameter for smoothed varitional inequality:", None),
        ("# a(y, v) + max_gamma(lambda_bar + c_regu_VI*(y - vi_obstacle)) = f,", None),
        ("# where smoothed max function is given by:", None),
        ("# max_gamma(x) = 0, if x <= -1./gamma_regu_VI", None),
        ("#              = 0.25*gamma_regu_VI*x**2 + 0.5*x + 0.25/gamma_regu_VI, if -1./gamma_regu_VI < x < 1./gamma_regu_VI", None),
        ("#              = 1, if x >= 1./gamma_regu_VI", None),
        ("gamma_regu_VI", "1."),
        ("# Absolute tolerance for solution of (regularized) VI state equation.", None),
        ("eps_tol_abs_VI", "1.e-7"),
        ("# Relative tolerance for solution of (regularized) VI state equation.", None),
        ("eps_tol_rel_VI", "1.e-6"),
        ))

    # (Pre-)shape gradient representation system parameters
    config['shp_grad_repr'] = collOrderedDict((
        ("# (Pre-)shape gradient representation/Steklov-Poincaré metrics.", None),
        ("# For right-hand-sides (RHS), see section prshp_main, Mode.", None),
        ("# LHS_Mode:", None),
        ("#    LinElas: Gradient system uses linear elasticity with zero order terms:", None),
        ("#        StekP_Viscosity*linelas(U, V) + StekP_L2*(U, V)_l2 = RHS", None),
        ("#    pLaplacian: Gradient system uses stabilized p-Laplacian with", None),
        ("#        local weighting mu_elas:", None),
        ("#        (eps_pLapl**2 + grad(U)^T grad(U))**(0.5*p_pLapl-1.) * mu_elas*grad(U)^T grad(V) = RHS", None),
        ("LHS_Mode", "LinElas"),
        ("StekP_Viscosity", "1."),  # 1.
        ("StekP_L2", "0."),  # 1.0
        ("p_pLapl", "6"),  # 10
        ("eps_pLapl", "9.5"),  # 8.  # 9.5
        ("# Constants used as outer and innter Dirichlet conditions", None),
        ("# for Laplace system in calc_lame_par for Lamé-parameters.", None),
        ("mu_min", "0.05"),  # 0.001  # TODO: Change to stekp_lame_min
        ("mu_max", "1."),  # 30.  # TODO: Change to stekp_lame_max
        ))

    # L-BFGS parameters
    config['shp_lbfgs'] = collOrderedDict((
        ("# If True: Uses shape limited memory BFGS method.", None),
        ("# Otherwise: Uses steepest shape gradient descent method.", None),
        ("L_BFGS", "False"),  # Achtung: pre-shape Hessian Bilineform kann in bfgs_step noch aktiv sein!
        ("# If True: If L-BFGS curvature condition is violated, routine exits.", None),
        ("curv_break", "False"),
        ("# Memory/history length of L-BFGS method.", None),
        ("memory_length", "5"),
        ))

    # Line search parameters
    config['shp_linesearch'] = collOrderedDict((
        ("# If True: Backtracking line search is employed.", None),
        ("Linesearch", "True"),
        ("# Initial search direction is normalized via L2-Norm before search.", None),
        ("Linesearch_normed", "True"),
        ("# Rescale factor after unsuccessful search attempt.", None),
        ("shrinkage", "0.5"),
        ("# Factor for Armijo condition in line search.", None),
        ("armijo_factor", "0.0"),
        ("# Initial scaling factor applied to search direction.", None),
        ("start_scale", "1.e-2"),  # 1.e-2
        ))

    # Preshape regularization main parameters
    config['prshp_main'] = collOrderedDict((
        ("# Mode: Defines the preshape regularizations employed via", None),
        ("#    right-hand-sides of preshape gradient systems. The following modes", None),
        ("#    are permissible, where J is the main shape objective, J_perim the", None),
        ("#    perimeter regularization, J_shp the shape mesh quality regularizer,", None),
        ("#    and J_vol the volume mesh quality regularizer.", None),
        ("#    Total:", None),
        ("#        Uses full preshape derivatives, hence shapes are distorted by regularization (Caution!).", None),
        ("#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_shape*DJ_shp[V] + alpha_vol*DJ_vol[V]", None),
        ("#    Total_ShpPrm_tang_VolPrm_inv:", None),
        ("#        All regularizations employed, where shape regularization uses only", None),
        ("#        tangential components, and volume regularization uses invariant shape (The method to go!).", None),
        ("#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_shape*DJ_shp_tangential[V] + alpha_vol*DJ_vol[Shp_proj[V]]", None),
        ("#    Total_ShpPrm_tang_VolPrm_inv_freetangential_HoldAll:", None),
        ("#        Outer hold-all-domain boundaries are allowed to move tangentially.", None),
        ("#        Same as Total_ShpPrm_tang_VolPrm_inv, but instead of 0-Dirichlet", None),
        ("#        condition on outer boundaries, the following Dirichlet boundary", None),
        ("#        condition holds in tangential (!) outer hold-all boundary directions:", None),
        ("#        gradient = boundary_magnification * tangential_components(DoFs(DJ_vol[Shp_proj[V]]))", None),
        ("#    ShapeObj:", None),
        ("#        Ordinary shape optimization.", None),
        ("#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V]", None),
        ("#    Simult_ShapeObj_VolPrm:", None),
        ("#        Simultaneous shape optimization with volume regularization (Caution: No invariant shapes!).", None),
        ("#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_vol*DJ_vol[V]", None),
        ("#    Simult_ShapeObj_VolPrm_inv:", None),
        ("#        Simultaneous shape optimization with volume regularization and invariant shapes.", None),
        ("#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_vol*DJ_vol[Shp_proj[V]]", None),
        ("#    Simult_ShapeObj_VolPrm_inv_freetangential_HoldAll:", None),
        ("#        Simultaneous shape optimization with volume regularization and invariant shapes.", None),
        ("#        Additionally, outer hold-all-domain boundaries are allowed to move tangentially,", None),
        ("#        via Dirichlet boundary condition as in Total_ShpPrm_tang_VolPrm_inv_freetangential_HoldAll.", None),
        ("#    Simult_ShapeObj_ShpPrm:", None),
        ("#        Simultaneous shape optimization with shape regularization (Caution: No invariant shapes!).", None),
        ("#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_shape*DJ_shp[V]", None),
        ("#    Simult_ShapeObj_ShpPrm_tang:", None),
        ("#        Simultaneous shape optimization with tangential shape regularization.", None),
        ("#        RHS = alpha_main*DJ[V] + nu*DJ_perim[V] + alpha_shape*DJ_shp_tangential[V]", None),
        ("#    ShapePrm:", None),
        ("#        Solo shape parameterization tracking functional, no shape objective.", None),
        ("#        RHS = alpha_shape*DJ_shp[V]", None),
        ("#    ShapePrm_normal:", None),
        ("#        Solo shape parameterization tracking functional with normal components only, no shape objective.", None),
        ("#        RHS = alpha_shape*DJ_shp_normal[V]", None),
        ("#    ShapePrm_tang:", None),
        ("#        Solo shape parameterization tracking functional with tangential components only, no shape objective.", None),
        ("#        RHS = alpha_shape*DJ_shp_tangential[V]", None),
        ("#    ShapeVol_ShapeInv_freetangential_HoldAll:", None),
        ("#        Solo volume parameterization tracking functional with invariant shapes", None),
        ("#        and free tangential outer hold-all boundaries, no shape objective.", None),
        ("#        RHS = alpha_vol*DJ_vol[Shp_proj[V]]", None),
        ("#    ShapeVol_ShapeInv:", None),
        ("#        Solo volume parameterization tracking functional with invariant shapes, no shape objective.", None),
        ("#        RHS = alpha_vol*DJ_vol[Shp_proj[V]]", None),
        ("#    ShapeVol:", None),
        ("#        Solo volume parameterization tracking functional, no shape objective.", None),
        ("#        RHS = alpha_vol*DJ_vol[V]", None),
        ("#    ShapeVol_scalarized_uniform_potential:", None),
        ("#        Experimantal mode, exploiting structure for uniform volume mesh target, and", None),
        ("#        solving scalar valued gradient equations instead of vector valued ones.", None),
        ("#    ShapeVol_scalarized_uniform_potential_shpinv:", None),
        ("#        Experimantal mode, exploiting structure for uniform volume mesh target, and", None),
        ("#        solving scalar valued gradient equations instead of vector valued ones.", None),
        ("#        Shapes are left invariant.", None),
        ("#    ShapeVol_scalarized_uniform_potential_shpinv_tangouter:", None),
        ("#        Experimantal mode, exploiting structure for uniform volume mesh target, and", None),
        ("#        solving scalar valued gradient equations instead of vector valued ones.", None),
        ("#        Shapes are left invariant and outer hold-all is tangentially free.", None),
        ("Mode", "ShapeObj"),
        ("alpha_main", "1."),  # 0.0001
        ("alpha_shape", "1.e-8"),  # 1000.
        ("alpha_volume", "1.e-3"),  # 1.e-4  # 100.  # 100.
        ("boundary_magnification", "250."),
        ("# Target function for shape regularization, controls the shape mesh node density. Needs FEniCS suitable function", None),
        ("# string, e.g. '1. + pow(1.+x[1], 10.)' or '1. + .1*sin(100.*x[0])'.", None),
        ("q_ext_str", "1."),
        ("# Target function for volume regularization, controls the volume mesh node density. Needs FEniCS suitable function", None),
        ("# string, e.g. '0.2 + pow(x[2]-0.5, 10)' or '55. - pow(10.*(x[0]-0.5), 2) - pow(10.*(x[1]-0.5), 2)'.", None),
        ("q_vol_str", "1."),
        ))

    # preshape vol regu target via signed distance
    # PreShape Tracking Targets for Submanifolds via signed distance
    config['prshp_sign_dist'] = collOrderedDict((
        ("# If True: Preshape volume regularization uses target constructed via", None),
        ("# signed distance function from a stabilized Eikonal equation", None),
        ("# (grad(s_dist)^T grad(s_dist))**(1./2.)*v + eps_stab*(grad(s_dist)^T grad(v)) - 1.*v = 0", None),
        ("# with Dirichlet condition s_dist = 0 on the active set boundary dA of the VI.", None),
        ("VolTr_signed_dist", "True"),
        ("eps_stab", "10."),
        ("# If True: Dynamical computation of stabilization parameter", None),
        ("# eps_stab_fac = eps_stab*h_max, with h_max being the maximum edge length of the mesh.", None),
        ("eps_stab_dyn", "True"),
        ("# Transformation function F(T(x)) used to generate volume regularization target from signed distance:", None),
        ("# F(x) = scaling_target_tr*beta_target_tr/(2.*alpha_target_tr*sp.gamma(1./beta_target_tr))", None),
        ("#       * exp(-|(x - location_target_tr)/alpha_target_tr|**beta_target_tr) + eps_target_tr,", None),
        ("# where sp.gamma is the gamma function, and T(x): [min_val, max_val] -> [0, 1] linearly.", None),
        ("alpha_target_tr", "0.45"),  # 0.35  # 0.25
        ("beta_target_tr", "8."),  # 8.  # 3.
        ("location_target_tr", "0.1"),  # 0.2  # 0.15
        ("scaling_target_tr", "2."),  # 0.75  # 7.5
        ("eps_target_tr", "1."),
        ))

    # alternating preshape volume optimization subroutine
    config['prshp_subr'] = collOrderedDict((
        ("# If True: Preshape volume regularization subroutine is employed.", None),
        ("# It optimizes the volume parameterization tracking functional, if:", None),
        ("# J_vol_rel <= tol_vol_subr_trigger", None),
        ("# Then the volume mesh is optimized without shape objective until:", None),
        ("# J_vol_prm_intermediate_rel <= tol_vol_subr_baseline or subr_iteration >= max_iter_vol", None),
        ("VolOpt_substep", "True"),
        ("tol_vol_subr_trigger", "5.e-1"),  # 1.3e0  # 1.4e1  # if J_vol_rel > tol_vol_subr_trigger, then subroutine is triggered until J_vol_rel <= tol_vol_subr_baseline
        ("tol_vol_subr_baseline", "3.e-2"),  # 1.e-4  # 0.25
        ("max_iter_vol", "10"),  # 25   # for 0 there is still 1 deformation, i.e. instead of 0 set VOLOPT_SUBSTEP = False for no deformations
        ("# The following volume mesh optimization routines are permissible", None),
        ("# (for descriptions see section prshp_main):", None),
        ("#    ShapeVol_ShapeInv", None),
        ("#    ShapeVol", None),
        ("#    ShapeVol_scalarized_uniform_potential", None),
        ("#    ShapeVol_scalarized_uniform_potential_shpinv", None),
        ("#    ShapeVol_scalarized_uniform_potential_shpinv_tangouter", None),
        ("Mode_vol_subr", "ShapeVol_ShapeInv"),
        # ("tol_vol_subr_abs", "1.e-3"),
        # ("tol_vol_subr_rel", "1.e-2"),
        ))

    # Alternating preshape subroutine gradient repr.
    config['prshp_subr_grad_repr'] = collOrderedDict((
        ("# Preshape gradient representation for subroutine,", None),
        ("# only linear elasticity is valid, see shp_grad_repr.", None),
        ("# mu_min and mu_max are used as defined in shp_grad_repr.", None),
        ("StekP_Viscosity_vol", "1."),
        ("StekP_L2_vol", "0."),  # 1.0
        ))

    # Subroutine line search parameters
    config['prshp_subr_linesearch'] = collOrderedDict((
        ("# Line search parameters for volume mesh optimization subroutine, "
         + "see shp_linesearch.", None),
        ("Linesearch_vol", "True"),
        ("Linesearch_vol_normed", "False"),
        ("shrinkage_vol", "0.5"),
        ("armijo_factor_vol", "0."),
        ("start_scale_vol", "0.25"),  # 0.25  # 0.1  # 0.9e0  # 10.
        ))

    # Subroutine L-BFGS parameters
    config['prshp_subr_lbfgs'] = collOrderedDict((
        ("# L-BFGS parameters for volume mesh optimization subroutine, "
         + "see shp_lbfgs.", None),
        ("L_BFGS_vol", "True"),      # Caution: Preshape bilinear form could still be active in L-BFGS step.
        ("curv_break_vol", "False"),
        ("memory_length_vol", "3"),
        ))

    with open("config.ini", 'w') as configfile:
        config.write(configfile)

    return None


def load_parameters() -> tp.Dict[str, tp.Dict[str, str]]:
    """Used to load parameters from config.ini file in the working directory.


    Returns:
        parameters: dictionary of main parameters of the shape optimization routine.
                    for format and names, look at write_config_parameters()
    """
    config = confp.ConfigParser()
    config.optionxform = str

    if not os.path.isfile(os.path.join(__this_files_dir, "config.ini")):
        write_config_parameters()

    config.read("config.ini")

    parameters = {section: dict(config[section].items()) for section in config.sections()}
    if not parameters:
        raise SystemExit("Error: config.ini could not be read or are empty.")

    return parameters


def print_initial_parameter(parameters: tp.Dict[str, tp.Dict[str, str]], _collength_print: int) -> None:
    """prints initial parameters from config.ini in stdout via extracted dict"""
    print("{0:-^{1}}".format(" MAIN PARAMETERS ", 2*_collength_print))
    print("{0:-<{1}}".format("---- Problem parameters ", _collength_print))
    print("{0:<{2}} {1:<{2}}".format("Start mesh name:", str(parameters["shp_main"]["start_mesh_file"]), _collength_print))
    print("{0:<{2}} {1:<{2}}".format("Target mesh name:", str(parameters["shp_main"]["target_mesh_file"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Abs. total objective tolerance:", float(parameters["shp_main"]["tol_shopt_abs"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Rel. total objective tolerance:", float(parameters["shp_main"]["tol_shopt_rel"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Rel. shape objective tolerance:", float(parameters["shp_main"]["tol_shopt_Jrel"]), _collength_print))
    print("{0:<{2}} {1:<{2}d}".format("Maximum main iterations:", int(parameters["shp_main"]["max_iter_main"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Perimeter regu. scaling:", float(parameters["shp_main"]["perimeter_regu"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("1st PDE-constraint source term:", float(parameters["shp_main"]["pde_source_term_1"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("2nd PDE-constraint source term:", float(parameters["shp_main"]["pde_source_term_2"]), _collength_print), end="\n"*2)

    # VI parameters
    print("{0:-<{1}}".format("---- Variational Inequality Parameters ", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Variational inequality:", parameters["shp_VI"]["Vari_Ineq"] == "True", _collength_print))
    # print("{0:<{2}} {1!s:<{2}}".format("Obstacle:", str(parameters["shp_VI"]["psi, _collength_print))
    # print("{0:<{2}} {1!s:<{2}}".format("VI regularization:", str(parameters["shp_VI"]["vi_regu, _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("VI regularization parameter:", float(parameters["shp_VI"]["c_regu_VI"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("VI smoothing parameter:", float(parameters["shp_VI"]["gamma_regu_VI"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Abs. tolerance VI solver:", float(parameters["shp_VI"]["eps_tol_abs_VI"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Rel. tolerance VI solver:", float(parameters["shp_VI"]["eps_tol_rel_VI"]), _collength_print), end="\n"*2)

    # Steklov-Poincaré parameters
    print("{0:-<{1}}".format("---- Steklov-Poincaré parameters ", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Shape gradient rep. metric:", str(parameters["shp_grad_repr"]["LHS_Mode"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("LinElas L2 scaling:", float(parameters["shp_grad_repr"]["StekP_L2"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("LinElas viscosity scaling:", float(parameters["shp_grad_repr"]["StekP_Viscosity"]), _collength_print))
    print("{0:<{2}} {1:<{2}d}".format("pLapl exponent:", int(parameters["shp_grad_repr"]["p_pLapl"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("pLapl regularization parameter:", float(parameters["shp_grad_repr"]["eps_pLapl"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("min. Lamé-parameter:", float(parameters["shp_grad_repr"]["mu_min"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("max. Lamé-parameter:", float(parameters["shp_grad_repr"]["mu_max"]), _collength_print), end="\n"*2)

    # L-BFGS parameters
    print("{0:-<{1}}".format("---- L-BFGS parameters ", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("L-BFGS (False: gradient descent):", parameters["shp_lbfgs"]["L_BFGS"] == "True", _collength_print))
    print("{0:<{2}} {1:<{2}d}".format("Memory length:", int(parameters["shp_lbfgs"]["memory_length"]), _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("exit if curv. cond. unfulfilled:", parameters["shp_lbfgs"]["curv_break"] == "True", _collength_print), end="\n"*2)

    # Linesearch parameters
    print("{0:-<{1}}".format("---- Line search parameters ", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Backtracking line search:", parameters["shp_linesearch"]["Linesearch"] == "True", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Search direction normalization:", parameters["shp_linesearch"]["Linesearch_normed"] == "True", _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Backtracking rescale factor:", float(parameters["shp_linesearch"]["shrinkage"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Backtracking initial scaling:", float(parameters["shp_linesearch"]["start_scale"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Armijo condition factor:", float(parameters["shp_linesearch"]["armijo_factor"]), _collength_print), end="\n"*2)

    # PreShape regularization parameters
    print("{0:-^{1}}".format(" PRESHAPE REGULARIZATION PARAMETERS ", 2*_collength_print))
    print("{0:-<{1}}".format("---- Preshape regularization parameters ", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Preshape reg. mode:", str(parameters["prshp_main"]["Mode"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Main shape obj. scaling:", float(parameters["prshp_main"]["alpha_main"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Shape mesh reg. scaling:", float(parameters["prshp_main"]["alpha_shape"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Volume mesh reg. scaling:", float(parameters["prshp_main"]["alpha_volume"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Outer holdall scaling:", float(parameters["prshp_main"]["boundary_magnification"]), _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Shape mesh reg. target:", str(parameters["prshp_main"]["q_ext_str"]), _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Volume mesh reg. target:", str(parameters["prshp_main"]["q_vol_str"]), _collength_print), end="\n"*2)

    # Preshape target via signed distance
    # PreShape Tracking Targets for Submanifolds via signed distance    print("{0:-<{1}}".format("---- Preshape volume target via stabilized signed distance ", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("vol. target via stab. signed distance:", parameters["prshp_sign_dist"]["VolTr_signed_dist"] == "True", _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("sign. dist. stabilization param.:", float(parameters["prshp_sign_dist"]["eps_stab"]), _collength_print))
    # True: stab_param = eps_stab*MeshData.mesh.hmax(), False: stab_param = eps_stab
    print("{0:<{2}} {1:<{2}.4E}".format("sign. dist. dynamic stabilization param.:", parameters["prshp_sign_dist"]["eps_stab_dyn"] == "True", _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("transformation parameter alpha:", float(parameters["prshp_sign_dist"]["alpha_target_tr"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("transformation parameter beta:", float(parameters["prshp_sign_dist"]["beta_target_tr"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("transformation parameter location:", float(parameters["prshp_sign_dist"]["location_target_tr"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("transformation parameter scaling:", float(parameters["prshp_sign_dist"]["scaling_target_tr"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("transformation parameter eps:", float(parameters["prshp_sign_dist"]["eps_target_tr"]), _collength_print), end="\n"*2)

    # Alternating preshape subroutine
    print("{0:-<{1}}".format("---- Alternating preshape volume subroutine ", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("alternating preshape volume subroutine:", parameters["prshp_subr"]["VolOpt_substep"] == "True", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Preshape subroutine mode:", str(parameters["prshp_subr"]["Mode_vol_subr"]), _collength_print))
#    print("{0:<{2}} {1:<{2}.4E}".format("Abs. vol. subr. tolerance:", float(parameters["prshp_subr"]["tol_vol_subr_abs"]), _collength_print))
#    print("{0:<{2}} {1:<{2}.4E}".format("Rel. vol. subr. tolerance:", float(parameters["prshp_subr"]["tol_vol_subr_rel"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Subroutine trigger level:", float(parameters["prshp_subr"]["tol_vol_subr_trigger"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Subroutine baseline level:", float(parameters["prshp_subr"]["tol_vol_subr_baseline"]), _collength_print))
    print("{0:<{2}} {1:<{2}d}".format("Maximum subroutine iterations:", int(parameters["prshp_subr"]["max_iter_vol"]), _collength_print), end="\n"*2)

    # Alternating preshape subroutine gradient repr.
    print("{0:-<{1}}".format("---- Subroutine preshape gradient repr. ", _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Subr. LinElas L2 scaling:", float(parameters["prshp_subr_grad_repr"]["StekP_L2_vol"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Subr. LinElas viscosity scaling:", float(parameters["prshp_subr_grad_repr"]["StekP_Viscosity_vol"]), _collength_print), end="\n"*2)

    # Subroutine line search parameters
    print("{0:-<{1}}".format("---- PreShape subroutine line search parameters ", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Subr. Backtracking line search:", parameters["prshp_subr_linesearch"]["Linesearch_vol"] == "True", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Subr. Search direction normalization:", parameters["prshp_subr_linesearch"]["Linesearch_vol_normed"] == "True", _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Subr. Backtracking rescale factor:", float(parameters["prshp_subr_linesearch"]["shrinkage_vol"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Subr. Backtracking initial scaling:", float(parameters["prshp_subr_linesearch"]["start_scale_vol"]), _collength_print))
    print("{0:<{2}} {1:<{2}.4E}".format("Subr. Armijo condition factor:", float(parameters["prshp_subr_linesearch"]["armijo_factor_vol"]), _collength_print), end="\n"*2)

    # Subroutine L-BFGS parameters
    print("{0:-<{1}}".format("---- Subroutine L-BFGS parameters ", _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Subr. L-BFGS (False: gradient descent):", parameters["prshp_subr_lbfgs"]["L_BFGS_vol"] == "True", _collength_print))
    print("{0:<{2}} {1:<{2}d}".format("Subr. Memory length:", int(parameters["prshp_subr_lbfgs"]["memory_length_vol"]), _collength_print))
    print("{0:<{2}} {1!s:<{2}}".format("Subr. exit if curv. cond. unfulfilled:", parameters["prshp_subr_lbfgs"]["curv_break_vol"] == "True", _collength_print))
    print("-"*2*_collength_print + "\n")

    return None


def create_outputfolder() -> str:
    """Creates new outputfolder for saved data of the current run.


    Returns:
        outputfolder: String of folder name in the format (year)(month)(day)_(hour)(minute)(second),
            placed in the folder __this_files_dir/Output.
    """
    try:
        os.mkdir('Output')
    except OSError as exc:
        if exc.errno != errno.EEXIST:
            raise exc
        pass

    # Create outputfolder in format (year)(month)(day)_(hour)(minute)(second)
    outputfolder = os.path.join(__this_files_dir,
                                'Output',
                                datetime.now().strftime('%Y%m%d_%H%M%S'))
    os.mkdir(outputfolder)

    # Copy initial parameters of current execution into output directory
    copyfile(os.path.join(__this_files_dir, "config.ini"),
             os.path.join(outputfolder, "config_"+datetime.now().strftime('%Y%m%d_%H%M%S')+".ini"))

    return outputfolder


def load_mesh(name: str, perturbation: tp.List[str]) -> MeshData:
    """
    Initializes instance of the MeshData class from Gmesh created xml files via
    Dolfin, which are placed in __this_files_dir/Meshes.
    To create these, use a newer Gmsh versions and FEniCS 16:
        gmsh -2 Name.geo -format msh2 -clscale (double)
        dolfin-convert Name.xml


    Args:
        name: String of name.xml files used as meshes.
        perturation: List of 2 strings for FEniCS construction of expression to
            perturb the nodes (e.g. ["0.025*sin(25.5*x[0])", "0."]).

    Returns:
        meshdata: Instance of MeshData.
    """
    # Pfad zur speziellen Gitter Datei
    path_meshFile = os.path.join(DATA_DIR, name)

    try:
        # Erstelle FEniCS Mesh mit Subdomains und Boundaries
        mesh = fe.Mesh(path_meshFile + ".xml")
        subdomains = fe.MeshFunction("size_t", mesh,
                                     path_meshFile + "_physical_region.xml")
        boundaries = fe.MeshFunction("size_t", mesh,
                                     path_meshFile + "_facet_region.xml")
    except Exception as ex:
        print("Error loading FEniCS mesh from files!")
        raise ex
    else:
        # initialize meshdata class
        meshdata = MeshData(mesh, subdomains, boundaries, ind_update=False)

    # perturb mesh
    if not perturbation == ["0.", "0."]:
        V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1, dim=2)
        try:
            movement = fe.Expression((perturbation[0], perturbation[1]), degree=1, domain=meshdata.mesh)
            perturbation_func = fe.project(movement, V_vec)
        except Exception as ex:
            print("Error in bib.load_mesh: perturbation must be list of 2 strings suitable for FEniCS expressions!")
            raise ex
        else:
            interface_inds = get_outer_boundary_verts(meshdata)
            v2d = fe.dolfin.vertex_to_dof_map(V_vec)
            vals = perturbation_func.vector().get_local()
            # fix outer boundaries during perturbation
            vals[v2d[2*interface_inds]] = 0.
            vals[v2d[2*interface_inds+1]] = 0.
            perturbation_func.vector()[:] = vals

            fe.ALE.move(meshdata.mesh, perturbation_func)

    # Rueckgabe als MeshData Objekt
    return meshdata


def load_mesh_xdmf(mesh_name: str, perturbation: tp.List[str]) -> MeshData:
    """Loads MeshData instance from .xdmf files in Meshes folder.


    Args:
        mesh_name: String of name of mesh files (i.e. mesh.xdmf).
        perturbation: List of 2 strings for FEniCS construction of expression
            to perturb the nodes (e.g. ["0.025*sin(25.5*x[0])", "0."]).


    Returns:
        meshdata: Instance of MeshData.
    """
    try:
        # create FEniCS mesh
        mesh = fe.Mesh()
        with fe.dolfin.XDMFFile(os.path.join(__this_files_dir, "Meshes/" + mesh_name + ".xdmf")) as infile:
            infile.read(mesh)

        # create FEniCS face-function
        faces_function = fe.MeshFunction("size_t", mesh, 1)
        with fe.dolfin.XDMFFile(os.path.join(__this_files_dir, "Meshes/" + mesh_name + "_faces.xdmf")) as infile:
            infile.read(faces_function)

        # create FEniCS subdomains-function
        subdomain_function = fe.MeshFunction("size_t", mesh, 2)
        with fe.dolfin.XDMFFile(os.path.join(__this_files_dir, "Meshes/" + mesh_name + "_subdomains.xdmf")) as infile:
            infile.read(subdomain_function)
    except Exception as ex:
        print("Error loading FEniCS mesh from files!")
        raise ex
    else:
        # initialize meshdata
        meshdata = MeshData(mesh, subdomain_function, faces_function, ind_update=False)

    # perturb mesh
    if not perturbation == ["0.", "0."]:
        V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1, dim=2)
        try:
            movement = fe.Expression((perturbation[0], perturbation[1]), degree=1, domain=meshdata.mesh)
            perturbation_func = fe.project(movement, V_vec)
        except Exception as ex:
            print("Error in bib.load_mesh: perturbation must be list of 2 strings suitable for FEniCS expressions!")
            raise ex
        else:
            interface_inds = get_outer_boundary_verts(meshdata)
            v2d = fe.dolfin.vertex_to_dof_map(V_vec)
            vals = perturbation_func.vector().get_local()
            # fix outer boundaries during perturbation
            vals[v2d[2*interface_inds]] = 0.
            vals[v2d[2*interface_inds+1]] = 0.
            perturbation_func.vector()[:] = vals

            fe.ALE.move(meshdata.mesh, perturbation_func)

    return meshdata


def get_outer_boundary_verts(meshdata: MeshData) -> np.ndarray:
    """
    Inputs:
        meshdata: Instance of MeshData, from which outer boundary nodes are
            extracted.


    Returns:
        interface_indizes: List of indices of outer boundary nodes of the
            hold-all domain. These are tagged as {1, 2, 3, 4}, and indices
            are invariant to deformation via ALE.deform
    """
    # Vertex-Indizes des Interface bekommen, bleiben invariant bei Deformation
    ind_boundary_facets = []
    for i in range(0, len(meshdata.boundaries)):
        if meshdata.boundaries[i] < 5 and meshdata.boundaries[i] > 0:
            ind_boundary_facets.append(i)

    ind_boundary_facets = list(set(ind_boundary_facets))

    ind_boundary_vertices = []
    for c in fe.cells(meshdata.mesh):
        for f in fe.facets(c):
            if f.index() in ind_boundary_facets:
                for v in fe.vertices(f):
                    ind_boundary_vertices.append(v.index())

    interface_indizes = np.array(list(set(ind_boundary_vertices)))

    return interface_indizes


def order_boundary_verts(meshdata: MeshData) -> tp.List[int]:
    """
    Returns:
        ordered_interface_indizes: List of ordered boundary nodes of the shape.
            Shape is marked via tags {5, 6}. This function uses that shape has
            dimension 1, and that it is closed, and thus that each node has
            2 neighbors.
    """
    # Collect node indices of interface
    ind_boundary_facets = []
    for i in range(0, len(meshdata.boundaries)):
        if meshdata.boundaries[i] > 4:
            ind_boundary_facets.append(i)

    ind_boundary_facets = list(set(ind_boundary_facets))

    ind_boundary_vertices = []
    for c in fe.cells(meshdata.mesh):
        for f in fe.facets(c):
            if f.index() in ind_boundary_facets:
                for v in fe.vertices(f):
                    ind_boundary_vertices.append(v.index())

    interface_indizes = list(set(ind_boundary_vertices))

    # Order interface indices with shape mesh connectivity, i.e. neighbors in
    # the array are neighbors in the geometric shape mesh.
    ordered_interface_indizes = []
    ordered_interface_indizes.append(interface_indizes[0])

    i = 0
    while len(ordered_interface_indizes) < len(interface_indizes):
        v = fe.Vertex(meshdata.mesh, ordered_interface_indizes[i])
        local_connectivity = []
        for f in fe.facets(v):
            # Can at most have 2 elements with 2D meshes, since shape is 1D.
            # Delete the node at which we are iterating currently.
            if ordered_interface_indizes[i] in f.entities(0):
                f_erased = f.entities(0).tolist()
                del f_erased[f_erased.index(ordered_interface_indizes[i])]
                local_connectivity.append(f_erased[0])
            if len(local_connectivity) >= 2:
                break
        # the first node has 2 possible neighbors
        if i == 0:
            ordered_interface_indizes.append(local_connectivity[0])
        else:
            # neighbor is already in the list, choose the other
            if local_connectivity[0] in ordered_interface_indizes:
                ordered_interface_indizes.append(local_connectivity[1])
            else:
                ordered_interface_indizes.append(local_connectivity[0])
        i += 1

    return ordered_interface_indizes


def __get_index_not_interior_boundary(mesh: fe.Mesh, subdomains: fe.MeshFunction, boundaries: fe.MeshFunction, interior: bool = True) -> tp.Union[tp.List[int], np.ndarray]:
    """
    Returns node indices of elements without support on interior boundary.
    If interior == False, the return is a list of indices of nodes on the interior
    boundary. Do not mistake these for DoF indices!
    """
    # Collect facet indices of interior boundary via tags {5, 6}.
    ind_interior_boundary_facets = []
    for i in range(0, len(boundaries)):
        if boundaries[i] > 4:
            ind_interior_boundary_facets.append(i)

    # Get node indices of the interior boundary
    ind_interior_boundary_vertices = []
    for c in fe.cells(mesh):
        for f in fe.facets(c):
            if f.index() in ind_interior_boundary_facets:
                for v in fe.vertices(f):
                    ind_interior_boundary_vertices.append(v.index())

    if not interior:
        return ind_interior_boundary_vertices

    ind_interior_boundary_vertices = list(set(ind_interior_boundary_vertices))

    # Get element indices of the interior boundary
    ind_around_interior_boundary_cells = []
    for c in fe.cells(mesh):
        ind = False
        for v in fe.vertices(c):
            if v.index() in ind_interior_boundary_vertices:
                ind = True
        if ind:
            ind_around_interior_boundary_cells.append(c.index())

    # Define a new subdomain
    new_sub = fe.MeshFunction("size_t", mesh, 2)
    new_sub.set_all(0)
    for i in ind_around_interior_boundary_cells:
        if subdomains[i] == 1:
            new_sub[i] = 1
        else:
            new_sub[i] = 2

    # Use testproblem to find indices without support on interior boundary
    V = fe.VectorFunctionSpace(mesh, "CG", 1, dim=2)
    dx_int = fe.Measure('dx', domain=mesh, subdomain_data=new_sub)
    v = fe.TestFunction(V)
    dummy_y = fe.Constant((1.0, 1.0))

    f_elas_int_1 = fe.inner(dummy_y, v)*dx_int(1)
    F_elas_int_1 = fe.assemble(f_elas_int_1)
    f_elas_int_2 = fe.inner(dummy_y, v)*dx_int(2)
    F_elas_int_2 = fe.assemble(f_elas_int_2)

    # Get indices, where values do not influence the system constructed on subdomains
    ind1 = (F_elas_int_1.get_local() == 0.0)
    ind2 = (F_elas_int_2.get_local() == 0.0)

    ind = ind1 | ind2

    return ind


def mesh_distance(meshdata1: MeshData, meshdata2: MeshData) -> float:
    """Computes meshdistance of two shapes in MeshData instances via integrated
    Hadamard style distance.

    Inputs:
        meshdata1: MeshData instance with shape, to which distance is computed.
        meshdata2: MeshData instance with shape, from which distance is computed.


    Returns:
        value: Integrated Hadamard style distance. It is computed via the formula
            integrate_mesh2 (min_nodes_mesh1 (dist(node_mesh1, S_mesh2)) dS_mesh2
    """
    # Node indices of shape meshes
    boundary_index_mesh1 = meshdata1.indBoundary
    boundary_index_mesh2 = meshdata2.indBoundary

    # Compute distance of all nodes of mesh1 to a fixed node of mesh2,
    # then take minimum and add to list.
    distance_list_vertex = np.zeros(meshdata2.mesh.num_vertices())
    for i in boundary_index_mesh2:
        temp_distance_list = []
        for j in boundary_index_mesh1:
            local_dist = np.linalg.norm(meshdata2.mesh.coordinates()[i] - meshdata1.mesh.coordinates()[j])
            temp_distance_list.append(local_dist)

        dist = np.amin(temp_distance_list)
        distance_list_vertex[i] = dist

    # define a function on mesh2 with distance values on the shape boundary
    V = fe.FunctionSpace(meshdata2.mesh, "CG", 1)
    distance_function = fe.Function(V)
    v2d = fe.dolfin.vertex_to_dof_map(V)

    # Translate vertex indices to DoF indices
    distance_list_dof = np.zeros(len(distance_function.vector().get_local()))
    for i in boundary_index_mesh2:
        distance_list_dof[v2d[i]] = distance_list_vertex[i]

    # define function on the shape
    distance_function.vector()[:] = distance_list_dof

    # compute integral on mesh2 by integrating distance function
    dS = fe.Measure('dS', subdomain_data=meshdata2.boundaries)
    distance_integral = distance_function('+')*dS(5) + distance_function('+')*dS(6)
    value = fe.assemble(distance_integral)

    return value


def targetfunction(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, y_z: fe.Function, deformation: fe.Function, psi: fe.Expression) -> float:
    """
    Computes value of tracking type shape functional with state constraint,
    and perimeter regularization. Target functional is computed according to

    (alpha_main/2.)*|| y - y_z ||_L2_volmesh**2 + perimeter_regu*(1. dS_shape),

    where y is the state equation on the mesh after (!) deformation according
    to FEniCS vector field deformation. States can be elliptic PDE or VIs.
    If deformation is the zero vector field, values are computed on the current mesh.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        y_z: FEniCS function representing target data.
        deformation: FEniCS vector field used to deform the mesh, and then
            calculate the target functional value on the deformed mesh.
            If chosen zero, target functional value is computed on meshdata.
        psi: FEniCS expression of the obstacle.


    Returns:
        value: Target functional value according to the above formula.
    """
    # Parameter unpacking
    perimeter_regu = float(parameters["shp_main"]["perimeter_regu"])
    alpha_main = float(parameters["prshp_main"]["alpha_main"])
    vari_ineq = parameters["shp_VI"]["Vari_Ineq"] == "True"

    # create local deep copy of mesh
    msh = fe.Mesh(meshdata.mesh)
    sbd = fe.MeshFunction("size_t", msh, 2)
    sbd.set_values(meshdata.subdomains.array())
    bnd = fe.MeshFunction("size_t", msh, 1)
    bnd.set_values(meshdata.boundaries.array())
    local_mesh = MeshData(msh, sbd, bnd, ind_update=True)
    local_mesh.indBoundary = meshdata.indBoundary
    local_mesh.indBoundaryFaces = meshdata.indBoundaryFaces
    local_mesh.indNotIntBoundary = meshdata.indNotIntBoundary
    local_mesh.indOuterBoundary = meshdata.indOuterBoundary
    local_mesh.indCornerNodes = meshdata.indCornerNodes

    # deform mesh
    fe.ALE.move(local_mesh.mesh, deformation)

    # Compute state equation on deformed mesh
    if vari_ineq:
        y = solve_state_gamma_regu(parameters, local_mesh, psi)
    else:
        y = solve_state(parameters, local_mesh)

    # Assemble target functional
    V = fe.FunctionSpace(local_mesh.mesh, 'P', 1)
    z = fe.project(y_z, V)

    j = 1./2.*fe.norm(fe.project(y-z, V), 'L2', local_mesh.mesh)**2

    ds = fe.Measure('dS', subdomain_data=local_mesh.boundaries)
    ones = fe.Function(V)
    ones.vector()[:] = 1.
    j_reg_integral = ones*ds(5) + ones*ds(6)
    j_reg = perimeter_regu*fe.assemble(j_reg_integral)

    J = alpha_main*j + j_reg

    return J


def shape_deriv(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, p: fe.Function, y: fe.Function, z: fe.Function, psi: fe.Expression, V: fe.dolfin.Argument) -> tp.Tuple[ufl.Form, ufl.Form]:
    """Computes shape derivative of the target functional in direction V.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        p: Adjoint according to state equation and objective functional on meshdata.
        y: State equation solution on meshdata.
        z: Projection of FEniCS function representing target data onto meshdata.
        psi: FEniCS expression of the obstacle.
        V: FEniCS test function to assemble shape derivative.


    Returns:
        (fe.Constant(alpha_main)*Dj, Dj_reg)
        fe.Constant(alpha_main)*Dj: ufl form of shape derivative of tracking type functional
        Dj_reg: ufl form of shape derivative of perimeter regularization
    """
    # Parameter unpacking
    f1 = fe.Constant(float(parameters["shp_main"]["pde_source_term_1"]))
    f2 = fe.Constant(float(parameters["shp_main"]["pde_source_term_2"]))
    perimeter_regu = float(parameters["shp_main"]["perimeter_regu"])
    alpha_main = float(parameters["prshp_main"]["alpha_main"])
    c_regu_VI = float(parameters["shp_VI"]["c_regu_VI"])
    gamma_regu_VI = float(parameters["shp_VI"]["gamma_regu_VI"])
    vari_ineq = parameters["shp_VI"]["Vari_Ineq"] == "True"

    dx = fe.Measure('dx', domain=meshdata.mesh, subdomain_data=meshdata.subdomains)
    dS = fe.Measure('dS', subdomain_data=meshdata.boundaries)

    epsilon_V = fe.Constant(2.)*fe.sym(fe.nabla_grad(V))
    n = fe.FacetNormal(meshdata.mesh)

    # define ufl form of tracking type shape functional for VI or PDE case
    if vari_ineq:
        V_space = fe.FunctionSpace(meshdata.mesh, "CG", 1)
        psi_V = fe.project(psi, V_space)
        lmbda_bar = initialize_lambda_bar(parameters, meshdata, psi)

        # compute max_gamma and sign_gamma
        affine_comb = lmbda_bar + c_regu_VI*(y-psi)
        max_gamma = fe.conditional(fe.le(affine_comb, -(1./gamma_regu_VI)), 0,
                                   fe.conditional(fe.lt(affine_comb, (1./gamma_regu_VI)), 0.25*gamma_regu_VI*(affine_comb)**2
                                                  + 0.5*affine_comb + 1./(4.*gamma_regu_VI), affine_comb)
                                   )
        max_gamma = fe.project(max_gamma, V_space)
        sign_gamma = fe.conditional(fe.le(affine_comb, -(1./gamma_regu_VI)), 0.,
                                    fe.conditional(fe.lt(affine_comb, (1./gamma_regu_VI)), 0.5*gamma_regu_VI*affine_comb + 0.5, 1.)
                                    )
        sign_gamma = fe.project(sign_gamma, V_space)

        # gradient of f is 0, since f is piecewise constant
        Dj = (-(y-z)*fe.inner(V, fe.grad(z)) - fe.inner(fe.grad(y), fe.dot(epsilon_V, fe.grad(p))))*dx('everywhere') \
            + sign_gamma*p*fe.inner(fe.grad(lmbda_bar) - c_regu_VI*fe.grad(psi_V), V)*dx('everywhere') \
            - fe.nabla_div(V)*f1*p*dx(1) - fe.nabla_div(V)*f2*p*dx(2) \
            + fe.nabla_div(V)*(0.5*(y-z)*(y-z) + fe.inner(fe.grad(y), fe.grad(p)) + max_gamma*p)*dx

    # gradient of f is 0, since f is piecewise constant
    else:
        Dj = (- fe.inner(fe.grad(y), fe.dot(epsilon_V, fe.grad(p)))*dx('everywhere')
              + fe.nabla_div(V)*(fe.Constant(0.5)*(y-z)*(y-z) + fe.inner(fe.grad(y), fe.grad(p)))*dx('everywhere')
              - fe.nabla_div(V)*f1*p*dx(1) - fe.nabla_div(V)*f2*p*dx(2))

    # define ufl form for perimeter regularization
    Dj_reg = fe.Constant(perimeter_regu)*(
            (fe.nabla_div(V('+')) - fe.inner(fe.dot(fe.nabla_grad(V('+')), n('+')), n('+')))*dS(5)
            + (fe.nabla_div(V('+')) - fe.inner(fe.dot(fe.nabla_grad(V('+')), n('+')), n('+')))*dS(6)
                                          )

    return fe.Constant(alpha_main)*Dj, Dj_reg


def solve_state(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData) -> fe.Function:
    """Solves state equation without variational inequality/ obstacle.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).


    Returns:
        y: FEniCS function representing the state equation solution.
    """
    # Parameters unpacking
    f1 = fe.Constant(float(parameters["shp_main"]["pde_source_term_1"]))
    f2 = fe.Constant(float(parameters["shp_main"]["pde_source_term_2"]))

    V = fe.FunctionSpace(meshdata.mesh, "P", 1)

    # Convergence criterion
    zero_function = fe.Expression("0.0", degree=1)
    abort = fe.Function(V)
    abort.interpolate(zero_function)

    # set Dirichlet boundaries
    y_out = fe.Constant(0.0)
    bcs = [fe.DirichletBC(V, y_out, meshdata.boundaries, i) for i in range(1, 5)]

    # define problem
    dx = fe.Measure('dx', domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    y = fe.TrialFunction(V)
    v = fe.TestFunction(V)

    a = fe.inner(fe.grad(y), fe.grad(v))*dx('everywhere')
    b = f1*v*dx(1) + f2*v*dx(2)('everywhere')

    # solve state equation and return solution
    y = fe.Function(V)
    fe.solve(a == b, y, bcs)

    return y


def solve_adjoint(meshdata: MeshData, y: fe.Function, z: fe.Function) -> fe.Function:
    """Solves adjoint equation of problem without variational inequality/ obstacle.


    Inputs:
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        y: State equation solution on meshdata, output from solve_state.
        z: Projection of FEniCS function representing target data onto meshdata.


    Returns:
        p: FEniCS function representing the adjoint equation solution.
    """
    V = fe.FunctionSpace(meshdata.mesh, "P", 1)

    # set Dirichlet boundary conditions
    p_out = fe.Constant(0.0)
    bcs = [fe.DirichletBC(V, p_out, meshdata.boundaries, i) for i in range(1, 5)]

    # define variational problem
    dx = fe.Measure('dx', domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    p = fe.TrialFunction(V)
    v = fe.TestFunction(V)

    lhs = fe.inner(fe.grad(p), fe.grad(v))*dx
    rhs = -y*v*dx + z*v*dx

    # Solve adjoint equation and return solution
    p = fe.Function(V)
    fe.solve(lhs == rhs, p, bcs)

    return p


def calc_lame_par(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData) -> fe.Function:
    """Computes the locally varying Lamé-parametes for the linear elasticity
    (pre-)shape gradient representations.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).


    Returns:
        mu_elas: FEniCS function representing locally varying Lamé-parametes.
            These are a solution to the Laplace equation with constant Dirichlet
            boundary conditions:
            mu_elas == mu_min on hold-all_outer_boundary (tags: {1, 2, 3, 4})
            mu_elas == mu_max on interior shape boundary (tags: {5, 6}).
    """
    # Parameters unpacking
    mu_min_value = float(parameters["shp_grad_repr"]["mu_min"])
    mu_max_value = float(parameters["shp_grad_repr"]["mu_max"])

    V = fe.FunctionSpace(meshdata.mesh, "P", 1)

    # Set Dirichlet boundary conditions
    mu_min = fe.Constant(mu_min_value)
    mu_max = fe.Constant(mu_max_value)
    bcs = ([fe.DirichletBC(V, mu_min, meshdata.boundaries, i) for i in range(1, 5)]
           + [fe.DirichletBC(V, mu_max, meshdata.boundaries, i) for i in range(5, 7)])

    # define Laplace problem
    dx = fe.Measure('dx', domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    mu_elas = fe.TrialFunction(V)
    v = fe.TestFunction(V)

    f = fe.Expression("0.0", degree=1)

    lhs = fe.inner(fe.grad(mu_elas), fe.grad(v))*dx
    rhs = f*v*dx

    # compute solution and return
    mu_elas = fe.Function(V)
    fe.solve(lhs == rhs, mu_elas, bcs)

    return mu_elas


def bilin_a(meshdata: MeshData, U: fe.Function, V: fe.Function, mu_elas: fe.Function) -> float:
    """Bilinear form representing weak linear elasticity for (pre-)shape gradient
    representations.


    Inputs:
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        U: FEniCS function representing the (pre-)shape derivative.
        V: FEniCS function representing the test function or direction.
        mu_elas: Lamé-parameters from calc_lame_par.


    Returns:
        value: Value of the assembled bilinear form a(U, V).
    """
    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    epsilon_V = (1./2.)*fe.sym(fe.nabla_grad(V))
    sigma_U = mu_elas*fe.sym(fe.nabla_grad(U))

    a = fe.inner(sigma_U, epsilon_V)*dx('everywhere')
    value = fe.assemble(a)

    return value


def bilin_ParamTracking_surface_simplified(meshdata: MeshData, U: fe.Function, V: fe.Function, q_ext: fe.Function) -> float:
    """
    Computes value of preshape Hessian of parameterization tracking functional
    in solutions. This bilinear form is positive-semidefinite, but not correct
    outside of solutions.
    """
    dS = fe.Measure("dS", domain=meshdata.mesh, subdomain_data=meshdata.boundaries)
    normal = fe.FacetNormal(meshdata.mesh)

    f_normalizing = 1./fe.assemble(q_ext('+')*(dS(5)+dS(6)))
    f_target = f_normalizing*q_ext

    fdiv_tang_U = fe.nabla_div(f_target('+')*U('+')) - fe.inner(fe.dot(fe.grad(f_target('+')*U('+')), normal('+')), normal('+'))
    fdiv_tang_V = fe.nabla_div(f_target('+')*V('+')) - fe.inner(fe.dot(fe.grad(f_target('+')*V('+')), normal('+')), normal('+'))

    a = fe.inner(fdiv_tang_U, fdiv_tang_V)*(dS(5)+dS(6))
    value = fe.assemble(a)

    return value


def bilin_ParamTracking_vol_simplified(meshdata: MeshData, U: fe.Function, V: fe.Function, q_ext: fe.Function) -> float:
    """
    Computes value of preshape Hessian of parameterization tracking functional
    in solutions via f-divergences. This bilinear form is positive-semidefinite,
    but not correct outside of solutions.
    """
    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    f_normalizing = 1./fe.assemble(q_ext*dx('everywhere'))
    f_target = f_normalizing*q_ext

    fdiv_U = fe.nabla_div(f_target*U)
    fdiv_V = fe.nabla_div(f_target*V)

    a = fe.inner(fdiv_U, fdiv_V)*dx('everywhere')
    value = fe.assemble(a)

    return value


def bfgs_step(meshdata: MeshData, memory: BFGSMemory, mu_elas: fe.Function, q_target: np.ndarray, stekp_viscosity: float, stekp_l2: float) -> fe.Function:
    """Computes a step via the two-loop shape L-BFGS algorithm.


    Inputs:
        meshdata: MeshData instance of the current L-BFGS iteration.
        memory: BFGSMemory instance with the L-BFGS history.
        mu_elas: Lamé-parametes from calc_lame_par for bilinear form
        q_target: DoF values of FEniCS vector field, onto which the approximate
            inverse of the Hessian is applied to (usually gradients).
        stekp_viscosity: Scaling for linear elasticity component of bilinear form
            set in config.ini.
        stekp_l2: Scaling for zero order terms in bilinear form set in config.ini.


    Returns:
        q: Result of the two-loop shape L-BFGS method, i.e. q = Hess_approx^-1 q_target.
    """
    if isinstance(meshdata, MeshData):
        pass
    else:
        raise SystemExit("bfgs_step needs instance of MeshData class as input!")

    if isinstance(memory, BFGSMemory):
        pass
    else:
        raise SystemExit("bfgs_step needs instance of BFGSMemory class as input!")

    V = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1, dim=2)
    q = fe.Function(V)
    q.vector()[:] = q_target
    alpha = np.zeros(memory.length)

    diff_grad = fe.Function(V)
    first_diff_grad = fe.Function(V)

    if memory.step_nr + 1 >= memory.length:
        # for full memory, all entries of the history are used
        for i in range(memory.length-1):
            # forward loop
            i = i+1
            diff_grad.vector()[:] = (memory.initialize_grad(meshdata, i-1).vector() - memory.initialize_grad(meshdata, i).vector())
            alpha[i] = bilin_a_preshape(meshdata, memory.initialize_defo(meshdata, i-1), q, mu_elas, stekp_viscosity, stekp_l2) / bilin_a_preshape(meshdata, diff_grad, memory.initialize_defo(meshdata, i-1), mu_elas, stekp_viscosity, stekp_l2)
            q.vector()[:] = q.vector() - float(alpha[i])*diff_grad.vector()
        # rescaling of q
        first_diff_grad.vector()[:] = (memory.initialize_grad(meshdata, 0).vector() - memory.initialize_grad(meshdata, 1).vector())
        gamma = bilin_a_preshape(meshdata, first_diff_grad, memory.initialize_defo(meshdata, 0), mu_elas, stekp_viscosity, stekp_l2) / bilin_a_preshape(meshdata, first_diff_grad, first_diff_grad, mu_elas, stekp_viscosity, stekp_l2)
        q.vector()[:] = gamma*q.vector()
        for i in range(memory.length-1):
            # backward loop
            i = i+1
            diff_grad.vector()[:] = (memory.initialize_grad(meshdata, -(i+1)).vector() - memory.initialize_grad(meshdata, -i).vector())
            beta = bilin_a_preshape(meshdata, diff_grad, q, mu_elas, stekp_viscosity, stekp_l2) / bilin_a_preshape(meshdata, diff_grad, memory.initialize_defo(meshdata, -(i+1)), mu_elas, stekp_viscosity, stekp_l2)
            q.vector()[:] = q.vector() + (float(alpha[-i]) - beta)*memory.initialize_defo(meshdata, -(i+1)).vector()
    elif memory.step_nr == 0:
            # the first L-BFGS step is a negative gradient step, no memory yet
            q.vector()[:] = -1.*q.vector().get_local()
            return q
    else:
        # if memory not full, only the yet computed entries are used
        for i in range(memory.step_nr):
            # forward loop
            i = i+1
            diff_grad.vector()[:] = (memory.initialize_grad(meshdata, i-1).vector() - memory.initialize_grad(meshdata, i).vector())
            alpha[i] = bilin_a_preshape(meshdata, memory.initialize_defo(meshdata, i-1), q, mu_elas, stekp_viscosity, stekp_l2) / bilin_a_preshape(meshdata, diff_grad, memory.initialize_defo(meshdata, i-1), mu_elas, stekp_viscosity, stekp_l2)
            q.vector()[:] = q.vector() - float(alpha[i])*diff_grad.vector()
        # rescaling of q
        first_diff_grad.vector()[:] = (memory.initialize_grad(meshdata, 0).vector() - memory.initialize_grad(meshdata, 1).vector())
        gamma = bilin_a_preshape(meshdata, first_diff_grad, memory.initialize_defo(meshdata, 0), mu_elas, stekp_viscosity, stekp_l2) / bilin_a_preshape(meshdata, first_diff_grad, first_diff_grad, mu_elas, stekp_viscosity, stekp_l2)
        q.vector()[:] = gamma*q.vector()
        for i in range(memory.step_nr):
            # backward loop
            shift = (memory.length-1) - memory.step_nr
            i = i+1
            diff_grad.vector()[:] = (memory.initialize_grad(meshdata, -(i+1)-shift).vector() - memory.initialize_grad(meshdata, -i-shift).vector())
            beta = bilin_a_preshape(meshdata, diff_grad, q, mu_elas, stekp_viscosity, stekp_l2) / bilin_a_preshape(meshdata, diff_grad, memory.initialize_defo(meshdata, -(i+1)-shift), mu_elas, stekp_viscosity, stekp_l2)
            q.vector()[:] = q.vector() + (float(alpha[-i-shift]) - beta)*memory.initialize_defo(meshdata, -(i+1)-shift).vector()

    q.vector()[:] = -1.*q.vector()
    return q


def bfgs_step_preshape_vol(meshdata: MeshData, memory: BFGSMemory, q_ext: fe.Function, q_target: fe.Function) -> fe.Function:
    """Computes a step via the two-loop shape L-BFGS algorithm and bilinear forms
    from the preshape parameterization tracking type approximate Hessians.


    Inputs:
        meshdata: MeshData instance of the current L-BFGS iteration.
        memory: BFGSMemory instance with the L-BFGS history.
        mu_elas: Lamé-parametes from calc_lame_par for bilinear form
        q_target: DoF values of FEniCS vector field, onto which the approximate
            inverse of the Hessian is applied to (usually gradients).
        stekp_viscosity: Scaling for linear elasticity component of bilinear form
            set in config.ini.
        stekp_l2: Scaling for zero order terms in bilinear form set in config.ini.


    Returns:
        q: Result of the two-loop shape L-BFGS method, i.e. q = Hess_approx^-1 q_target.
    """
    if isinstance(meshdata, MeshData):
        pass
    else:
        raise SystemExit("bfgs_step needs instance of MeshData class as input!")

    if isinstance(memory, BFGSMemory):
        pass
    else:
        raise SystemExit("bfgs_step needs instance of BFGSMemory class as input!")

    V = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1, dim=2)
    q = fe.Function(V)
    q.vector()[:] = q_target
    alpha = np.zeros(memory.length)

    diff_grad = fe.Function(V)
    first_diff_grad = fe.Function(V)

    if memory.step_nr + 1 >= memory.length:
        # for full memory, all entries of the history are used
        for i in range(memory.length-1):
            # forward loop
            i = i+1
            diff_grad.vector()[:] = (memory.initialize_grad(meshdata, i-1).vector() - memory.initialize_grad(meshdata, i).vector())
            alpha[i] = bilin_ParamTracking_vol_simplified(meshdata, memory.initialize_defo(meshdata, i-1), q, q_ext) / bilin_ParamTracking_vol_simplified(meshdata, diff_grad, memory.initialize_defo(meshdata, i-1), q_ext)
            q.vector()[:] = q.vector() - float(alpha[i])*diff_grad.vector()
        # rescaling of q
        first_diff_grad.vector()[:] = (memory.initialize_grad(meshdata, 0).vector() - memory.initialize_grad(meshdata, 1).vector())
        # gamma = bilin_a(meshdata, first_diff_grad, memory.initialize_defo(meshdata, 0), mu_elas) / bilin_a(meshdata, first_diff_grad, first_diff_grad, mu_elas)
        gamma = bilin_ParamTracking_vol_simplified(meshdata, first_diff_grad, memory.initialize_defo(meshdata, 0), q_ext) / bilin_ParamTracking_vol_simplified(meshdata, first_diff_grad, first_diff_grad, q_ext)
        q.vector()[:] = gamma*q.vector()
        for i in range(memory.length-1):
            # backward loop
            i = i+1
            diff_grad.vector()[:] = (memory.initialize_grad(meshdata, -(i+1)).vector() - memory.initialize_grad(meshdata, -i).vector())
            beta = bilin_ParamTracking_vol_simplified(meshdata, diff_grad, q, q_ext) / bilin_ParamTracking_vol_simplified(meshdata, diff_grad, memory.initialize_defo(meshdata, -(i+1)), q_ext)
            q.vector()[:] = q.vector() + (float(alpha[-i]) - beta)*memory.initialize_defo(meshdata, -(i+1)).vector()
    elif memory.step_nr == 0:
            # the first L-BFGS step is a negative gradient step, no memory yet
            q.vector()[:] = -1.*q.vector().get_local()
            return q
    else:
        # if memory not full, only the yet computed entries are used
        for i in range(memory.step_nr):
            # forward loop
            i = i+1
            diff_grad.vector()[:] = (memory.initialize_grad(meshdata, i-1).vector() - memory.initialize_grad(meshdata, i).vector())
            alpha[i] = bilin_ParamTracking_vol_simplified(meshdata, memory.initialize_defo(meshdata, i-1), q, q_ext) / bilin_ParamTracking_vol_simplified(meshdata, diff_grad, memory.initialize_defo(meshdata, i-1), q_ext)
            q.vector()[:] = q.vector() - float(alpha[i])*diff_grad.vector()
        # rescaling of q
        first_diff_grad.vector()[:] = (memory.initialize_grad(meshdata, 0).vector() - memory.initialize_grad(meshdata, 1).vector())
        # gamma = bilin_a(meshdata, first_diff_grad, memory.initialize_defo(meshdata, 0), mu_elas, stekp_viscosity, stekp_l2) / bilin_a(meshdata, first_diff_grad, first_diff_grad, mu_elas, stekp_viscosity, stekp_l2)
        gamma = bilin_ParamTracking_vol_simplified(meshdata, first_diff_grad, memory.initialize_defo(meshdata, 0), q_ext) / bilin_ParamTracking_vol_simplified(meshdata, first_diff_grad, first_diff_grad, q_ext)
        q.vector()[:] = gamma*q.vector()

        for i in range(memory.step_nr):
            # backward loop
            shift = (memory.length-1) - memory.step_nr
            i = i+1
            diff_grad.vector()[:] = (memory.initialize_grad(meshdata, -(i+1)-shift).vector() - memory.initialize_grad(meshdata, -i-shift).vector())
            beta = bilin_ParamTracking_vol_simplified(meshdata, diff_grad, q, q_ext) / bilin_ParamTracking_vol_simplified(meshdata, diff_grad, memory.initialize_defo(meshdata, -(i+1)-shift), q_ext)
            q.vector()[:] = q.vector() + (float(alpha[-i-shift]) - beta)*memory.initialize_defo(meshdata, -(i+1)-shift).vector()

    q.vector()[:] = -1.*q.vector()
    return q


# PRE-SHAPE TECHNIQUES
def normal_vector(meshdata: MeshData) -> fe.Function:
    """Computes outer normal vector field on the shape, which is not normed.


    Inputs:
        meshdata: Instance of MeshData, for whose shape normal vectors are computed.


    Returns:
        n: FEniCS vector function on meshdata.mesh representing the outer
            normal vector field of the respective shape with tags {5, 6}.
    """
    dS = fe.Measure("dS", subdomain_data=meshdata.boundaries)
    V = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1)

    # Informations are collected in a mass matrix
    u = fe.TrialFunction(V)
    v = fe.TestFunction(V)
    n = fe.Function(V)
    fn = fe.FacetNormal(meshdata.mesh)

    lhs = fe.inner(u('+'), v('+'))*dS(5) + fe.inner(u('+'), v('+'))*dS(6)  # TODO: should change indices of the shape to a single tag!
    lhs_ass = fe.assemble(lhs, keep_diagonal=True)

    lhs_ass.ident_zeros()

    # compute values of the outer normal vector
    rhs = fe.inner(fn('-'), v('+'))*dS(5) + fe.inner(fn('-'), v('+'))*dS(6)
    rhs_ass = fe.assemble(rhs)

    fe.solve(lhs_ass, n.vector(), rhs_ass)

    return n


def tangential_vector(meshdata: MeshData) -> fe.Function:
    """Computes a tangential vector field on the shape, which is not normed.


    Inputs:
        meshdata: Instance of MeshData, for whose shape tangential vectors are computed.


    Returns:
        tangential: FEniCS vector function on meshdata.mesh representing a
            tangential vector field of the respective shape with tags {5, 6}.
    """
    V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1)
    tangential = fe.Function(V_vec)

    # compute normal vector field on shape and extract values
    normal = normal_vector(meshdata)
    normal_vals = normal.vector().get_local()
    array_indices = np.array(range(len(normal_vals)))[:int(len(normal_vals)/2)]

    normal_vals_x0 = normal_vals[2*array_indices]
    normal_vals_x1 = normal_vals[2*array_indices + 1]

    # rotate these values to get tangential vector field
    tang_vals = np.dstack((normal_vals_x1, -1.*normal_vals_x0)).flatten()
    tangential.vector()[:] = tang_vals

    return tangential


def normalize_q_int(meshdata: MeshData, q_int: tp.Union[fe.Function, fe.Expression], vol: bool = False) -> fe.Function:
    """Normalizes FEniCS functions.


    Inputs:
        meshdata: Instance of MeshData, with embedded shape (tags {5, 6}).
        q_int: FEniCS function or expression, which is to be normalized.
            Has to be instantiated on meshdata.mesh.
        vol: If true, normalization is taking place w.r.t. the volume mesh.
            Else, normalization is taking place w.r.t. the shape mesh.


    Returns:
        q_int_normed: Normed FEniCS function on meshdata.mesh.
    """
    dS = fe.Measure("dS", domain=meshdata.mesh, subdomain_data=meshdata.boundaries)
    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)
    V_sc = fe.FunctionSpace(meshdata.mesh, "CG", 1)

    if vol:
        nrm_q_int = fe.assemble(q_int*dx)
        q_int_normed = fe.project((1./nrm_q_int)*q_int, V_sc)
    else:
        nrm_q_int = fe.assemble(q_int*(dS(5)+dS(6)))
        q_int_normed = fe.project((1./nrm_q_int)*q_int, V_sc)

    return q_int_normed


def tangential_projection(meshdata: MeshData, f: tp.Union[fe.Function, fe.Expression]) -> fe.Function:
    """Projects FEniCS function onto its tangential component of the shape.


    Inputs:
        meshdata: Instance of MeshData, with embedded shape (tags {5, 6}).
        f: FEniCS function or expression, which is to be projected on tangential
            components. Has to be instantiated on meshdata.mesh.


    Return:
        tang_func: FEniCS function with values of f only in tangential
        direction of the shape of meshdata.
    """
    dS = fe.Measure("dS", subdomain_data=meshdata.boundaries)
    # dx     = fe.Measure("dx", subdomain_data=meshdata.subdomains)
    normal = fe.FacetNormal(meshdata.mesh)
    # normal = normal_vector(meshdata)

    V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1)
    tang_func = fe.Function(V_vec)
    test = fe.TestFunction(V_vec)
    trial = fe.TrialFunction(V_vec)

    lhs = fe.inner(trial('+'), test('+'))*(dS(5) + dS(6))
    # lhs = fe.inner(trial, test)*dx
    lhs = fe.assemble(lhs, keep_diagonal=True)
    lhs.ident_zeros()

    rhs = fe.inner(f('+') - fe.inner(f('+'), normal('+'))*normal('+'), test('+'))*(dS(5) + dS(6))
    # rhs = fe.inner(f - fe.inner(f, normal)*normal, test)*dx
    rhs = fe.assemble(rhs)

    fe.solve(lhs, tang_func.vector(), rhs)

    # set alls values outside the boundary/shape to 0
    v2d = fe.dolfin.vertex_to_dof_map(V_vec)
    vals = np.zeros(len(tang_func.vector().get_local()))
    for i in range(0, 2):
        vals[v2d[np.multiply(2, meshdata.indBoundary, dtype=np.int16) + i]] = tang_func.vector().get_local()[v2d[np.multiply(2, meshdata.indBoundary, dtype=np.int16) + i]]
    tang_func.vector()[:] = vals

    return tang_func


def tangential_projection_outer(meshdata: MeshData, f: tp.Union[fe.Function, fe.Expression]) -> fe.Function:
    """
    Projects FEniCS function onto its tangential component of the outer
    hold-all boundary. Corners are not included in the outer boundary!

    Inputs:
        meshdata: Instance of MeshData, with outer boundary (tags {1, 2, 3, 4}).
        f: FEniCS function or expression, which is to be projected on tangential
            components. Has to be instantiated on meshdata.mesh.


    Return:
        tang_func: FEniCS function with values of f only in tangential
            direction of the outer boundary of meshdata.
    """
    # dS = fe.Measure("ds", domain=meshdata.mesh, subdomain_data=meshdata.boundaries)
    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)
    # normal = fe.FacetNormal(meshdata.mesh)
    # normal = normal_vector(meshdata)

    V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1)
    tang_func = fe.Function(V_vec)
    test = fe.TestFunction(V_vec)
    trial = fe.TrialFunction(V_vec)

    lhs = fe.inner(trial, test)*dx  # + fe.inner(trial('+'), test('+'))*(dS(1) + dS(2) + dS(3) + dS(4))
    # lhs = fe.inner(trial, test)*dx
    lhs = fe.assemble(lhs, keep_diagonal=True)
    lhs.ident_zeros()

    rhs = fe.inner(fe.Expression(("0.", "0."), domain=meshdata.mesh, degree=1), test)*dx  # + fe.inner(f('+') - fe.inner(f('+'), normal('+'))*normal('+'), test('+'))*(dS(1)+dS(2)+dS(3)+dS(4))
    # rhs = fe.inner(f - fe.inner(f, normal)*normal, test)*dx
    rhs = fe.assemble(rhs)

    # compute normal projections by hand, since hold-all outer boundaries are explicitly known
    bcs = [fe.DirichletBC(V_vec, f - fe.inner(f, fe.Expression(("0.", "-1."), domain=meshdata.mesh, degree=1))*fe.Expression(("0.", "-1."), domain=meshdata.mesh, degree=1), meshdata.boundaries, 1),
           fe.DirichletBC(V_vec, f - fe.inner(f, fe.Expression(("1.", "0."), domain=meshdata.mesh, degree=1))*fe.Expression(("1.", "0."), domain=meshdata.mesh, degree=1), meshdata.boundaries, 2),
           fe.DirichletBC(V_vec, f - fe.inner(f, fe.Expression(("0.", "1."), domain=meshdata.mesh, degree=1))*fe.Expression(("0.", "1."), domain=meshdata.mesh, degree=1), meshdata.boundaries, 3),
           fe.DirichletBC(V_vec, f - fe.inner(f, fe.Expression(("-1.", "0."), domain=meshdata.mesh, degree=1))*fe.Expression(("-1.", "0."), domain=meshdata.mesh, degree=1), meshdata.boundaries, 4)
           ]

    for bc in bcs:
        bc.apply(lhs)
        bc.apply(rhs)

    fe.solve(lhs, tang_func.vector(), rhs)
    # TODO: Values at corner nodes and inner nodes are set to 0
    # Set values outside of outer boundary to 0
    v2d = fe.dolfin.vertex_to_dof_map(V_vec)
    vals = np.zeros(len(tang_func.vector().get_local()))
    # take values of the outer boundary, including corners
    for i in range(0, 2):
        vals[v2d[np.multiply(2, meshdata.indOuterBoundary, dtype=np.int16) + i]] = tang_func.vector().get_local()[v2d[np.multiply(2, meshdata.indOuterBoundary, dtype=np.int16) + i]]
    # set values at corners to 0
    for i in range(0, 2):
        vals[v2d[np.multiply(2, meshdata.indCornerNodes, dtype=np.int16) + i]] = 0.
    # set values
    tang_func.vector()[:] = vals

    return tang_func


def normal_projection(meshdata: MeshData, f: tp.Union[fe.Function, fe.Expression]) -> fe.Function:
    """Projects FEniCS function onto its normal component of the shape with tags {5, 6}.

    Inputs:
        meshdata: Instance of MeshData, with outer boundary (tags {1, 2, 3, 4}).
        f: FEniCS function or expression, which is to be projected on tangential
            components. Has to be instantiated on meshdata.mesh.


    Return:
        normal_func: FEniCS function with values of f only in normal
            direction of the shape of meshdata.
    """
    dS = fe.Measure("dS", subdomain_data=meshdata.boundaries)
    # dx     = fe.Measure("dx", subdomain_data=meshdata.subdomains)
    normal = fe.FacetNormal(meshdata.mesh)
    # normal = normal_vector(meshdata)

    V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1)
    normal_func = fe.Function(V_vec)
    test = fe.TestFunction(V_vec)
    trial = fe.TrialFunction(V_vec)

    lhs = fe.inner(trial('+'), test('+'))*(dS(5) + dS(6))
    # lhs = fe.inner(trial, test)*dx
    lhs = fe.assemble(lhs, keep_diagonal=True)
    lhs.ident_zeros()

    rhs = fe.inner(fe.inner(f('+'), normal('+'))*normal('+'), test('+'))*(dS(5) + dS(6))
    # rhs = fe.inner(f - fe.inner(f, normal)*normal, test)*dx
    rhs = fe.assemble(rhs)

    fe.solve(lhs, normal_func.vector(), rhs)

    # set all values outside of shape to 0
    v2d = fe.dolfin.vertex_to_dof_map(V_vec)
    vals = np.zeros(len(normal_func.vector().get_local()))
    for i in range(0, 2):
        vals[v2d[np.multiply(2, meshdata.indBoundary, dtype=np.int16) + i]] = normal_func.vector().get_local()[v2d[np.multiply(2, meshdata.indBoundary, dtype=np.int16) + i]]
    normal_func.vector()[:] = vals

    return normal_func


def estimate_g_shape(meshdata_initial: MeshData, g_visc: float, gamma_param: float, p_param: float, cg: bool = False) -> fe.Function:
    """
    !not used!
    Estimates shape mesh node density with a priori information, based on values on nodes and neighbors.
    """
    V_cg = fe.FunctionSpace(meshdata_initial.mesh, "CG", 1)
    V_dg0 = fe.FunctionSpace(meshdata_initial.mesh, "DG", 0)

    g_shape = fe.Function(V_dg0)
    g_shape_smoothed = fe.Function(V_cg)
    g_shape_normed = fe.Function(V_cg)
    g_shape_transf = fe.Function(V_cg)

    # g_shp_dg Estimation
    # get cell indices of cells neighbouring interior boundary (shape-mesh)
    cell_inds = []
    facet_vols = []

    for f in fe.facets(meshdata_initial.mesh):
        if meshdata_initial.boundaries[f.index()] in [5, 6]:
            facet_vols.append(fe.dolfin.Edge(meshdata_initial.mesh, f.index()).length())
            for c in fe.cells(f):
                cell_inds.append(c.index())

    cell_inds = np.array(cell_inds)
    facet_vols = np.array(facet_vols)
#    total_vol = np.sum(facet_vols)

    # set values according to estimation with uniform reference mesh normed to 1
    vals_dg = np.zeros(len(g_shape.vector().get_local()))
    vals_dg[cell_inds[:: 2]] = (1./float(len(facet_vols))) * (1./facet_vols)
    vals_dg[cell_inds[1::2]] = (1./float(len(facet_vols))) * (1./facet_vols)
    g_shape.vector()[:] = vals_dg

    if cg:
        g_shape = fe.project(g_shape, V_cg)

    # g_shp_smooth smoothing
    # viscosity version of g_dg
    dS = fe.Measure("dS", domain=meshdata_initial.mesh, subdomain_data=meshdata_initial.boundaries)

    trial = fe.TrialFunction(V_cg)
    test = fe.TestFunction(V_cg)

    lhs = fe.Constant(g_visc)*fe.inner(fe.grad(trial('+')), fe.grad(test('+')))*(dS(5)+dS(6)) + fe.inner(trial('+'), test('+'))*(dS(5)+dS(6))
    lhs_ass = fe.assemble(lhs, keep_diagonal=True)
    lhs_ass.ident_zeros()

    rhs = fe.inner(g_shape('+'), test('+'))*(dS(5)+dS(6))
    rhs_ass = fe.assemble(rhs)

    bcs = [fe.DirichletBC(V_cg, fe.Expression("0.", degree=1, domain=meshdata_initial.mesh), meshdata_initial.boundaries, i) for i in range(1, 5)]

    for bc in bcs:
        bc.apply(lhs_ass)
        bc.apply(rhs_ass)

    fe.solve(lhs_ass, g_shape_smoothed.vector(), rhs_ass)

    # norm to 1
    g_shape_normed.vector()[:] = (1./fe.assemble(g_shape_smoothed*(dS(5)+dS(6)))) * g_shape_smoothed.vector().get_local()

    # set all values outside shape to 0
    interface_inds = order_boundary_verts(meshdata_initial)
    v2d = fe.dolfin.vertex_to_dof_map(V_cg)
    dof_relevant = g_shape_normed.vector().get_local()[v2d[interface_inds]]
    g_shape_normed_vals = np.zeros(len(g_shape_normed.vector().get_local()))
    g_shape_normed_vals[v2d[interface_inds]] = dof_relevant
    g_shape_normed.vector()[:] = g_shape_normed_vals

#     # g_shp_transf transformation
#     def beta_reg_transf_old(x):
#         p_param     = 6.  # controls right side of beta (dense nodes)
#         q_param     = 20.  # controls left side of beta (non-dense nodes)
#         gamma_param = 1.5
#         g_max = np.max(g_vol_normed.vector().get_local())
#         g_min = np.min(g_vol_normed.vector().get_local())
#
#         def lin_trafo(y):
#             speed = 1.2
#             return (1./(1.-np.exp(-speed)))*(1.-np.exp(-speed*(x-g_min)/(g_max-g_min)))
#
#         return (1./(g_max-g_min))*(1./(gamma_param+1.))*((1./Stammfkt)*sp.betainc(p_param, q_param, ((x-g_min)/(g_max-g_min)))+gamma_param)
#         return (1./(g_max-g_min))*(1./(gamma_param+1.))*((1./Stammfkt)*sp.betainc(p_param, q_param, lin_trafo(x))+gamma_param) + 0.3
    # Bayes style transformation of g_smoothed via a priori information
    vol = fe.assemble(1.*(dS(5)+dS(6)))
    g_max = np.max(dof_relevant)
    g_min = np.min(dof_relevant)

    from scipy import interpolate
    tck = interpolate.splrep(np.array([g_min, 1./vol, g_max]), np.array([0., 0.5, 1.]), s=0, k=1)

    def beta_reg_transf_new(x):
        x_transf = interpolate.splev(x, tck, der=0)
        # return x_transf
        return (1./(gamma_param+1.)) * (2.*sp.betainc(p_param, p_param, x_transf) + gamma_param)

# for troubleshooting/tuning transformation
#    xs = np.array(np.arange(g_min, g_max, 0.00001))
#    ys = beta_reg_transf_new(xs)
#    plot1, = plt.plot(xs, ys)
#    ys[:] = 1.
#    plot2, = plt.plot(xs, ys)
#    plt.show()

    # transform g_smoothed via parameter choice of gamma and p
    g_shape_transf_vals = np.zeros(len(g_shape_normed.vector().get_local()))
    dof_relevant_transf = beta_reg_transf_new(dof_relevant)
    g_shape_transf_vals[v2d[interface_inds]] = dof_relevant_transf
    g_shape_transf.vector()[:] = g_shape_transf_vals
    g_shape_transf.vector()[:] = (1./fe.assemble(g_shape_transf*(dS(5)+dS(6)))) * g_shape_transf.vector().get_local()
    g_shape_transf.vector()[:] = np.multiply(g_shape_normed.vector().get_local(), g_shape_transf.vector().get_local())
    g_shape_transf.vector()[:] = (1./fe.assemble(g_shape_transf*(dS(5)+dS(6)))) * g_shape_transf.vector().get_local()

    return g_shape_normed, g_shape_transf


def estimate_g_shape_averaged(meshdata_initial: MeshData) -> fe.Function:
    """
    Estimate initial node densities of the shape mesh. Computed on the nodes
    of the shape mesh as the average of neighboring facet volumes.


    Inputs:
        meshdata_initial: Instance of MeshData, with an embedded shape with
            tags {5, 6}, for which node density is computed.


    Returns:
        g_shape_normed: Estimate of the node density of the shape mesh from
            meshdata. g_shape_normed is a FEniCS function normalized to 1 via
            the L1-norm on the shape mesh.
    """
    V_cg = fe.FunctionSpace(meshdata_initial.mesh, "CG", 1)
    v2d = fe.dolfin.vertex_to_dof_map(V_cg)
    dS = fe.Measure("dS", domain=meshdata_initial.mesh, subdomain_data=meshdata_initial.boundaries)

    # g_shp_dg estimation via averaged facet volumes of neighboring shape nodes.
    # Values are saved in the same order as meshdata_initial.indBoundary.
    vals_verts = np.zeros(meshdata_initial.mesh.num_vertices())
    for v_ind in meshdata_initial.indBoundary:
        counter = 0
        cumm_edge_length = 0.
        for e in fe.edges(fe.dolfin.Vertex(meshdata_initial.mesh, v_ind)):
            if meshdata_initial.boundaries[e.index()] == 5 or meshdata_initial.boundaries[e.index()] == 6:
                counter += 1
                cumm_edge_length += 1./(fe.dolfin.Edge(meshdata_initial.mesh, e.index()).length())
        vals_verts[v_ind] = (1./counter)*cumm_edge_length

    # set values to DoFs
    g_shape = fe.Function(V_cg)
    g_shape_normed = fe.Function(V_cg)
    vals_dofs = np.zeros(meshdata_initial.mesh.num_vertices())
    vals_dofs[v2d] = vals_verts
    g_shape.vector()[:] = vals_dofs

    L1_norm_g_shape = fe.assemble(g_shape*(dS(5)+dS(6)))  # length assumed > 0
    g_shape_normed.vector()[:] = (1./L1_norm_g_shape)*g_shape.vector().get_local()

    return g_shape_normed


def estimate_g_vol(meshdata_initial: MeshData, g_visc: float, gamma_param: float, p_param: float, cg: bool = False) -> fe.Function:
    """
    !not used!
    Estimates volume node density with a priori information, based on values on nodes and neighbors.
    """
    V_cg = fe.FunctionSpace(meshdata_initial.mesh, "CG", 1)
    V_dg0 = fe.FunctionSpace(meshdata_initial.mesh, "DG", 0)

    g_vol = fe.Function(V_dg0)
    g_vol_smoothed = fe.Function(V_cg)
    g_vol_normed = fe.Function(V_cg)
    g_vol_transf = fe.Function(V_cg)

    # g_vol_dg estimation
    # get cell indices of cells neighbouring interior boundary (shape-mesh)
    cell_inds = []
    cell_vols = []
    for c in fe.cells(meshdata_initial.mesh):
        cell_inds.append(c.index())
        cell_vols.append(c.volume())
        # cell_vols.append(c.radius_ratio())
        # cell_vols.append(c.inradius())
    cell_inds = np.array(cell_inds)
    cell_vols = np.array(cell_vols)
#    total_vol = np.sum(cell_vols)

    # set values according to estimation with uniform reference mesh normed to 1
    vals_dg = np.zeros(len(g_vol.vector().get_local()))
    vals_dg[cell_inds] = (1./float(len(cell_vols))) * (1./cell_vols)
    g_vol.vector()[:] = vals_dg

    if cg:
        g_vol = fe.project(g_vol, V_cg)

    # g_vol_smooth smoothing
    # dual to primal
    dx = fe.Measure("dx", domain=meshdata_initial.mesh)

    trial = fe.TrialFunction(V_cg)
    test = fe.TestFunction(V_cg)

    lhs = fe.Constant(g_visc)*fe.inner(fe.grad(trial), fe.grad(test))*dx + fe.inner(trial, test)*dx
    rhs = fe.inner(g_vol, test)*dx

    bcs = [fe.DirichletBC(V_cg, fe.Expression("1", degree=1, domain=meshdata_initial.mesh), meshdata_initial.boundaries, i) for i in range(1, 5)]

    fe.solve(lhs == rhs, g_vol_smoothed, bcs)

    g_vol_normed.vector()[:] = (1./fe.assemble(g_vol_smoothed*dx)) * g_vol_smoothed.vector().get_local()

#    # g_vol_transf Transformation
#    def beta_reg_transf_old(x):
#        p_param     = 6.  # controls right side of beta (dense nodes)
#        q_param     = 20.  # controls left side of beta (non-dense nodes)
#        gamma_param = 1.5
#        g_max = np.max(g_vol_normed.vector().get_local())
#        g_min = np.min(g_vol_normed.vector().get_local())
#
#        def lin_trafo(y):
#            speed = 1.2
#            return (1./(1.-np.exp(-speed)))*(1.-np.exp(-speed*(x-g_min)/(g_max-g_min)))
#
#        return (1./(g_max-g_min))*(1./(gamma_param+1.))*((1./Stammfkt)*sp.betainc(p_param, q_param, ((x-g_min)/(g_max-g_min)))+gamma_param)
#        return (1./(g_max-g_min))*(1./(gamma_param+1.))*((1./Stammfkt)*sp.betainc(p_param, q_param, lin_trafo(x))+gamma_param) + 0.3

    # Bayes style transformation of g_smoothed via a priori information
    vol = fe.assemble(1.*dx)
    g_max = np.max(g_vol_normed.vector().get_local())
    g_min = np.min(g_vol_normed.vector().get_local())

    from scipy import interpolate
    tck = interpolate.splrep(np.array([g_min, 1./vol, g_max]), np.array([0., 0.5, 1.]), s=0, k=1)

    def beta_reg_transf_new(x):
        x_transf = interpolate.splev(x, tck, der=0)
        # return x_transf
        return (1./(gamma_param+1.)) * (2.*sp.betainc(p_param, p_param, x_transf) + gamma_param)

# for troubleshooting/tuning transformation
#    xs = np.array(np.arange(g_min, g_max, 0.00001))
#    ys = beta_reg_transf_new(xs)
#    plot1, = plt.plot(xs, ys)
#    ys[:] = 1.
#    plot2, = plt.plot(xs, ys)
#    plt.show()

    # transform g_smoothed by choice of parameters gamma and p
    g_vol_transf.vector()[:] = beta_reg_transf_new(g_vol_normed.vector().get_local())
    g_vol_transf.vector()[:] = (1./fe.assemble(g_vol_transf*dx)) * g_vol_transf.vector().get_local()
    g_vol_transf.vector()[:] = np.multiply(g_vol_normed.vector().get_local(), g_vol_transf.vector().get_local())
    g_vol_transf.vector()[:] = (1./fe.assemble(g_vol_transf*dx)) * g_vol_transf.vector().get_local()

    return g_vol_normed, g_vol_transf


def estimate_g_vol_averaged(meshdata: MeshData) -> fe.Function:
    """
    Estimate initial node densities of the volume mesh. Computed on the nodes
    of the shape mesh as the average of neighboring facet volumes.


    Inputs:
        meshdata_initial: Instance of MeshData, acting as the hold-all volume mesh.


    Returns:
        g_vol_normed: Estimate of the node density of the volume mesh from
            meshdata. g_vol_normed is a FEniCS function normalized to 1 via
            the L1-norm of the volume mesh.
    """
    V_cg = fe.FunctionSpace(meshdata.mesh, "CG", 1)
    v2d = fe.dolfin.vertex_to_dof_map(V_cg)
    dx = fe.Measure("dx", domain=meshdata.mesh)

    vals_verts = np.zeros(meshdata.mesh.num_vertices())
    for v in fe.vertices(meshdata.mesh):
        index = v.index()
        vol_cumm = 0.
        counter = 0
        for c in fe.cells(v):
            counter += 1
            vol_cumm += (1./c.volume())
        vals_verts[index] = (1./counter)*vol_cumm

    g_vol = fe.Function(V_cg)
    g_vol_normed = fe.Function(V_cg)
    vals_dofs = np.zeros(meshdata.mesh.num_vertices())
    vals_dofs[v2d] = vals_verts
    g_vol.vector()[:] = vals_dofs

    L1_norm_g_vol = fe.assemble(g_vol*dx('everywhere'))
    g_vol_normed.vector()[:] = (1./L1_norm_g_vol)*g_vol.vector().get_local()

    return g_vol_normed


def embedding(meshdata: MeshData, meshdata_initial: MeshData) -> fe.Function:
    """Computes the embedding acting as the preshape of meshdata.


    Inputs:
        meshdata: Current mesh, instance of the MeshData class.
        meshdata_initial: Initial/ reference mesh, instance of the MeshData class.


    Returns:
        emb: FEniCS CG1 function on meshdata_initial.mesh, representing the preshape
            embedding: meshdata_initial --> meshdata as a difference of
            node coordinate values. Since emb is initialized on meshdata_initial,
            no actual coordinate difference needs to be taken.
    """
    V_vec = fe.VectorFunctionSpace(meshdata_initial.mesh, "CG", 1)
    emb = fe.Function(V_vec)
    v2d = fe.dolfin.vertex_to_dof_map(V_vec)

    difference = meshdata.mesh.coordinates()
    difference = np.reshape(difference, 2*meshdata_initial.mesh.num_vertices())

    emb_vals = np.zeros(2*meshdata_initial.mesh.num_vertices())
    emb_vals[v2d] = difference

    emb.vector()[:] = emb_vals

    return emb


def embedding_inverse(meshdata: MeshData, meshdata_initial: MeshData) -> fe.Function:
    """Computes the inverse embedding acting as the inverse of preshape of meshdata.


    Inputs:
        meshdata: Current mesh, instance of the MeshData class.
        meshdata_initial: Initial/ reference mesh, instance of the MeshData class.


    Returns:
        emb_inv: FEniCS CG1 function on meshdata.mesh, representing the inverse preshape
            embedding: meshdata --> meshdata_initial as a difference of
            node coordinate values. Since emb is initialized on meshdata,
            no actual coordinate difference needs to be taken.
    """
    V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1)
    emb_inv = fe.Function(V_vec)
    v2d = fe.dolfin.vertex_to_dof_map(V_vec)

    difference = meshdata_initial.mesh.coordinates()
    difference = np.reshape(difference, 2*meshdata_initial.mesh.num_vertices())

    emb_vals = np.zeros(2*meshdata_initial.mesh.num_vertices())
    emb_vals[v2d] = difference

    emb_inv.vector()[:] = emb_vals

    return emb_inv


def covariant_volume(meshdata: MeshData, meshdata_initial: MeshData, tang_initial: fe.Function) -> fe.Function:
    """Computes covariant volume of preshape of a shape.


    Inputs:
        meshdata: Current mesh, instance of the MeshData class.
        meshdata_initial: Initial/ reference mesh, instance of the MeshData class.
        tang_initial: Tangential unit vector field on shape of meshdata_initial,
            can be computed by tangential_vector(meshdata_initial) and
            normalize_q_int with vol = False.


    Returns:
        vol_form: FEniCS scalar function, with values of volume form
            det D^tau(emb)o(emb^-1) on initial shape, otherwise zero. Since shape is 1D,
            det D^tau (emb) = fe.inner(nabla(emb)^T tang_initial, tang).
    """
    V_vec_initial = fe.VectorFunctionSpace(meshdata_initial.mesh, "CG", 1)
    V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1)
    V_sc = fe.FunctionSpace(meshdata.mesh, "CG", 1)

#    tang_initial = tangential_vector(meshdata_initial)
    tang = tangential_vector(meshdata)
    emb = embedding(meshdata, meshdata_initial)

    # solve for D(emb) tang_initial_i on initial_mesh
    dS = fe.Measure("dS", domain=meshdata_initial.mesh, subdomain_data=meshdata_initial.boundaries)

    test = fe.TestFunction(V_vec_initial)
    trial = fe.TrialFunction(V_vec_initial)
    D_emb_tau1_initial = fe.Function(V_vec_initial)

    lhs = fe.inner(trial('+'), test('+'))*(dS(5)+dS(6))
    lhs_ass = fe.assemble(lhs, keep_diagonal=True)
    lhs_ass.ident_zeros()

    rhs = fe.inner(fe.dot(fe.grad(emb('+')), tang_initial('+')), test('+'))*(dS(5)+dS(6))
    rhs_ass = fe.assemble(rhs)

    fe.solve(lhs_ass, D_emb_tau1_initial.vector(), rhs_ass)

    # push D(emb) tang_initial_i forward to current mesh (Derivative acts as push-forward)
    D_emb_tau1 = fe.Function(V_vec)

    vals1 = D_emb_tau1_initial.vector().get_local()
    D_emb_tau1.vector()[:] = vals1

    # det D^tau (emb) = D^tau (emb) da 1D!
    dS = fe.Measure("dS", domain=meshdata.mesh, subdomain_data=meshdata.boundaries)

    test = fe.TestFunction(V_sc)
    trial = fe.TrialFunction(V_sc)
    vol_form = fe.Function(V_sc)

    lhs = fe.inner(trial('+'), test('+'))*(dS(5)+dS(6))
    lhs_ass = fe.assemble(lhs, keep_diagonal=True)
    lhs_ass.ident_zeros()

    rhs = fe.inner(fe.inner(D_emb_tau1('+'), tang('+')), test('+'))*(dS(5)+dS(6))
    rhs_ass = fe.assemble(rhs)

    fe.solve(lhs_ass, vol_form.vector(), rhs_ass)

    return vol_form


def covariant_volume_inverse(meshdata: MeshData, meshdata_initial: MeshData, tang_initial: fe.Function) -> fe.Function:
    """Computes inverse covariant volume of preshape of a shape.


    Inputs:
        meshdata: Current mesh, instance of the MeshData class.
        meshdata_initial: Initial/ reference mesh, instance of the MeshData class.
        tang_initial: Tangential unit vector field on shape of meshdata_initial,
            can be computed by tangential_vector(meshdata_initial) and
            normalize_q_int with vol = False.


    Returns:
        vol_form: FEniCS scalar function, with values of volume form
            det D^tau(emb^-1)o(emb) on shape, otherwise zero. Since shape is 1D,
            det D^tau (emb^-1) = fe.inner(nabla(emb^-1)^T tang, tang_initial).
    """
    V_vec_initial = fe.VectorFunctionSpace(meshdata_initial.mesh, "CG", 1)
    V_sc_initial = fe.FunctionSpace(meshdata_initial.mesh, "CG", 1)
    V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1)
    V_sc = fe.FunctionSpace(meshdata.mesh, "CG", 1)

#    tang_initial = tangential_vector(meshdata_initial)
    tang = tangential_vector(meshdata)
    emb_inv = embedding_inverse(meshdata, meshdata_initial)

    # calculate D(Emb_inv) tang \in T meshdata_initial
    dS = fe.Measure("dS", domain=meshdata.mesh, subdomain_data=meshdata.boundaries)

    test = fe.TestFunction(V_vec)
    trial = fe.TrialFunction(V_vec)
    D_emb_inv_tau1 = fe.Function(V_vec)

    lhs = fe.inner(trial('+'), test('+'))*(dS(5)+dS(6))
    lhs_ass = fe.assemble(lhs, keep_diagonal=True)
    lhs_ass.ident_zeros()

    rhs = fe.inner(fe.dot(fe.grad(emb_inv('+')), tang('+')), test('+'))*(dS(5)+dS(6))
    rhs_ass = fe.assemble(rhs)

    fe.solve(lhs_ass, D_emb_inv_tau1.vector(), rhs_ass)

    # push D(Emb_inv) tang to initial mesh
    D_emb_inv_tau1_pushed = fe.Function(V_vec_initial)
    D_emb_inv_tau1_pushed.vector()[:] = D_emb_inv_tau1.vector().get_local()

    # solve for <D(emb) tang_j, tang_initial_pushed_i> on initial mesh
    dS = fe.Measure("dS", domain=meshdata_initial.mesh, subdomain_data=meshdata_initial.boundaries)

    test = fe.TestFunction(V_sc_initial)
    trial = fe.TrialFunction(V_sc_initial)
    D_emb_inv_tau1_tauinit1 = fe.Function(V_sc_initial)

    lhs = fe.inner(trial('+'), test('+'))*(dS(5)+dS(6))
    lhs_ass = fe.assemble(lhs, keep_diagonal=True)
    lhs_ass.ident_zeros()

    rhs = fe.inner(fe.inner(D_emb_inv_tau1_pushed('+'), tang_initial('+')), test('+'))*(dS(5)+dS(6))
    rhs_ass = fe.assemble(rhs)

    fe.solve(lhs_ass, D_emb_inv_tau1_tauinit1.vector(), rhs_ass)

    # det D^tau (emb) = D^tau (emb) da 1D!
    vol_form_initial = fe.project(abs(D_emb_inv_tau1_tauinit1), V_sc_initial)

    # pull form back to current mesh
    vol_form = fe.Function(V_sc)
    vol_form.vector()[:] = vol_form_initial.vector().get_local()

    return vol_form


def Jacobian_determinant(meshdata: MeshData, meshdata_initial: MeshData) -> fe.Function:
    """Calculates det(D emb): meshdata_initial --> R, with emb preshape output of embedding.


    Inputs:
        meshdata: Current mesh, instance of the MeshData class.
        meshdata_initial: Initial/ reference mesh, instance of the MeshData class.


    Returns:
        jac_det: FEniCS scalar function, with values of the Jacobian volume form
            det (D emb)o(emb^-1) on volume mesh meshdata_initial.mesh.
    """
    V_sc_init = fe.FunctionSpace(meshdata_initial.mesh, "CG", 1)
    emb = embedding(meshdata, meshdata_initial)
    jac_det = fe.project(fe.det(fe.grad(emb)), V_sc_init)

    return jac_det


def jacobian_determinant_inverse(meshdata: MeshData, meshdata_initial: MeshData) -> fe.Function:
    """Calculates det(D emb^-1): meshdata --> R, with emb preshape output of embedding.


    Inputs:
        meshdata: Current mesh, instance of the MeshData class.
        meshdata_initial: Initial/ reference mesh, instance of the MeshData class.


    Returns:
        jac_det_inv: FEniCS scalar function, with values of the inverse Jacobian
            volume form det (D emb^-1)o(emb) on volume mesh meshdata.mesh.
    """
    V_sc = fe.FunctionSpace(meshdata.mesh, "CG", 1)
    emb_inv = embedding_inverse(meshdata, meshdata_initial)
    jac_det_inv = fe.project(fe.det(fe.grad(emb_inv)), V_sc)

    return jac_det_inv


def jacobi_deriv(meshdata: MeshData, meshdata_initial: MeshData, vol_form: fe.Function, V: tp.Union[fe.Function, fe.dolfin.Argument]) -> ufl.Form:
    """
    !not used!
    Calculates Tr(Adju(D^tau(Emb)) * D^tau VoEmb) on shape as a form on meshdata.mesh
    uses chain rule to assemble D^tau (VcircEmb) = < DV(Emb)DEmb tang_init, tang>
    with pre-computation of pushed (DEmb tang_init).
    """
    V_vec_initial = fe.VectorFunctionSpace(meshdata_initial.mesh, "CG", 1)
    V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1)

    tang_initial = tangential_vector(meshdata_initial)
    tang = tangential_vector(meshdata)
    emb = embedding(meshdata, meshdata_initial)

    # (DEmb tang_init) computation (1 Term since 1D)
    # solve for D(emb) tang_initial_i on initial_mesh
    dS = fe.Measure("dS", domain=meshdata_initial.mesh, subdomain_data=meshdata_initial.boundaries)

    test = fe.TestFunction(V_vec_initial)
    trial = fe.TrialFunction(V_vec_initial)
    D_emb_tau1_initial = fe.Function(V_vec_initial)

    lhs = fe.inner(trial('+'), test('+'))*(dS(5)+dS(6))
    lhs_ass = fe.assemble(lhs, keep_diagonal=True)
    lhs_ass.ident_zeros()

    rhs = fe.inner(fe.dot(fe.grad(emb('+')), tang_initial('+')), test('+'))*(dS(5)+dS(6))
    rhs_ass = fe.assemble(rhs)

    fe.solve(lhs_ass, D_emb_tau1_initial.vector(), rhs_ass)

    # push D(emb) tang_initial_i forward to current mesh for (DEmb tang_init) pushed
    D_emb_tau1 = fe.Function(V_vec)

    vals = D_emb_tau1_initial.vector().get_local()
    D_emb_tau1.vector()[:] = vals

    # calculate form of jacobi_deriv (1D)
    # Adju = Id for (1D) per Definition
    # Trace trivial for (1D)
    jacobi_deriv = fe.inner(fe.dot(fe.grad(V('+')), D_emb_tau1('+')), tang('+'))

    return jacobi_deriv


def shape_deriv_shape_tracking(meshdata: MeshData, meshdata_initial: MeshData, tang_initial: fe.Function, q_ext: fe.Function, g_shape: fe.Function, V: tp.Union[fe.Function, fe.dolfin.Argument]) -> ufl.Form:
    """Computes ufl form of the tangential part (!) of the preshape derivative of
    the shape parameterization tracking objective.


    Inputs:
       meshdata: Current mesh, instance of the MeshData class.
       meshdata_initial: Initial/ reference mesh, instance of the MeshData class.
       tang_initial: Tangential unit vector field on shape of meshdata_initial,
           can be computed by tangential_vector(meshdata_initial) and
           normalize_q_int with vol = False.
        q_ext: FEniCS function of globally defined target shape mesh densitites
            from config.ini.
        g_shape: Estimated initial shape mesh node densities,
            output of estimate_g_shape_averaged.
        V: FEniCS test function or direction for evaluation of preshape derivative.


    Returns:
        deriv: ufl form of tangential components of the preshape derivative of
            the shape parameterizatin tracking objective. Can be evaluated,
            or be used as a regularizing right-hand-side term of (pre)shape
            derivative systems. Leaves shapes approximately invariant, since
            only of tangential components of the preshape derivative are used!
    """
    dS = fe.Measure("dS", domain=meshdata.mesh, subdomain_data=meshdata.boundaries)

    # define elementary participating integrads
    vol_form_inverse = covariant_volume_inverse(meshdata, meshdata_initial, tang_initial)
    norm_q_ext = fe.assemble(q_ext*(dS(5)+dS(6)))
    normal = fe.FacetNormal(meshdata.mesh)
    div_tang_V = fe.nabla_div(V('+')) - fe.inner(fe.dot(fe.grad(V('+')), normal('+')), normal('+'))

    deriv = ((fe.Constant(1./norm_q_ext)*q_ext - g_shape*vol_form_inverse)
             * (fe.Constant(1./norm_q_ext)*fe.inner(fe.grad(q_ext)('+') - fe.inner(fe.grad(q_ext)('+'), normal('+'))*normal('+'), V('+'))
             + fe.Constant(0.5)*(fe.Constant(1./norm_q_ext)*q_ext + g_shape*vol_form_inverse)*div_tang_V
                )
             )*(dS(5)+dS(6))

    return deriv


def shape_deriv_shape_tracking_full(meshdata: MeshData, meshdata_initial: MeshData, tang_initial: fe.Function, q_ext: fe.Function, g_shape: fe.Function, V: tp.Union[fe.Function, fe.dolfin.Argument]) -> ufl.Form:
    """Computes ufl form of the full preshape derivative of
    the shape parameterization tracking objective.


    Inputs:
       meshdata: Current mesh, instance of the MeshData class.
       meshdata_initial: Initial/ reference mesh, instance of the MeshData class.
       tang_initial: Tangential unit vector field on shape of meshdata_initial,
           can be computed by tangential_vector(meshdata_initial) and
           normalize_q_int with vol = False.
        q_ext: FEniCS function of globally defined target shape mesh densitites
            from config.ini.
        g_shape: Estimated initial shape mesh node densities,
            output of estimate_g_shape_averaged.
        V: FEniCS test function or direction for evaluation of preshape derivative.


    Returns:
        deriv: ufl form of full preshape derivative of the shape parameterizatin tracking
            objective. Can be evaluated, or be used as a regularizing right-hand-side
            term of (pre)shape derivative systems and tests.
    """
    dS = fe.Measure("dS", domain=meshdata.mesh, subdomain_data=meshdata.boundaries)

    # define elementary participating integrads
    vol_form_inverse = covariant_volume_inverse(meshdata, meshdata_initial, tang_initial)
    norm_q_ext = fe.assemble(q_ext*(dS(5)+dS(6)))
    normal = fe.FacetNormal(meshdata.mesh)
    div_tang_V = fe.nabla_div(V('+')) - fe.inner(fe.dot(fe.grad(V('+')), normal('+')), normal('+'))

    deriv = ((fe.Constant(1./norm_q_ext)*q_ext - g_shape*vol_form_inverse)
             * (fe.Constant(1./norm_q_ext)*fe.inner(fe.grad(q_ext)('+'), V('+'))
             + fe.Constant(0.5)*(fe.Constant(1./norm_q_ext)*q_ext + g_shape*vol_form_inverse)*div_tang_V
                )
             )*(dS(5)+dS(6))

    return deriv


def shape_deriv_vol_tracking(meshdata: MeshData, meshdata_initial: MeshData, q_vol: fe.Function, g_vol: fe.Function, V: tp.Union[fe.Function, fe.dolfin.Argument]) -> ufl.Form:
    """
    Computes ufl form of the preshape derivative of the volume parameterization
    tracking objective suitable for invariant shapes.


    Inputs:
       meshdata: Current mesh, instance of the MeshData class.
       meshdata_initial: Initial/ reference mesh, instance of the MeshData class.
       tang_initial: Tangential unit vector field on shape of meshdata_initial,
           can be computed by tangential_vector(meshdata_initial) and
           normalize_q_int with vol = False.
        q_vol: FEniCS function of the target volume mesh node densitites
            from config.ini.
        g_vol: Estimated initial volume mesh node densities,
            output of estimate_g_vol_averaged.
        V: FEniCS test function or direction for evaluation of preshape derivative.


    Returns:
        deriv: ufl form of preshape derivative of the volume parameterizatin tracking
            objective, with values using 2 normalizations needed for invariant
            shapes without boundary. Can be evaluated,
            or be used as a regularizing right-hand-side term of (pre)shape
            derivative systems and tests.
    """
    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    # define elementary participating integrads
    vol_form_inverse = jacobian_determinant_inverse(meshdata, meshdata_initial)
    norm_g_vol_in = fe.assemble(g_vol*vol_form_inverse*dx(1))
    norm_g_vol_out = fe.assemble(g_vol*vol_form_inverse*dx(2))
    norm_q_vol_in = fe.assemble(q_vol*dx(1))
    norm_q_vol_out = fe.assemble(q_vol*dx(2))

    # two integrals for outer and inner problem normalization
    deriv = ((fe.Constant(norm_g_vol_in/norm_q_vol_in)*q_vol - g_vol*vol_form_inverse)
             * (fe.Constant(norm_g_vol_in/norm_q_vol_in)*fe.inner(fe.grad(q_vol), V)
             + fe.Constant(0.5)*(fe.Constant(norm_g_vol_in/norm_q_vol_in)*q_vol + g_vol*vol_form_inverse)*fe.nabla_div(V)
                )
             )*dx(1) \
        + ((fe.Constant(norm_g_vol_out/norm_q_vol_out)*q_vol - g_vol*vol_form_inverse)
           * (fe.Constant(norm_g_vol_out/norm_q_vol_out)*fe.inner(fe.grad(q_vol), V)
           + fe.Constant(0.5)*(fe.Constant(norm_g_vol_out/norm_q_vol_out)*q_vol + g_vol*vol_form_inverse)*fe.nabla_div(V)
              )
           )*dx(2)

    return deriv


def shape_deriv_vol_tracking_noninv(meshdata: MeshData, meshdata_initial: MeshData, q_vol: fe.Function, g_vol: fe.Function, V: tp.Union[fe.Function, fe.dolfin.Argument]) -> ufl.Form:
    """
    Computes ufl form of the preshape derivative of the volume parameterization
    tracking objective suitable for non-invariant shapes.


    Inputs:
        meshdata: Current mesh, instance of the MeshData class.
        meshdata_initial: Initial/ reference mesh, instance of the MeshData class.
        tang_initial: Tangential unit vector field on shape of meshdata_initial,
            can be computed by tangential_vector(meshdata_initial) and
            normalize_q_int with vol = False.
        q_vol: FEniCS function of the target volume mesh node densitites
            from config.ini.
        g_vol: Estimated initial volume mesh node densities,
            output of estimate_g_vol_averaged.
        V: FEniCS test function or direction for evaluation of preshape derivative.


    Returns:
        deriv: ufl form of preshape derivative of the volume parameterizatin tracking
            objective, with values using 1 normalization. Hence not suitable
            for invariant shapes without boundary. Can be evaluated,
            or be used as a regularizing right-hand-side term of (pre)shape
            derivative systems and tests.
    """
    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    # define elementary participating integrads
    vol_form_inverse = jacobian_determinant_inverse(meshdata, meshdata_initial)
    norm_q_vol = fe.assemble(q_vol*dx)

    # one integral for combined outer and inner problem normalization
    deriv = ((fe.Constant(1./norm_q_vol)*q_vol - g_vol*vol_form_inverse)
             * (fe.Constant(1./norm_q_vol)*fe.inner(fe.grad(q_vol), V)
                + fe.Constant(0.5)*(fe.Constant(1./norm_q_vol)*q_vol + g_vol*vol_form_inverse)*fe.nabla_div(V)
                )
             )*dx

    return deriv


def target_shape_tracking(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, meshdata_initial: MeshData, tang_initial: fe.Function, q_ext: fe.Function, g_shape: fe.Function, S: tp.Union[fe.Function, fe.Expression]) -> float:
    """
    Computes value of shape parameterization tracking functional according to

    (alpha_shape/2.)*integral_shape (q_ext - (g_shape o emb^-1)*(det D^tau emb^-1))**2 dS,

    on the shape mesh after (!) deformation according to FEniCS vector field S.
    If S is the zero vector field, values are computed on the current mesh.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        meshdata_initial: Initial/ reference mesh, instance of the MeshData class.
        tang_initial: Tangential unit vector field on shape of meshdata_initial,
            can be computed by tangential_vector(meshdata_initial) and
            normalize_q_int with vol = False.
        q_ext: FEniCS function of globally defined target shape mesh densitites
            from config.ini.
        g_shape: Estimated initial shape mesh node densities,
            output of estimate_g_shape_averaged.        deformation: FEniCS vector field used to deform the mesh, and then
            calculate the target functional value on the deformed mesh.
            If chosen zero, target functional value is computed on meshdata.
        S: FEniCS vector field used to deform the mesh, and then
            calculate the functional value on the deformed mesh.
            If chosen zero, target functional value is computed on meshdata.


    Returns:
        alpha_shape*target_val: Shape parameterization tracking functional
            value according to the above formula.
    """
    # Parameters unpacking
    alpha_shape = float(parameters["prshp_main"]["alpha_shape"])

    # create local deep copy of mesh
    msh = fe.Mesh(meshdata.mesh)
    sbd = fe.MeshFunction("size_t", msh, 2)
    sbd.set_values(meshdata.subdomains.array())
    bnd = fe.MeshFunction("size_t", msh, 1)
    bnd.set_values(meshdata.boundaries.array())
    local_mesh = MeshData(msh, sbd, bnd, ind_update=True)
    local_mesh.indBoundary = meshdata.indBoundary
    local_mesh.indBoundaryFaces = meshdata.indBoundaryFaces
    local_mesh.indNotIntBoundary = meshdata.indNotIntBoundary
    local_mesh.indOuterBoundary = meshdata.indOuterBoundary
    local_mesh.indCornerNodes = meshdata.indCornerNodes

    # project initial shape node densities before (!) deformation
    V_sc = fe.FunctionSpace(local_mesh.mesh, "CG", 1)
    g_shape_local = fe.Function(V_sc)
    g_shape_local.vector()[:] = g_shape.vector().get_local()

    # deform mesh copy
    fe.ALE.move(local_mesh.mesh, S)

    # set shape parameterization tracking functional
    dS = fe.Measure("dS", domain=local_mesh.mesh, subdomain_data=local_mesh.boundaries)
    norm_q_ext = fe.assemble(q_ext*(dS(5)+dS(6)))
    vol_form_inverse = covariant_volume_inverse(local_mesh, meshdata_initial, tang_initial)

    target_val = fe.assemble(fe.Constant(0.5)*(fe.Constant(1./norm_q_ext)*q_ext - g_shape_local*vol_form_inverse)
                             * (fe.Constant(1./norm_q_ext)*q_ext - g_shape_local*vol_form_inverse)*(dS(5)+dS(6))
                             )

    return alpha_shape*target_val


def target_volume_tracking(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, meshdata_initial: MeshData, q_vol_init: fe.Function, g_vol_init: fe.Function, S: tp.Union[fe.Function, fe.Expression]) -> float:
    """
    Computes value of volume parameterization tracking functional according to

    (alpha_vol/2.)*integral_vol (q_vol - (g_vol o emb^-1)*(det D emb^-1))**2 dx,

    on the volume mesh after (!) deformation according to FEniCS vector field S.
    If S is the zero vector field, values are computed on the current mesh.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        meshdata_initial: Initial/ reference mesh, instance of the MeshData class.
        q_vol_init: FEniCS function of target volume mesh densitites
            from config.ini.
        g_vol_init: Estimated initial volume mesh node densities,
            output of estimate_g_vol_averaged.
        S: FEniCS vector field used to deform the mesh, and then
            calculate the functional value on the deformed mesh.
            If chosen zero, target functional value is computed on meshdata.


    Returns:
        alpha_vol*target_val: Volume parameterization tracking functional value
            according to the above formula.
    """
    # Parameters unpacking
    alpha_vol = float(parameters["prshp_main"]["alpha_volume"])

    # create local deep copy of mesh
    msh = fe.Mesh(meshdata.mesh)
    sbd = fe.MeshFunction("size_t", msh, 2)
    sbd.set_values(meshdata.subdomains.array())
    bnd = fe.MeshFunction("size_t", msh, 1)
    bnd.set_values(meshdata.boundaries.array())
    local_mesh = MeshData(msh, sbd, bnd, ind_update=True)
    local_mesh.indBoundary = meshdata.indBoundary
    local_mesh.indBoundaryFaces = meshdata.indBoundaryFaces
    local_mesh.indNotIntBoundary = meshdata.indNotIntBoundary
    local_mesh.indOuterBoundary = meshdata.indOuterBoundary
    local_mesh.indCornerNodes = meshdata.indCornerNodes

    # project initial volume node densities before (!) deformation
    V_sc = fe.FunctionSpace(local_mesh.mesh, "CG", 1)
    g_vol = fe.project(g_vol_init, V_sc)

    # deform mesh copy
    fe.ALE.move(local_mesh.mesh, S)
    local_mesh.mesh.bounding_box_tree().build(local_mesh.mesh)

    # set volume parameterization tracking functional
    dx = fe.Measure("dx", domain=local_mesh.mesh, subdomain_data=local_mesh.subdomains)
    vol_form_inverse = jacobian_determinant_inverse(local_mesh, meshdata_initial)
    q_vol = fe.project(q_vol_init, V_sc)
    norm_g_vol_in = fe.assemble(g_vol*vol_form_inverse*dx(1))
    norm_g_vol_out = fe.assemble(g_vol*vol_form_inverse*dx(2))
    norm_q_vol_in = fe.assemble(q_vol*dx(1))
    norm_q_vol_out = fe.assemble(q_vol*dx(2))

    target_val = fe.assemble(fe.Constant(0.5)*(fe.Constant(norm_g_vol_in/norm_q_vol_in)*q_vol - g_vol*vol_form_inverse)
                             * (fe.Constant(norm_g_vol_in/norm_q_vol_in)*q_vol - g_vol*vol_form_inverse)*dx(1)
                             + fe.Constant(0.5)*(fe.Constant(norm_g_vol_out/norm_q_vol_out)*q_vol - g_vol*vol_form_inverse)
                             * (fe.Constant(norm_g_vol_out/norm_q_vol_out)*q_vol - g_vol*vol_form_inverse)*dx(2))

    return alpha_vol*target_val


def bilin_a_preshape(meshdata: MeshData, U: tp.Union[fe.Function, fe.dolfin.Argument], V: tp.Union[fe.Function, fe.dolfin.Argument], mu_elas: fe.Function, stekp_viscosity: float, stekp_l2: float) -> tp.Union[fe.dolfin.cpp.la.Matrix, float]:
    """Bilinear form representing weak linear elasticity with zero order terms
    for (pre-)shape gradient representations.


    Inputs:
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        U: FEniCS function representing the (pre-)shape derivative.
        V: FEniCS function representing the test function or direction.
        mu_elas: Lamé-parameters from calc_lame_par.
        stekp_viscosity: Scaling for linear elasticity component of bilinear form
            set in config.ini.
        stekp_l2: Scaling for zero order terms in bilinear form set in config.ini.


    Returns:
        value: Value of the assembled bilinear form a(U, V).
    """
    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    epsilon_V = fe.Constant(1./2.)*fe.sym(fe.nabla_grad(V))
    sigma_U = mu_elas*fe.sym(fe.nabla_grad(U))

    a = fe.Constant(stekp_viscosity)*fe.inner(sigma_U, epsilon_V)*dx('everywhere') + fe.Constant(stekp_l2)*fe.inner(U, V)*dx('everywhere')
    a = fe.assemble(a, keep_diagonal=True)

    return a


def solve_linelas_preshape(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, p: fe.Function, y: fe.Function, z: fe.Function, mu_elas: fe.Function, stekp_viscosity: float, stekp_l2: float, meshdata_initial: MeshData, tang_initial: fe.Function, q_ext: fe.Function, g_shape: fe.Function, q_vol: fe.Function, g_vol: fe.Function, MODE: str, psi: fe.Function, grad0: tp.Optional[fe.Function] = None, zeroed: bool = True) -> fe.Function:
    """
    Computes a (pre-)shape gradient representation using linear elasticity
    or p-Laplacian systems, which can be controlled by parameter LHS_Mode in
    config.ini.
    Possibly to use preshape shape and/or volume mesh regularizations.
    For a detailed discussion of possible parameters, see
    config.ini or write_config_parameters function.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        p: Adjoint according to state equation and objective functional on meshdata.
        y: State equation solution on meshdata.
        z: Projection of FEniCS function representing target data onto meshdata.
        mu_elas: Lamé-parameters from calc_lame_par in case of linear elasticity.
        stekp_viscosity: Scaling for linear elasticity component of bilinear form
            set in config.ini.
        stekp_l2: Scaling for zero order terms in bilinear form set in config.ini.
        meshdata_initial: Initial/ reference mesh, instance of the MeshData class.
        tang_initial: Tangential unit vector field on shape of meshdata_initial,
            can be computed by tangential_vector(meshdata_initial) and
            normalize_q_int with vol = False.
        q_ext: FEniCS function of globally defined target shape mesh densitites
            from config.ini.
        g_shape: Estimated initial shape mesh node densities,
            output of estimate_g_shape_averaged.
        q_vol: FEniCS function of target volume mesh densitites
            from config.ini.
        g_vol: Estimated initial volume mesh node densities,
            output of estimate_g_vol_averaged.
        MODE: Regularization mode. For descriptions, see config.ini or write_config_parameters.
            Permissible options:
            Total
            Total_ShpPrm_tang_VolPrm_inv
            Total_ShpPrm_tang_VolPrm_inv_freetangential_HoldAll
            Simult_ShapeObj_VolPrm
            Simult_ShapeObj_VolPrm_inv
            Simult_ShapeObj_VolPrm_inv_freetangential_HoldAll
            Simult_ShapeObj_ShpPrm
            Simult_ShapeObj_ShpPrm_tang
            ShapeObj
            ShapePrm
            ShapePrm_normal
            ShapePrm_tang
            ShapeVol
            ShapeVol_ShapeInv
            ShapeVol_ShapeInv_freetangential_HoldAll
            ShapeVol_scalarized_uniform_potential
            ShapeVol_scalarized_uniform_potential_shpinv
            ShapeVol_scalarized_uniform_potential_shpinv_tangouter
        psi: FEniCS expression of the variational inequality obstacle.
        grad0: Initial value for solution algorithm of (pre-)shape gradient system.
        zeroed: If True, shape derivative value for right-hand side are set to zero
            for nodes without support on the shape mesh boundary.


    Returns:
        (grad, nrm_f_elas)
        grad: (Pre)shape gradient computed by solution of (regularized) shape
            gradient system.
        nrm_f_elas: Either dual norm of right-hand-side, or dummy value.
    """
    # Parameter unpacking
    alpha_shape = float(parameters["prshp_main"]["alpha_shape"])
    alpha_volume = float(parameters["prshp_main"]["alpha_volume"])
    LHS_MODE = str(parameters["shp_grad_repr"]["LHS_Mode"])
    boundary_magnification = float(parameters["prshp_main"]["boundary_magnification"])

    # Function spaces and trial/test functions
    V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1, dim=2)
    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    u_trial = fe.TrialFunction(V_vec)
    v_test = fe.TestFunction(V_vec)

    # Right-hand-side (rhs) and boundary condition lists.
    # Assemble rhs --> gradient initialization initialisieren
    # --> zeroing --> set boundary conditions --> solving
    if MODE == "Total":
        shapeder_shpobj, shapeder_perim = shape_deriv(parameters, meshdata, p, y, z, psi, v_test)
        shapeder_shapeprm = fe.Constant(alpha_shape) * shape_deriv_shape_tracking(meshdata, meshdata_initial, tang_initial, q_ext, g_shape, v_test)
        shapeder_volprm = fe.Constant(alpha_volume) * shape_deriv_vol_tracking(meshdata, meshdata_initial, q_vol, g_vol, v_test)
        rhs_total = fe.assemble(shapeder_shpobj + shapeder_perim + shapeder_shapeprm + shapeder_volprm)

        rhs_final_vec = rhs_total

        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!

        bcs = []
        bcs += bcs_inv_outer

    elif MODE == "Total_ShpPrm_tang_VolPrm_inv":
        shapeder_shpobj, shapeder_perim = shape_deriv(parameters, meshdata, p, y, z, psi, v_test)
        shapeder_volprm = fe.Constant(alpha_volume) * shape_deriv_vol_tracking(meshdata, meshdata_initial, q_vol, g_vol, v_test)
        shapeder_shapeprm_tang = fe.Constant(alpha_shape) * shape_deriv_shape_tracking(meshdata, meshdata_initial, tang_initial, q_ext, g_shape, v_test)

        rhs_shpobj = fe.assemble(shapeder_shpobj + shapeder_perim)
        rhs_volprm_inv = fe.assemble(shapeder_volprm)
        rhs_shapeprm_tang = fe.assemble(shapeder_shapeprm_tang)

        # Projection, such that only normal components of shape derivative for
        # main shape objective are used.
        rhs_shpobj_proj = fe.Function(V_vec)
        rhs_shpobj_proj.vector()[:] = rhs_shpobj.get_local()
        rhs_shpobj_proj = normal_projection(meshdata, rhs_shpobj_proj)
        rhs_shpobj[:] = rhs_shpobj_proj.vector().get_local()

        # Set values of volume parameterization tracking preshape derivative
        # to 0 on the shape, in order to leave the shape invariant.
        v2d = fe.dolfin.vertex_to_dof_map(V_vec)
        rhs_volprm_inv[v2d[np.multiply(2, meshdata.indBoundary)]] = 0.0
        rhs_volprm_inv[v2d[np.multiply(2, meshdata.indBoundary)+1]] = 0.0

        # Project L2 gradient of shape parameterization tracking preshape derivative
        # to tangential components for approximately invariant shapes.
        rhs_shapeprm_tang_proj = fe.Function(V_vec)
        rhs_shapeprm_tang_proj.vector()[:] = rhs_shapeprm_tang.get_local()
        rhs_shapeprm_tang_proj = tangential_projection(meshdata, rhs_shapeprm_tang_proj)
        rhs_shapeprm_tang[:] = rhs_shapeprm_tang_proj.vector().get_local()

        # combined regularized right-hand-side
        rhs_total = fe.Function(V_vec)
        rhs_total.vector()[:] = rhs_shpobj.get_local() + rhs_volprm_inv.get_local() + rhs_shapeprm_tang.get_local()

        rhs_final_vec = rhs_total.vector()

        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!

        bcs = []
        bcs += bcs_inv_outer

    elif MODE == "Total_ShpPrm_tang_VolPrm_inv_freetangential_HoldAll":
        shapeder_shpobj, shapeder_perim = shape_deriv(parameters, meshdata, p, y, z, psi, v_test)
        shapeder_volprm = fe.Constant(alpha_volume) * shape_deriv_vol_tracking(meshdata, meshdata_initial, q_vol, g_vol, v_test)
        shapeder_shapeprm_tang = fe.Constant(alpha_shape) * shape_deriv_shape_tracking(meshdata, meshdata_initial, tang_initial, q_ext, g_shape, v_test)

        rhs_shpobj = fe.assemble(shapeder_shpobj + shapeder_perim)
        rhs_volprm_inv = fe.assemble(shapeder_volprm)
        rhs_shapeprm_tang = fe.assemble(shapeder_shapeprm_tang)

        # Projection, such that only normal components of shape derivative for
        # main shape objective are used.
        rhs_shpobj_proj = fe.Function(V_vec)
        rhs_shpobj_proj.vector()[:] = rhs_shpobj.get_local()
        rhs_shpobj_proj = normal_projection(meshdata, rhs_shpobj_proj)
        rhs_shpobj[:] = rhs_shpobj_proj.vector().get_local()

        # Set values of volume parameterization tracking preshape derivative
        # to 0 on the shape, in order to leave the shape invariant.
        v2d = fe.dolfin.vertex_to_dof_map(V_vec)
        rhs_volprm_inv[v2d[np.multiply(2, meshdata.indBoundary)]] = 0.0
        rhs_volprm_inv[v2d[np.multiply(2, meshdata.indBoundary)+1]] = 0.0

        # project the volume tracking preshape derivative onto the tangential
        # components of the outer hold-all boundary.
        # TODO: check for corners!
        rhs_volprm_proj = fe.Function(V_vec)
        rhs_volprm_proj.vector()[:] = np.multiply(boundary_magnification, rhs_volprm_inv.get_local())
        rhs_volprm_proj = tangential_projection_outer(meshdata, rhs_volprm_proj)

        # Project L2 gradient of shape parameterization tracking preshape derivative
        # to tangential components for approximately invariant shapes.
        rhs_shapeprm_tang_proj = fe.Function(V_vec)
        rhs_shapeprm_tang_proj.vector()[:] = rhs_shapeprm_tang.get_local()
        rhs_shapeprm_tang_proj = tangential_projection(meshdata, rhs_shapeprm_tang_proj)
        rhs_shapeprm_tang[:] = rhs_shapeprm_tang_proj.vector().get_local()

        # combined regularized right-hand-side
        rhs_total = fe.Function(V_vec)
        rhs_total.vector()[:] = rhs_shpobj.get_local() + rhs_volprm_inv.get_local() + rhs_shapeprm_tang.get_local()

        rhs_final_vec = rhs_total.vector()

        # to have tangentially free outer hold-all boundaries, set tangential
        # components of volume tracking preshape derivative as Dirichlet condition
        # on outer hold-all boundaries (tags {1, 2, 3, 4}).
        bcs_free_outer = [fe.DirichletBC(V_vec, rhs_volprm_proj, meshdata.boundaries, i) for i in range(1, 5)]

        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!

        bcs = []
        bcs += bcs_free_outer

    elif MODE == "Simult_ShapeObj_VolPrm":
        shapeder_shpobj, shapeder_perim = shape_deriv(parameters, meshdata, p, y, z, psi, v_test)
        shapeder_volprm = fe.Constant(alpha_volume) * shape_deriv_vol_tracking_noninv(meshdata, meshdata_initial, q_vol, g_vol, v_test)
        rhs_shpobj = fe.assemble(shapeder_shpobj + shapeder_perim)
        rhs_volprm_inv = fe.assemble(shapeder_volprm)

        # Projection, such that only normal components of shape derivative for
        # main shape objective are used.
        rhs_shpobj_proj = fe.Function(V_vec)
        rhs_shpobj_proj.vector()[:] = rhs_shpobj.get_local()
        rhs_shpobj_proj = normal_projection(meshdata, rhs_shpobj_proj)
        rhs_shpobj[:] = rhs_shpobj_proj.vector().get_local()

        # combined regularized right-hand-side
        rhs_simult_shobj_voltr_inv = fe.Function(V_vec)
        rhs_simult_shobj_voltr_inv.vector()[:] = rhs_shpobj.get_local() + rhs_volprm_inv.get_local()

        rhs_final_vec = rhs_simult_shobj_voltr_inv.vector()

        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!

        bcs = []
        bcs += bcs_inv_outer

    elif MODE == "Simult_ShapeObj_VolPrm_inv":
        shapeder_shpobj, shapeder_perim = shape_deriv(parameters, meshdata, p, y, z, psi, v_test)
        shapeder_volprm = fe.Constant(alpha_volume) * shape_deriv_vol_tracking(meshdata, meshdata_initial, q_vol, g_vol, v_test)
        rhs_shpobj = fe.assemble(shapeder_shpobj + shapeder_perim)
        rhs_volprm_inv = fe.assemble(shapeder_volprm)

        # Projection, such that only normal components of shape derivative for
        # main shape objective are used.
        rhs_shpobj_proj = fe.Function(V_vec)
        rhs_shpobj_proj.vector()[:] = rhs_shpobj.get_local()
        rhs_shpobj_proj = normal_projection(meshdata, rhs_shpobj_proj)
        rhs_shpobj[:] = rhs_shpobj_proj.vector().get_local()

        # Set values of volume parameterization tracking preshape derivative
        # to 0 on the shape, in order to leave the shape invariant.
        v2d = fe.dolfin.vertex_to_dof_map(V_vec)
        rhs_volprm_inv[v2d[np.multiply(2, meshdata.indBoundary)]] = 0.0
        rhs_volprm_inv[v2d[np.multiply(2, meshdata.indBoundary)+1]] = 0.0

        # combined regularized right-hand-side
        rhs_simult_shobj_voltr_inv = fe.Function(V_vec)
        rhs_simult_shobj_voltr_inv.vector()[:] = rhs_shpobj.get_local() + rhs_volprm_inv.get_local()

        rhs_final_vec = rhs_simult_shobj_voltr_inv.vector()

        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!

        bcs = []
        bcs += bcs_inv_outer

    elif MODE == "Simult_ShapeObj_VolPrm_inv_freetangential_HoldAll":
        shapeder_shpobj, shapeder_perim = shape_deriv(parameters, meshdata, p, y, z, psi, v_test)
        shapeder_volprm = fe.Constant(alpha_volume) * shape_deriv_vol_tracking(meshdata, meshdata_initial, q_vol, g_vol, v_test)
        rhs_shpobj = fe.assemble(shapeder_shpobj + shapeder_perim)
        rhs_volprm_inv = fe.assemble(shapeder_volprm)

        # Projection, such that only normal components of shape derivative for
        # main shape objective are used.
        rhs_shpobj_proj = fe.Function(V_vec)
        rhs_shpobj_proj.vector()[:] = rhs_shpobj.get_local()
        rhs_shpobj_proj = normal_projection(meshdata, rhs_shpobj_proj)
        rhs_shpobj[:] = rhs_shpobj_proj.vector().get_local()

        # Set values of volume parameterization tracking preshape derivative
        # to 0 on the shape, in order to leave the shape invariant.
        v2d = fe.dolfin.vertex_to_dof_map(V_vec)
        rhs_volprm_inv[v2d[np.multiply(2, meshdata.indBoundary)]] = 0.0
        rhs_volprm_inv[v2d[np.multiply(2, meshdata.indBoundary)+1]] = 0.0

        # project the volume tracking preshape derivative onto the tangential
        # components of the outer hold-all boundary.
        # TODO: check for corners!
        rhs_volprm_proj = fe.Function(V_vec)
        rhs_volprm_proj.vector()[:] = np.multiply(boundary_magnification, rhs_volprm_inv.get_local())
        rhs_volprm_proj = tangential_projection_outer(meshdata, rhs_volprm_proj)

        # combined regularized right-hand-side
        rhs_simult_shobj_voltr_inv = fe.Function(V_vec)
        rhs_simult_shobj_voltr_inv.vector()[:] = rhs_shpobj.get_local() + rhs_volprm_inv.get_local()

        rhs_final_vec = rhs_simult_shobj_voltr_inv.vector()

        # to have tangentially free outer hold-all boundaries, set tangential
        # components of volume tracking preshape derivative as Dirichlet condition
        # on outer hold-all boundaries (tags {1, 2, 3, 4}).
        bcs_free_outer = [fe.DirichletBC(V_vec, rhs_volprm_proj, meshdata.boundaries, i) for i in range(1, 5)]

        bcs = []
        bcs += bcs_free_outer

    elif MODE == "ShapeObj":
        shapeder_shpobj, shapeder_perim = shape_deriv(parameters, meshdata, p, y, z, psi, v_test)

        rhs_shapeobj = fe.assemble(shapeder_shpobj + shapeder_perim)

        if zeroed:
            rhs_shapeobj[meshdata.indNotIntBoundary] = 0.0

        rhs_final_vec = rhs_shapeobj

        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!

        bcs = []
        bcs += bcs_inv_outer

    elif MODE == "Simult_ShapeObj_ShpPrm_tang":
        shapeder_shpobj, shapeder_perim = shape_deriv(parameters, meshdata, p, y, z, psi, v_test)
        shapeder_shapeprm_tang = fe.Constant(alpha_shape) * shape_deriv_shape_tracking(meshdata, meshdata_initial, tang_initial, q_ext, g_shape, v_test)
        rhs_shapeprm_tang = fe.assemble(shapeder_shapeprm_tang)

        # Project L2 gradient of shape parameterization tracking preshape derivative
        # to tangential components for approximately invariant shapes.
        rhs_shapeprm_tang_proj = fe.Function(V_vec)
        rhs_shapeprm_tang_proj.vector()[:] = rhs_shapeprm_tang.get_local()
        rhs_shapeprm_tang_proj = tangential_projection(meshdata, rhs_shapeprm_tang_proj)
        rhs_shapeprm_tang[:] = rhs_shapeprm_tang_proj.vector().get_local()

        # combined regularized right-hand-side
        rhs_simult_shobj_prmtr_tang = fe.Function(V_vec)
        rhs_simult_shobj_prmtr_tang.vector()[:] = fe.assemble(shapeder_shpobj + shapeder_perim).get_local() + rhs_shapeprm_tang.get_local()

        if zeroed:
            rhs_simult_shobj_prmtr_tang.vector()[meshdata.indNotIntBoundary] = 0.0

        rhs_final_vec = rhs_simult_shobj_prmtr_tang.vector()

        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!

        bcs = []
        bcs += bcs_inv_outer

    elif MODE == "Simult_ShapeObj_ShpPrm":
        shapeder_shpobj, shapeder_perim = shape_deriv(parameters, meshdata, p, y, z, psi, v_test)
        shapeder_shapeprm_tang = fe.Constant(alpha_shape)*shape_deriv_shape_tracking_full(meshdata, meshdata_initial, tang_initial, q_ext, g_shape, v_test)
        rhs_shapeprm_tang = fe.assemble(shapeder_shapeprm_tang)

        # combined regularized right-hand-side
        rhs_simult_shobj_prmtr_tang = fe.Function(V_vec)
        rhs_simult_shobj_prmtr_tang.vector()[:] = fe.assemble(shapeder_shpobj + shapeder_perim).get_local() + rhs_shapeprm_tang.get_local()

        if zeroed:
            rhs_simult_shobj_prmtr_tang.vector()[meshdata.indNotIntBoundary] = 0.0

        rhs_final_vec = rhs_simult_shobj_prmtr_tang.vector()

        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!

        bcs = []
        bcs += bcs_inv_outer

    elif MODE == "ShapePrm":
        shapeder_shapeprm = fe.Constant(alpha_shape) * shape_deriv_shape_tracking_full(meshdata, meshdata_initial, tang_initial, q_ext, g_shape, v_test)
        rhs_shapeprm = fe.assemble(shapeder_shapeprm)
        if zeroed:
            rhs_shapeprm[meshdata.indNotIntBoundary] = 0.0

        rhs_final_vec = rhs_shapeprm

        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!

        bcs = []
        bcs += bcs_inv_outer

    elif MODE == "ShapePrm_normal":
        shapeder_shapeprm_full = fe.Constant(alpha_shape) * shape_deriv_shape_tracking_full(meshdata, meshdata_initial, tang_initial, q_ext, g_shape, v_test)
        rhs_shapeprm_full = fe.assemble(shapeder_shapeprm_full)

        # Projection, such that only normal components of preshape derivative for
        # shape parameterization tracking functional are used on the shape.
        rhs_shapeprm_full_func = fe.Function(V_vec)
        rhs_shapeprm_full_func.vector()[:] = rhs_shapeprm_full.get_local()
        rhs_shapeprm_normal_proj = normal_projection(meshdata, rhs_shapeprm_full_func)
        rhs_shapeprm_full[:] = rhs_shapeprm_normal_proj.vector().get_local()

        rhs_final_vec = rhs_shapeprm_full

        # TODO: boundary festhalten fuer MinimalSurface Berechnung
        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!

        bcs = []
        bcs += bcs_inv_outer

    elif MODE == "ShapePrm_tang":
        shapeder_shapeprm_tang = fe.Constant(alpha_shape) * shape_deriv_shape_tracking(meshdata, meshdata_initial, tang_initial, q_ext, g_shape, v_test)
        rhs_shapeprm_tang = fe.assemble(shapeder_shapeprm_tang)

        # Project L2 gradient of shape parameterization tracking preshape derivative
        # to tangential components for approximately invariant shapes.
        rhs_shapeprm_tang_proj = fe.Function(V_vec)
        rhs_shapeprm_tang_proj.vector()[:] = rhs_shapeprm_tang.get_local()
        rhs_shapeprm_tang_proj = tangential_projection(meshdata, rhs_shapeprm_tang_proj)
        rhs_shapeprm_tang[:] = rhs_shapeprm_tang_proj.vector().get_local()

        rhs_final_vec = rhs_shapeprm_tang

        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!
        bcs = []
        bcs += bcs_inv_outer

    elif MODE == "ShapeVol":
        shapeder_volprm = fe.Constant(alpha_volume) * shape_deriv_vol_tracking_noninv(meshdata, meshdata_initial, q_vol, g_vol, v_test)
        rhs_shapevol = fe.assemble(shapeder_volprm)

        rhs_final_vec = rhs_shapevol

        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!
        bcs = []
        bcs += bcs_inv_outer

    elif MODE == "ShapeVol_ShapeInv":
        shapeder_volprm = fe.Constant(alpha_volume) * shape_deriv_vol_tracking(meshdata, meshdata_initial, q_vol, g_vol, v_test)
        rhs_shapevol = fe.assemble(shapeder_volprm)

        rhs_final_vec = rhs_shapevol

        # outer hold-all boundary Dirichlet 0 condition
        u_out = fe.Constant((0.0, 0.0))
        bcs_inv_outer = [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, i) for i in range(1, 5)]
        # bcs += [fe.DirichletBC(V_vec, u_out, meshdata.boundaries, 6)]  # Trick: fixes small segment (tag 6) for meshes with _fixed_segment!

        # Set hard Dirichlet 0 condition on shape mesh for invariant shapes.
        # Permissible, since no shape objective participates.
        bcs_inv_inner = [fe.DirichletBC(V_vec, fe.Constant((0., 0.)), meshdata.boundaries, i) for i in range(5, 7)]

        bcs = []
        bcs += bcs_inv_outer
        bcs += bcs_inv_inner

    elif MODE == "ShapeVol_ShapeInv_freetangential_HoldAll":
        shapeder_volprm = fe.Constant(alpha_volume) * shape_deriv_vol_tracking(meshdata, meshdata_initial, q_vol, g_vol, v_test)
        rhs_volprm_inv = fe.assemble(shapeder_volprm)

        # project the volume tracking preshape derivative onto the tangential
        # components of the outer hold-all boundary.
        # TODO: check for corners!
        rhs_volprm_proj = fe.Function(V_vec)
        rhs_volprm_proj.vector()[:] = np.multiply(boundary_magnification, rhs_volprm_inv.get_local())
        rhs_volprm_proj = tangential_projection_outer(meshdata, rhs_volprm_proj)

        rhs_final_vec = rhs_volprm_inv

        # to have tangentially free outer hold-all boundaries, set tangential
        # components of volume tracking preshape derivative as Dirichlet condition
        # on outer hold-all boundaries (tags {1, 2, 3, 4}).
        bcs_free_outer = [fe.DirichletBC(V_vec, rhs_volprm_proj, meshdata.boundaries, i) for i in range(1, 5)]

        # Make shape invariant, by imposing hard Dirichlet 0 condition on the shape.
        # Possible, since no shape objective functional participates.
        bcs_inv_inner = [fe.DirichletBC(V_vec, fe.Constant((0., 0.)), meshdata.boundaries, i) for i in range(5, 7)]

        bcs = []
        bcs += bcs_free_outer
        bcs += bcs_inv_inner

    elif MODE == "ShapeVol_scalarized_uniform_potential" or MODE == "ShapeVol_scalarized_uniform_potential_shpinv" or MODE == "ShapeVol_scalarized_uniform_potential_shpinv_tangouter":
        # Computes curl-free gradient via potential, which comes from
        # the tangential component of the preshape derivative, and is suitable
        # to the preshape Hessian information. Works only for constant target
        # node densities.
        dS = fe.Measure("ds", domain=meshdata.mesh, subdomain_data=meshdata.boundaries)

        V_sc = fe.FunctionSpace(meshdata.mesh, "CG", 1)
        v_test = fe.TestFunction(V_sc)
        u_trial = fe.TrialFunction(V_sc)

        # define elementary participating integrads
        vol_form_inverse = jacobian_determinant_inverse(meshdata, meshdata_initial)

        if MODE == "ShapeVol_scalarized_uniform_potential":
            norm_q_vol = fe.assemble(q_vol*dx)
            deriv = fe.Constant(0.5)*((g_vol*vol_form_inverse)*(g_vol*vol_form_inverse) - (fe.Constant(1./norm_q_vol)*q_vol)*(fe.Constant(1./norm_q_vol)*q_vol))*dx
            deriv_testfunc = fe.Constant(0.5)*((g_vol*vol_form_inverse)*(g_vol*vol_form_inverse) - (fe.Constant(1./norm_q_vol)*q_vol)*(fe.Constant(1./norm_q_vol)*q_vol))*v_test*dx

            natural_Neumann = fe.assemble(deriv)/fe.assemble(fe.Constant(1.)*(dS(1)+dS(2)+dS(3)+dS(4)))

            lhs = fe.inner(fe.grad(u_trial), fe.grad(v_test))*dx

            rhs = deriv_testfunc - fe.Constant(natural_Neumann)*v_test('+')*(dS(1)+dS(2)+dS(3)+dS(4))

            u_potential = fe.Function(V_sc)
            fe.solve(lhs == rhs, u_potential, solver_parameters={'linear_solver': 'gmres', 'preconditioner': 'petsc_amg'})

            # computes gradient via potential
            grad = fe.project(fe.grad(u_potential), V_vec)

            # set gradient to 0 on corners
            grad_bproj = tangential_projection_outer(meshdata, grad)

            # set values on outer hold-all boundary to 0
            interface_inds = meshdata.indOuterBoundary
            v2d = fe.dolfin.vertex_to_dof_map(V_vec)
            grad.vector()[v2d[2*interface_inds]] = 0.
            grad.vector()[v2d[2*interface_inds+1]] = 0.

        elif MODE == "ShapeVol_scalarized_uniform_potential_shpinv" or MODE == "ShapeVol_scalarized_uniform_potential_shpinv_tangouter":
            # solves to potential problems on the inside and outside of the shape,
            # in order to achieve necessary normalization conditions for
            # invariant shapes.
            mesh_1 = fe.dolfin.SubMesh(meshdata.mesh, meshdata.subdomains, 1)
            mesh_2 = fe.dolfin.SubMesh(meshdata.mesh, meshdata.subdomains, 2)

            ds_1 = fe.Measure("ds", domain=mesh_1)
            ds_2 = fe.Measure("ds", domain=mesh_2)
            dx_1 = fe.Measure("dx", domain=mesh_1)
            dx_2 = fe.Measure("dx", domain=mesh_2)

            V_sc1 = fe.FunctionSpace(mesh_1, "CG", 1)
            V_sc2 = fe.FunctionSpace(mesh_2, "CG", 1)
            V_vec1 = fe.VectorFunctionSpace(mesh_1, "CG", 1)
            V_vec2 = fe.VectorFunctionSpace(mesh_2, "CG", 1)

            v_test1 = fe.TestFunction(V_sc1)
            v_test2 = fe.TestFunction(V_sc2)
            u_trial1 = fe.TrialFunction(V_sc1)
            u_trial2 = fe.TrialFunction(V_sc2)

            norm_g_vol_in = fe.assemble(g_vol*vol_form_inverse*dx(1))
            norm_g_vol_out = fe.assemble(g_vol*vol_form_inverse*dx(2))
            norm_q_vol_in = fe.assemble(q_vol*dx(1))
            norm_q_vol_out = fe.assemble(q_vol*dx(2))

            deriv_1 = fe.Constant(0.5)*((g_vol*vol_form_inverse)*(g_vol*vol_form_inverse) - (fe.Constant(norm_g_vol_in/norm_q_vol_in)*q_vol)*(fe.Constant(norm_g_vol_in/norm_q_vol_in)*q_vol))*dx_1
            deriv_2 = fe.Constant(0.5)*((g_vol*vol_form_inverse)*(g_vol*vol_form_inverse) - (fe.Constant(norm_g_vol_out/norm_q_vol_out)*q_vol)*(fe.Constant(norm_g_vol_out/norm_q_vol_out)*q_vol))*dx_2
            deriv_testfunc1 = fe.Constant(0.5)*((g_vol*vol_form_inverse)*(g_vol*vol_form_inverse) - (fe.Constant(norm_g_vol_in/norm_q_vol_in)*q_vol)*(fe.Constant(norm_g_vol_in/norm_q_vol_in)*q_vol))*v_test1*dx_1
            deriv_testfunc2 = fe.Constant(0.5)*((g_vol*vol_form_inverse)*(g_vol*vol_form_inverse) - (fe.Constant(norm_g_vol_out/norm_q_vol_out)*q_vol)*(fe.Constant(norm_g_vol_out/norm_q_vol_out)*q_vol))*v_test2*dx_2

            natural_Neumann1 = fe.assemble(deriv_1)/fe.assemble(fe.Constant(1.)*ds_1)
            natural_Neumann2 = fe.assemble(deriv_2)/fe.assemble(fe.Constant(1.)*ds_2)

            lhs1 = mu_elas*fe.inner(fe.grad(u_trial1), fe.grad(v_test1))*dx_1
            lhs2 = mu_elas*fe.inner(fe.grad(u_trial2), fe.grad(v_test2))*dx_2

            rhs1 = deriv_testfunc1 - fe.Constant(natural_Neumann1)*v_test1('+')*ds_1
            rhs2 = deriv_testfunc2 - fe.Constant(natural_Neumann2)*v_test2('+')*ds_2

            u_potential1 = fe.Function(V_sc1)
            u_potential2 = fe.Function(V_sc2)
            fe.solve(lhs1 == rhs1, u_potential1, solver_parameters={'linear_solver': 'gmres', 'preconditioner': 'petsc_amg'})
            fe.solve(lhs2 == rhs2, u_potential2, solver_parameters={'linear_solver': 'gmres', 'preconditioner': 'petsc_amg'})

            # compute gradient via potentials
            grad1 = fe.project(fe.grad(u_potential1), V_vec1)
            grad2 = fe.project(fe.grad(u_potential2), V_vec2)

            grad = fe.Function(V_vec)
            grad_vals = np.zeros(len(grad.vector().get_local()))
            grad1_vals = grad1.vector().get_local()
            grad2_vals = grad2.vector().get_local()

            verts_mesh1_to_parent = mesh_1.data().array("parent_vertex_indices", 0)
            verts_mesh2_to_parent = mesh_2.data().array("parent_vertex_indices", 0)

            v2d = fe.dolfin.vertex_to_dof_map(V_vec)
            v2d_mesh1 = fe.dolfin.vertex_to_dof_map(V_vec1)
            v2d_mesh2 = fe.dolfin.vertex_to_dof_map(V_vec2)

            grad_vals[v2d[2*verts_mesh1_to_parent]] = grad1_vals[v2d_mesh1[:: 2]]
            grad_vals[v2d[2*verts_mesh1_to_parent+1]] = grad1_vals[v2d_mesh1[1::2]]
            grad_vals[v2d[2*verts_mesh2_to_parent]] = grad2_vals[v2d_mesh2[:: 2]]
            grad_vals[v2d[2*verts_mesh2_to_parent+1]] = grad2_vals[v2d_mesh2[1::2]]

            grad.vector()[:] = grad_vals

            # set values to 0 on outer hold-all boundaries
            interface_inds = meshdata.indOuterBoundary
            grad.vector()[v2d[2*interface_inds]] = 0.
            grad.vector()[v2d[2*interface_inds+1]] = 0.

            # make shape invariant
            interface_inds = meshdata.indBoundary
            grad.vector()[v2d[2*interface_inds]] = 0.
            grad.vector()[v2d[2*interface_inds+1]] = 0.

            # In case of tangentially free outer hold-all boundary,
            # project the volume tracking preshape derivative onto the tangential
            # components of the outer hold-all boundary.
            # TODO: check for corners!
            if MODE == "ShapeVol_scalarized_uniform_potential_shpinv_tangouter":
                grad_bproj = tangential_projection_outer(meshdata, grad)
                grad.vector()[:] = grad.vector().get_local() + np.multiply(boundary_magnification, grad_bproj.vector().get_local())

    else:
        raise SystemExit("Mode not valid! Quitting Optmization...")

    # alternative: manual gradient calculation from 2 systems to save 1 solve
    # grad_ShapePrm_tang.vector()[:] = grad_ShapePrm.vector().get_local() - grad_ShapePrm_normal.vector().get_local()

    # left-hand-side (lhs) list for (pre-)shape gradient representations
    if not MODE == "ShapeVol_scalarized_uniform_potential" and not MODE == "ShapeVol_scalarized_uniform_potential_shpinv" and not MODE == "ShapeVol_scalarized_uniform_potential_shpinv_tangouter":
        if LHS_MODE == "LinElas":
            # create linear elasticity representation as lhs with zero order terms.
            lhs = bilin_a_preshape(meshdata, u_trial, v_test, mu_elas, stekp_viscosity, stekp_l2)
            lhs.ident_zeros()

            # apply Dirichlet conditions
            for bc in bcs:
                bc.apply(lhs)
                bc.apply(rhs_final_vec)

            # solve linear (pre-)shape gradient system
            grad = fe.Function(V_vec)
            if grad0:
                grad.vector()[:] = grad0.vector().get_local()

            # fe.solve(lhs, grad.vector(), rhs_final_vec, "gmres", "ilu")
            fe.solve(lhs, grad.vector(), rhs_final_vec)

        elif LHS_MODE == "pLaplacian":
            # In case of p-Laplacian (pre-)shape gradient representations,
            # solve non-linear system via Newtons method.
            grad = solve_p_Laplacian_vec_algebraic(parameters, meshdata, mu_elas, rhs_final_vec, grad0)

        else:
            raise SystemExit("LHS_Mode has to be LinElas or pLaplacian! Exiting...")

    # Norm of the assembled rhs
    if MODE == "ShapeObj" or MODE == "Total" or MODE == "Simult_ShapeObj_ShpPrm_tang":
        shapeder_shpobj, shapeder_perim = shape_deriv(parameters, meshdata, p, y, z, psi, v_test)
        nrm_f_elas = fe.norm(fe.assemble(shapeder_shpobj), 'L2', meshdata.mesh)
    else:
        nrm_f_elas = 1.

    return grad, nrm_f_elas


def solve_p_Laplacian_vec_algebraic(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, mu_elas: fe.Function, rhs: fe.dolfin.cpp.la.GenericVector, U0: tp.Optional[fe.Function] = None) -> fe.Function:
    """
    Solves an algebraic version of the p-Laplacian system with stabilization
    parameter eps^2. The weak formulation is given as

    (eps_pLapl**2 + grad(U)^T grad(U))**(0.5*p_pLapl-1.) * mu_elas*grad(U)^T grad(V) = RHS


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        mu_elas: Lamé-parameters from calc_lame_par for locally varying
            p-Laplacian coefficients.
        rhs: Right-hand-side for the stabilized p-Laplacian system, usually
            consisting of (regularized) (pre-)shape derivatives.
        U0: Initial value for Newtons method to solve the non-linear system.


    Returns:
        grad: Solution of the stabilized p-Laplacian system.


    References:
        https://www2.karlin.mff.cuni.cz/~hron/fenics-tutorial/p_laplace/doc.html
        https://home.simula.no/~hpl/homepage/fenics-tutorial/release-1.0/webm/nonlinear.html
    """
    # Parameters unpacking
    eps = float(parameters["shp_grad_repr"]["eps_pLapl"])
    p = int(parameters["shp_grad_repr"]["p_pLapl"])

    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)
    V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1)

    eps_const = fe.Constant(eps)
    max_iter = 100
    tol_abs_Newton = 1.e-8
    tol_rel_Newton = 1.e-6
    Newton_relaxation = 1.

    # Initial approximation for Newton
    grad = fe.Function(V_vec)
    grad_incr = fe.Function(V_vec)
    dgrad = fe.TrialFunction(V_vec)
    V = fe.TestFunction(V_vec)

    # set initial condition
    if U0:
        grad.vector()[:] = U0.vector().get_local()

    # f = fe.Expression(("pow((x[0]-0.5), 3)", "sin(20.*x[1])+2."), domain=meshdata.mesh, degree=1)
    # rhs = fe.assemble(fe.inner(f, V)*dx)

    # Energy functional
    # Energy_pLapl = fe.Constant(1./p)*(eps_const**2 + fe.inner(fe.grad(grad), fe.grad(grad)))** fe.Constant(0.5*p)*dx - fe.inner(f, grad)*dx

    # First Gateaux derivative of Energy (no rhs!)
    J = (eps_const**2 + fe.inner(fe.grad(grad), fe.grad(grad)))**fe.Constant(0.5*p-1.) * mu_elas*fe.inner(fe.grad(grad), fe.grad(V))*dx

    # Second Gateaux derivative of Energy
    K = fe.Constant(p-2.)*(eps_const**2 + fe.inner(fe.grad(grad), fe.grad(grad)))**fe.Constant(0.5*p-2.) * mu_elas*fe.inner(fe.grad(grad), fe.grad(V))*fe.inner(fe.grad(grad), fe.grad(dgrad))*dx \
        + (eps_const**2 + fe.inner(fe.grad(grad), fe.grad(grad)))**fe.Constant(0.5*p-1.) * mu_elas*fe.inner(fe.grad(dgrad), fe.grad(V))*dx

    # Boundary Conditions
    bcs = [fe.DirichletBC(V_vec, fe.Constant((0., 0.)), meshdata.boundaries, i) for i in range(1, 5)]

    iter_nr = 0
    nrm_res_abs = 1.
    nrm_res_rel = 1.
    nrm_res_ini = 1.
    print("Solve Newton System for p-Laplacian with p={0:f} and eps={1:f}".format(p, eps))
    while nrm_res_abs > tol_abs_Newton and nrm_res_rel > tol_rel_Newton and iter_nr < max_iter:
        iter_nr += 1

        # assemble first Gateaux derivative of Energy with non-trivial rhs
        J_ass = fe.assemble(J)
        J_ass[:] += -rhs.get_local()

        # assemble second Gateaux derivative of Energy as lhs
        K_ass = fe.assemble(K)

        for bc in bcs:
            bc.apply(J_ass)
            bc.apply(K_ass)

        # solve linear subproblem
        fe.solve(K_ass, grad_incr.vector(), -J_ass, "gmres", "ilu")
        # info(LinearVariationalSolver.default_parameters(), True)

        # Residual calculation
        nrm_res_abs = np.linalg.norm(grad_incr.vector().get_local())
        if iter_nr == 1:
            nrm_res_ini = nrm_res_abs
        nrm_res_rel = nrm_res_abs/nrm_res_ini

        # update Newton step with relaxation factor
        grad.vector()[:] += np.multiply(Newton_relaxation, grad_incr.vector().get_local())

        print("{0:d} Newton solver: res_abs: {1:4E}   res_rel: {2:4E}".format(iter_nr, nrm_res_abs, nrm_res_rel))

    # Compute energies
    # energy_regularized = fe.assemble(Energy_pLapl)
    # eps_const.assign(0.0)
    # energy = fe.assemble(Energy_pLapl)
    # print("Energy: {0:4e} Energy_reg: {1:4e}".format(energy, energy_regularized))

    return grad


def solve_p_Laplacian_vec(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, U0: tp.Optional[fe.Function] = None) -> fe.Function:
    """
    !not used!
    Solves an PETSc vector version of the p-Laplacian system with stabilization
    parameter eps^2. Uses an exemplary right-hand-side. The weak formulation is
    given as

    (eps_pLapl**2 + grad(U)^T grad(U))**(0.5*p_pLapl-1.) * grad(U)^T grad(V) = RHS


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        U0: Initial value for Newtons method to solve the non-linear system.


    Returns:
        grad: Solution of the stabilized p-Laplacian system.


    References:
        https://www2.karlin.mff.cuni.cz/~hron/fenics-tutorial/p_laplace/doc.html
        https://home.simula.no/~hpl/homepage/fenics-tutorial/release-1.0/webm/nonlinear.html
    """
    # Parameters unpacking
    eps = float(parameters["shp_grad_repr"]["eps_pLapl"])
    p = int(parameters["shp_grad_repr"]["p_pLapl"])

    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)
    V_vec = fe.VectorFunctionSpace(meshdata.mesh, "CG", 1)

    eps = fe.Constant(eps)

    # Initial approximation for Newton
    grad = fe.Function(V_vec)
    dgrad = fe.TrialFunction(V_vec)
    V = fe.TestFunction(V_vec)
    if U0:
        grad.vector()[:] = U0.vector().get_local()

    # exemplary right-hand-side
    f = fe.Expression(("pow((x[0]-0.5), 3)", "sin(20.*x[1])+2."), domain=meshdata.mesh, degree=1)

    # Energy functional
    Energy_pLapl = fe.Constant(1./p)*(eps**2 + fe.inner(fe.grad(grad), fe.grad(grad)))**fe.Constant(0.5*p)*dx - fe.inner(f, grad)*dx

    # First Gateaux derivative of Energy
    J = (eps**2 + fe.inner(fe.grad(grad), fe.grad(grad)))**fe.Constant(0.5*p-1.)*fe.inner(fe.grad(grad), fe.grad(V))*dx - fe.inner(f, V)*dx

    # Second Gateaux derivative of Energy
    K = fe.Constant(p-2.)*(eps**2 + fe.inner(fe.grad(grad), fe.grad(grad)))**fe.Constant(0.5*p-2.) * fe.inner(fe.grad(grad), fe.grad(V))*fe.inner(fe.grad(grad), fe.grad(dgrad))*dx \
        + (eps**2 + fe.inner(fe.grad(grad), fe.grad(grad)))**fe.Constant(0.5*p-1.) * fe.inner(fe.grad(dgrad), fe.grad(V))*dx

    # Solve nonlinear problem; Newton is used by default
    bcs = [fe.DirichletBC(V_vec, fe.Constant((0., 0.)), meshdata.boundaries, i) for i in range(1, 5)]

    # Nonlinear solver
    problem = fe.NonlinearVariationalProblem(J, grad, bcs, K)
    solver = fe.NonlinearVariationalSolver(problem)

    prm = solver.parameters['newton_solver']
    # info(parameters, verbose=True)
    prm['absolute_tolerance'] = 1E-7
    prm['relative_tolerance'] = 1E-6
    prm['maximum_iterations'] = 70
    prm['relaxation_parameter'] = 1.0
    prm['linear_solver'] = 'gmres'
    prm['preconditioner'] = 'ilu'
    prm['krylov_solver']['absolute_tolerance'] = 1E-8
    prm['krylov_solver']['relative_tolerance'] = 1E-7
    prm['krylov_solver']['maximum_iterations'] = 1000
    # prm['newton_solver']['krylov_solver']['gmres']['restart'] = 40
    # prm['newton_solver']['preconditioner']['ilu']['fill_level'] = 0
    # set_log_level(PROGRESS)

    solver.solve()

    # Compute energies
    energy_regularized = fe.assemble(Energy_pLapl)
    eps.assign(0.0)
    energy = fe.assemble(Energy_pLapl)

    file_test = fe.File(os.path.join(__this_files_dir, "test_UFL.pvd"))
    file_test << grad

    return grad


def solve_signed_distance(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, subdomain_targ: fe.MeshFunction, subdomain_targ_tags: tp.List[int], eps_Newton: float = 0.9, s_dist_initial: tp.Optional[fe.Function] = None) -> fe.Function:
    """
    Solves a stabilized Eikonal equatin with Newtons method in order to compute
    an approximate signed distance function to a given target domain.
    Solver parameters can be adjusted below. Stabilized Eikonal equation is of
    the form

    (grad(s_dist)^T grad(s_dist))**(1./2.)*v + eps_stab*(grad(s_dist)^T grad(v)) - 1.*v = 0,
    with Dirichlet condition
    s_dist = 0 on the target subdomain_targ.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        subdomain_targ: FEniCS MeshFunction with subdomain tags, to which signed
            distance function is computed via Dirichlet boundaries.
        subdomain_targ_tags: List of volume mesh node indices associated to the
            target subdomain subdomain_targ.
        eps_Newton: Relaxation/damping parameter for the Newton method.
        s_dist_initial: Initial value for Newtons method to solve the non-linear system.


    Returns:
        s_dist: Solution of the stabilized Eikonal equation.
    """
    # Parameters unpacking
    eps_stab_fac = float(parameters["prshp_sign_dist"]["eps_stab"])
    eps_stab_dyn = parameters["prshp_sign_dist"]["eps_stab_dyn"] == "True"

    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)
    V_sc = fe.FunctionSpace(meshdata.mesh, "CG", 1)

    # True: stab_param = eps_stab*MeshData.mesh.hmax(), False: stab_param = eps_stab
    if eps_stab_dyn:
        eps_stab = eps_stab_fac*meshdata.mesh.hmax()
    else:
        eps_stab = eps_stab_fac

    # Initial approximation for Newton
    s_dist = fe.Function(V_sc)
    ds_dist = fe.TrialFunction(V_sc)
    v_test = fe.TestFunction(V_sc)

    if subdomain_targ_tags not in subdomain_targ.array():
        print("No target set for signed distance function found! Returning constant 1 Function")
        s_dist.vector()[:] = 1.
        return s_dist

    # Boundary to which distance is calculated
    bcs = [fe.DirichletBC(V_sc, fe.Constant(0.), subdomain_targ, i) for i in subdomain_targ_tags]

    if s_dist_initial:
        s_dist.vector()[:] = s_dist_initial.vector().get_local()
    else:
        lhs = fe.inner(fe.grad(ds_dist), fe.grad(v_test))*dx
        rhs = fe.Constant(1.)*v_test*dx
        fe.solve(lhs == rhs, s_dist, bcs)

    # Stabilized Eikonal Equation
    J = fe.sqrt(fe.inner(fe.grad(s_dist), fe.grad(s_dist)))*v_test*dx + fe.Constant(eps_stab)*fe.inner(fe.grad(s_dist), fe.grad(v_test))*dx - fe.Constant(1.)*v_test*dx

    # First Variation of Stab. Eikonal Eq.
    K = fe.Constant(-2.)*fe.sqrt(fe.inner(fe.grad(s_dist), fe.grad(s_dist)))**(-1)*fe.inner(fe.grad(s_dist), fe.grad(ds_dist))*v_test*dx \
        + fe.Constant(eps_stab)*fe.inner(fe.grad(ds_dist), fe.grad(v_test))*dx

    # Nonlinear solver
    problem = fe.NonlinearVariationalProblem(J, s_dist, bcs, K)
    solver = fe.NonlinearVariationalSolver(problem)

    prm = solver.parameters['newton_solver']
    # info(parameters, verbose=True)
    prm['absolute_tolerance'] = 1E-7
    prm['relative_tolerance'] = 1E-6
    prm['maximum_iterations'] = 70
    prm['relaxation_parameter'] = eps_Newton
    prm['linear_solver'] = 'gmres'
    prm['preconditioner'] = 'ilu'
    prm['krylov_solver']['absolute_tolerance'] = 1E-8
    prm['krylov_solver']['relative_tolerance'] = 1E-7
    prm['krylov_solver']['maximum_iterations'] = 1000
    # prm['newton_solver']['krylov_solver']['gmres']['restart'] = 40
    # prm['newton_solver']['preconditioner']['ilu']['fill_level'] = 0
    # set_log_level(PROGRESS)

    solver.solve()

    return s_dist


def target_f_distance_shape(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, signed_dist: fe.Function, outputfolder: str, iter_nr: int) -> fe.Function:
    """
    Transformation function in order to compute the target function for volume
    parameterization tracking with respect to active set boundaries of VIs via
    the signed distance function. The transformation is given by F(T(x)), where

    F(x) = scaling_target_tr*beta_target_tr/(2.*alpha_target_tr*sp.gamma(1./beta_target_tr))
            * exp(-|(x - location_target_tr)/alpha_target_tr|**beta_target_tr) + eps_target_tr,
    where sp.gamma is the gamma function, and T(x): [min_val, max_val] -> [0, 1] linearly.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        signed_dist: FEniCS function representing the approximate signed distance
            function from solve_signed_distance.
        outputfolder: String of the outputfolder name, return of create_outputfolder.
        iter_nr: Iteration number of the optimization loop.


    Returns:
        f_target: FEniCS scalar function acting as the target function for
            volume parameterization tracking with respect to the active set
            boundary of a variational inequality.
    """
    # Parameters unpacking
    alpha = float(parameters["prshp_sign_dist"]["alpha_target_tr"])
    beta = float(parameters["prshp_sign_dist"]["beta_target_tr"])
    location = float(parameters["prshp_sign_dist"]["location_target_tr"])
    scaling = float(parameters["prshp_sign_dist"]["scaling_target_tr"])
    eps = float(parameters["prshp_sign_dist"]["eps_target_tr"])

    V_sc = fe.FunctionSpace(meshdata.mesh, "CG", 1)
    f_target = fe.Function(V_sc)
    # v2d = fe.dolfin.vertex_to_dof_map(V_sc)

    # generalized normal
    # f_vals = np.multiply(beta/(2.*alpha*sp_gmma(1./beta)), np.exp(-np.power(np.multiply(1./alpha, signed_dist.vector().get_local()), beta)))

    max_val = np.max(signed_dist.vector().get_local())  # 2.35  # oder diam(HoldAll), gibts immer da kompakta beschraenkt (sonst haengt das von max der abst. ab...)
    min_val = np.min(signed_dist.vector().get_local())  # 0.  # kleinmoeglicher Abstand

    # linear transformation [min, max] -> [c, d], c can also be greater d for a mirroring
    c = 0.
    d = 1.
    transf_vals = np.multiply((d-c)/(max_val - min_val), signed_dist.vector().get_local() - min_val) + c  # Werte gespiegelt auf [0, 1] transformiert

#    # CDF of the Kumaraswamy distribution, rescaled and mirrored (c=1, d=0)
#    f_vals    = np.zeros(len(signed_dist.vector().get_local()))
#    f_vals[:] = 1.  # 500.*(1. - np.power(1. - np.power(c, alpha2), beta2)) + eps2
#    f_vals[v2d[meshdata.indVerticesBySubdomain[0]]] = 15.*(1. - np.power(1. - np.power(transf_vals[v2d[meshdata.indVerticesBySubdomain[0]]], alpha), beta)) + eps
#    f_vals[v2d[meshdata.indVerticesBySubdomain[1]]] = 500.*(1. - np.power(1. - np.power(transf_vals[v2d[meshdata.indVerticesBySubdomain[1]]], alpha2), beta2)) + eps2
#    f_vals    = 15.*(1. - np.power(1. - np.power(transf_vals[v2d[meshdata.indVerticesBySubdomain[0]]], alpha), beta)) + eps
#
#    # PDF of the generalized normal distribution
#    f_vals[v2d[meshdata.indVerticesBySubdomain[0]]] = 7.5*np.exp(-np.power(np.abs(transf_vals[v2d[meshdata.indVerticesBySubdomain[0]]] - location)/scale, shape)) + eps
#    f_vals[v2d[meshdata.indVerticesBySubdomain[1]]] = 200.*np.exp(-np.power(np.abs(transf_vals[v2d[meshdata.indVerticesBySubdomain[1]]] - location)/scale2, shape2)) + eps2
    f_vals = scaling*beta/(2.*alpha*sp.gamma(1./beta))*np.exp(-np.power(np.abs(transf_vals - location)/alpha, beta)) + eps

    f_target.vector()[:] = f_vals
#    f_target.vector()[:] = transf_vals + eps

    # for troubleshooting/designing transformation of sign_dist target
#    if iter_nr <= 1:
#        xs_trgt = np.arange(c, d, 1./100.)
#        ys_trgt = scaling*beta/(2.*alpha*sp.gamma(1./beta))*np.exp(-np.power(np.abs(xs_trgt - location)/alpha, beta)) + eps
#        plot, = plt.plot(xs_trgt, ys_trgt, label = "q_vol_transformation")
#
#        legend_mshd = plt.legend(handles=[plot], loc=1)
#        plt.savefig(os.path.join(outputfolder, 'q_vol_transformation'))
#        plt.show()

    return f_target


def mesh_condition(meshdata: MeshData) -> tp.Tuple[float, ...]:
    """
    Computes minimum edge length, maximum edge length, minimum inradious,
    maximum inradoius, covering radius, and mesh condition numbers.
    """
    min_edge = meshdata.mesh.hmin()
    max_edge = meshdata.mesh.hmax()
    min_inrad = meshdata.mesh.rmin()
    max_inrad = meshdata.mesh.rmax()

    # cov_rad mit MonteCarlo uniformer Punkte schaetzen
    cov_rad = 1.
    condition = cov_rad/min_edge

    return min_edge, max_edge, min_inrad, max_inrad, cov_rad, condition


def laplace_beltrami_cond(meshdata: MeshData) -> tp.Tuple[fe.Function, fe.Function, float]:
    """
    Calculates condition of surface Laplacian mass matrix on the shape mesh
    using Frobenius norms and the negative Laplace-Beltrami operator
    -LaplBeltr(u) + c = g, integral (u) = 0 to inforce uniqueness of solutions
    (invertability of massmatrix).
    """
    submesh = fe.dolfin.SubMesh(meshdata.mesh, meshdata.subdomains, 2)
    boundarymesh = fe.dolfin.BoundaryMesh(submesh, "exterior")
#    global_normal = fe.Expression(("x[0]", "x[1]"), degree = 1)
#    boundarymesh.init_cell_orientations(global_normal)
    dx_shape = fe.Measure("dx", domain=boundarymesh)

    # Construct mixed Function Spaces
#    V_cg = fe.FunctionSpace(meshdata.mesh, "CG", 1)
    V_boundary_cg = fe.FiniteElement("CG", boundarymesh.ufl_cell(), 1)
    V_boundary_dg = fe.FunctionSpace(boundarymesh, "DG", 0)
    V_reals = fe.FiniteElement("R", boundarymesh.ufl_cell(), 0)
    V_mixed_el = V_boundary_cg*V_reals
    V_mixed = fe.FunctionSpace(boundarymesh, V_mixed_el)

    # lhs
    (testfunc_u, testfunc_c) = fe.TestFunction(V_mixed)
    (trialfunc_u, trialfunc_c) = fe.TrialFunction(V_mixed)
    sol = fe.Function(V_mixed)

    lhs_LaplBeltr = fe.inner(fe.grad(trialfunc_u), fe.grad(testfunc_u))*dx_shape + trialfunc_c*testfunc_u*dx_shape + trialfunc_u*testfunc_c*dx_shape

    # calculate condition using Frobenius norms
    laplBeltr_MassMatrix = fe.assemble(lhs_LaplBeltr).array()
    laplBeltr_MassMatrix_inv = np.linalg.inv(laplBeltr_MassMatrix)
    condition = np.linalg.norm(laplBeltr_MassMatrix, ord=2)*np.linalg.norm(laplBeltr_MassMatrix_inv, ord=2)

    # rhs
    g_shape = fe.project(fe.Expression("sin(x[0])*sin(x[1])", degree=1), V_boundary_dg)
    rhs = fe.inner(g_shape, testfunc_u)*dx_shape + fe.Constant(0.)*testfunc_c*dx_shape

    # solve
    fe.solve(lhs_LaplBeltr == rhs, sol)

    # split solutions
    u = sol.sub(0)
    c = sol.sub(1)

    return u, c, condition


def laplace_cond(meshdata: MeshData) -> tp.Tuple[fe.Function, float]:
    """
    Calculates condition of Laplacian mass matrix on the volume mesh (hold-all)
    using Frobenius norms and the negative Laplacian
    -LaplBeltr(u) = g, u = 0 on the outer boundary to inforce uniqueness of
    solutions (invertability of massmatrix).
    """
    dx = fe.Measure("dx", domain=meshdata.mesh)

    # Construct Function Space
    V_cg = fe.FunctionSpace(meshdata.mesh, "CG", 1)

    # lhs
    testfunc = fe.TestFunction(V_cg)
    trialfunc = fe.TrialFunction(V_cg)
    sol = fe.Function(V_cg)

    bcs = [fe.DirichletBC(V_cg, fe.Constant(0.), meshdata.boundaries, i) for i in range(1, 5)]

    lhs_Lapl = fe.inner(fe.grad(trialfunc), fe.grad(testfunc))*dx

    # calculate condition using Frobeniusnorms
    lapl_ass = fe.assemble(lhs_Lapl)

    for bc in bcs:
        bc.apply(lapl_ass)

    lapl_massmatrix = lapl_ass.array()
    lapl_massmatrix_inv = np.linalg.inv(lapl_massmatrix)
    condition = np.linalg.norm(lapl_massmatrix, ord=2)*np.linalg.norm(lapl_massmatrix_inv, ord=2)

    # rhs
    g_shape = fe.project(fe.Expression("sin(x[0])*sin(x[1])", degree=1), V_cg)
    rhs = fe.inner(g_shape, testfunc)*dx

    # solve
    fe.solve(lhs_Lapl == rhs, sol, bcs)

    return sol, condition


# VARIATIONAL INEQUALITIES
def initialize_lambda_bar(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, psi: fe.Function, CG: bool = True) -> fe.Function:
    """
    Initializes the approximate dual variable to the variational inequality,
    such that semi-smooth Newton methods yield solutions respecting the VI
    obstacle constraint.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        psi: FEniCS expression of the variational inequality obstacle.
        CG: If True, the return is given as a CG function,
            else the return is a DG function.


    Returns:
        labmda_bar: Approximate dual variable given by max(0, f - Apsi), where
            A is the differential operator of the elliptic part of the VI. In
            Our case it is -Laplacian.
    """
#    dx = fe.Measure('dx', domain=meshdata.mesh, subdomain_data=meshdata.subdomains)
    V = fe.FunctionSpace(meshdata.mesh, "CG", 2)
    if not CG:
        V = fe.FunctionSpace(meshdata.mesh, "DG", 2)

#    psi_V    = fe.project(psi, V, solver_type='cg', preconditioner_type='icc')
    psi_V = fe.interpolate(psi, V)
    lapl_psi = fe.project(fe.nabla_div(fe.grad(psi_V)), V, solver_type='cg', preconditioner_type='icc')
    f = initialize_f(parameters, meshdata)

    lambda_bar = fe.project(fe.conditional(fe.ge(0., f + lapl_psi), 0., f + lapl_psi), V, solver_type='cg', preconditioner_type='icc')

    return lambda_bar


def initialize_f(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, CG: bool = False) -> fe.Function:
    """
    Initializes the piecewise constant shape dependent source term of the PDE
    or VI as a FEniCS function.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        CG: If True, the return is given as a CG function,
            else the return is a DG function.


    Returns:
        f_er: Piecewise constant shape dependent source term of the PDE
            or VI represented as a FEniCS function.
            f_er = pde_source_term_1 on the outside of the shape, and
            f_er = pde_source_term_2 on the inside of the shape.
    """
    f1 = fe.Constant(float(parameters["shp_main"]["pde_source_term_1"]))
    f2 = fe.Constant(float(parameters["shp_main"]["pde_source_term_2"]))

    dx = fe.Measure('dx', domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    V_dg = fe.FunctionSpace(meshdata.mesh, "DG", 1)
    f_er = fe.Function(V_dg)
    f_trial = fe.TrialFunction(V_dg)
    v = fe.TestFunction(V_dg)

    lhs = f_trial*v*dx
    rhs = f1*v*dx(1) + f2*v*dx(2)
    bcs_dg = [fe.DirichletBC(V_dg, fe.Constant(0.), meshdata.boundaries, i) for i in range(1, 5)]

    fe.solve(lhs == rhs, f_er, bcs_dg)

    if CG:
        f_er = fe.project(f_er, fe.FunctionSpace(meshdata.mesh, "CG", 1))

    return f_er


def solve_state_gamma_regu(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, psi: fe.Function) -> fe.Function:
    """
    Solve the state equation given by the regularized and smoothed obstacle
    problem

    a(y, v) + max_gamma(lambda_bar + c_regu_VI*(y - vi_obstacle)) = source_term,
    where smoothed max function max_gamma is given by
    max_gamma(x) = 0, if x <= -1./gamma_regu_VI
                 = 0.25*gamma_regu_VI*x**2 + 0.5*x + 0.25/gamma_regu_VI, if -1./gamma_regu_VI < x < 1./gamma_regu_VI
                 = 1, if x >= 1./gamma_regu_VI.

    Solution methods can be adjusted in the function body below, regularization
    parameters of the VI are controlled in the config.ini file.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        psi: FEniCS expression or function of the variational inequality obstacle.


    Returns:
        y: FEniCS function solution to the regularized and smoothed obstacle problem.
    """
    # parameters unpacking
    f1 = fe.Constant(float(parameters["shp_main"]["pde_source_term_1"]))
    f2 = fe.Constant(float(parameters["shp_main"]["pde_source_term_2"]))
    c = float(parameters["shp_VI"]["c_regu_VI"])
    gamma = float(parameters["shp_VI"]["gamma_regu_VI"])
    eps_tol_abs = float(parameters["shp_VI"]["eps_tol_abs_VI"])
    eps_tol_rel = float(parameters["shp_VI"]["eps_tol_rel_VI"])

    V = fe.FunctionSpace(meshdata.mesh, "CG", 1)

    psi = fe.project(psi, V)
    v = fe.TestFunction(V)
    y = fe.Function(V)

    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    bcs = [fe.DirichletBC(V, 0., meshdata.boundaries, i) for i in range(1, 5)]
    lmbda_bar = initialize_lambda_bar(parameters, meshdata, psi)

    # set non-linear term
    affine_comb = lmbda_bar + c*(y-psi)
    max_gam = fe.conditional(fe.le(affine_comb, -(1./gamma)), 0,
                             fe.conditional(fe.lt(affine_comb, (1./gamma)),
                                            0.25*gamma*(affine_comb)**2 + 0.5*affine_comb + 1./(4.*gamma), affine_comb
                                            )
                             )

    F = fe.inner(fe.grad(y), fe.grad(v))*dx + max_gam*v*dx \
        - f1*v*dx(1) - f2*v*dx(2)

    # compute solution
    fe.solve(F == 0, y, bcs, solver_parameters={"newton_solver":
                                                {"linear_solver": "cg",
                                                 "preconditioner": "icc",
                                                 "absolute_tolerance": eps_tol_abs,
                                                 "relative_tolerance": eps_tol_rel,
                                                 "maximum_iterations": 50000}
                                                })
    return y


def active_set_boundary(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, y: fe.Function, lambda_bar: fe.Function, psi: fe.Function, eps: float) -> fe.MeshFunction:
    """
    Computes a FEniCS FacetFunction on the active edges of the VI.
    Active edges are found as edges of nodes, which satisfy the condition

    lambda_bar + c*(y-psi) > -eps.

    This means there are no 0D edges, i.e. no active points!


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        y: Solution to a (regularized and/or smoothed) VI.
        lambda_bar: Approximate dual variable given by max(0, f - Apsi),
            return of initialize_lambda_bar.
        psi: FEniCS expression or function of the variational inequality obstacle.
        eps: Parameter controlling tolerance for activity condition of nodes.


    Returns:
        indicator_A: FEniCS FacetFunction with active set edges.
            indicator_A = 0 for inactive edges, and
            indicator_A = 1 for active edges.
    """
    # parameters unpacking
    c = float(parameters["shp_VI"]["c_regu_VI"])

    V = fe.FunctionSpace(meshdata.mesh, "CG", 1)
    active_condition = fe.project(lambda_bar + c*(y-psi), V)

    # compute active DoFs and vertices
    active_dofs = np.where(active_condition.vector().get_local() > -eps)[0]
    d2v = fe.dolfin.dof_to_vertex_map(V)
    active_verts = d2v[active_dofs]
    active_verts_list = list(set(active_verts))

    # compute general active facets
    active_facet_list = []
    counter = 0
    for f in fe.facets(meshdata.mesh):
        for v in fe.vertices(f):
            if v.index() in active_verts_list:
                counter += 1
            if counter == 2:
                active_facet_list.append(f.index())
        counter = 0
    active_facet_list = list(set(active_facet_list))

    # find active facets, which are at the boundary of the active set
    active_boundary_list = []
    counter_out = 0
    for f_index in active_facet_list:
        cell_count = 0
        for c in fe.cells(fe.Facet(meshdata.mesh, f_index)):
            cell_count += 1
            non_active_count = 0
            active_count = 0
            for v in fe.vertices(c):
                if v.index() in active_verts_list:
                    active_count += 1
                else:
                    non_active_count += 1
            if active_count == 2 and non_active_count == 1:
                counter_out += 1
        if counter_out == 1 and cell_count == 2:
            active_boundary_list.append(f_index)
        counter_out = 0
    active_boundary_list = list(set(active_boundary_list))

    indicator_A = fe.MeshFunction("size_t", meshdata.mesh, 1)
    for i in range(0, len(indicator_A)):
        if i in active_boundary_list:
            indicator_A[i] = 1
        else:
            indicator_A[i] = 0

#    # for visualization via paraview
#    filer = fe.File(os.path.join(__this_files_dir, "active_points.pvd"))
#    filer << active_condition
    return indicator_A


def sign_difference(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, y_c: fe.Function, y_gamma_c: fe.Function, psi_actual: fe.Function, eps_sign: float) -> tp.Tuple[float, float]:
    """
    Computes the L1 and L2 norms of the critical convergence assumption for the
    adjoints of the smoothly regularized varitional inequalities. The objects,
    whose norms are taken is the difference

    sign_gamma_regu_VI(lambda_bar + c_regu_VI*(y_gamma_c - psi_actual))
    - sign(lambda_bar + c_regu_VI*(y_c - psi_actual)).

    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        y_c: FEniCS function representing solution of the regularized VI.
            Return of solve_state_c_regu.
        y_gamma_c: FEniCS function representing solution of the regularized and
            smoothed VI. Return of solve_state_gamma_regu.
        psi_actual: FEniCS expression or function of the variational inequality obstacle.
        eps_sign: Parameter controlling tolerance for activity condition of nodes,
            see function active_set_boundary.


    Returns:
        (l1_norm, l2_norm)
        l1_norm: L1 norm of the difference of interest.
        l2_norm: L2 norm of the difference of interest.
    """
    # parameters unpacking
    c_regu_VI = float(parameters["shp_VI"]["c_regu_VI"])
    gamma_regu_VI = float(parameters["shp_VI"]["gamma_regu_VI"])

    V = fe.FunctionSpace(meshdata.mesh, "CG", 1)

    # calculate sign_gamma(lambda_bar + c(y_gamma_c - psi_actual))
    lambda_bar = initialize_lambda_bar(parameters, meshdata, psi_actual)
#    affine_comb = fe.project(lambda_bar + c_reg*(y_gamma_c - psi_actual), V)
#    sign_gamma = fe.conditional(fe.le(affine_comb, -(1./gamma)), 0.,
#                          fe.conditional(fe.lt(affine_comb, (1./gamma)), 0.5*gamma*affine_comb + 0.5, 1.))
    sign_gamma = fe.conditional(fe.le(lambda_bar + c_regu_VI*(y_gamma_c - psi_actual), -(1./gamma_regu_VI)), 0.,
                                fe.conditional(fe.lt(lambda_bar + c_regu_VI*(y_gamma_c - psi_actual), (1./gamma_regu_VI)), 0.5*gamma_regu_VI*(lambda_bar + c_regu_VI*(y_gamma_c - psi_actual)) + 0.5, 1.)
                                )
    sign_gamma = fe.project(sign_gamma, V, solver_type='cg', preconditioner_type='icc')

    # calculate sign(lambda_bar + c(y_c - psi_actual))
#    lambda_bar = sbib.initialize_lambda_bar(parameters, MeshData, psi_actual)
#    affine_comb = fe.project(lambda_bar + c_reg*(y_c - psi_actual), V)
    sign = fe.conditional(fe.le(lambda_bar + c_regu_VI*(y_c - psi_actual), eps_sign), 0., 1.)
    sign = fe.project(sign, V, solver_type='cg', preconditioner_type='icc')

    # save L1_norm of difference, save L2_norm of difference
    dx = fe.Measure("dx", domain=meshdata.mesh)
    l1_norm = fe.assemble(abs(sign_gamma - sign)*dx)
    l2_norm = np.sqrt(fe.assemble((sign_gamma - sign)**2*dx))

#    __file_differences << fe.project(abs(sign_gamma - sign), V, solver_type='cg', preconditioner_type='icc')
    return l1_norm, l2_norm


def solve_state_unreg(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, psi: fe.Function, obstacle: bool = True) -> fe.Function:
    """
    Solves unregularized obstacle problem with a primal-dual active set method.
    Can also solve PDE versions of the obstacle problem without obstacle.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        psi: FEniCS expression or function of the variational inequality obstacle.
        obstacle: If True, the obstacle problem is solved.
            Else, the obstacle is omitted, and the according PDE version is solved.


    Returns:
        y: FEniCS function of the solution to the VI/PDE.
    """
    # parameters unpacking
    f1 = fe.Constant(float(parameters["shp_main"]["pde_source_term_1"]))
    f2 = fe.Constant(float(parameters["shp_main"]["pde_source_term_2"]))
    gamma = float(parameters["shp_VI"]["gamma_regu_VI"])
    eps_tol_abs = float(parameters["shp_VI"]["eps_tol_abs_VI"])
#    eps_tol_rel = float(parameters["shp_VI"]["eps_tol_rel_VI"])

    # function space
    V = fe.FunctionSpace(meshdata.mesh, "P", 1)

    # project obstacle psi onto the current mesh
    psi_V = fe.project(psi, V)

    # Dirichlet 0 conditions
    y_out = fe.Constant(0.0)
    bcs = [fe.DirichletBC(V, y_out, meshdata.boundaries, i) for i in range(1, 5)]

    # define varational problem
    dx = fe.Measure('dx', domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    y_trial = fe.TrialFunction(V)
    v = fe.TestFunction(V)

    a = fe.inner(fe.grad(y_trial), fe.grad(v))*dx('everywhere')
    b = f1*v*dx(1) + f2*v*dx(2)

    # compute initial guess y_0 by not considering the obstacle
    y = fe.Function(V, name="state_sol")
    fe.solve(a == b, y, bcs)

    # define vectorized updates of slack variable lambda
    def lmbda_calc(state, obst, prev_lmbda):
        value = prev_lmbda + gamma*(state - obst)
        if value > fe.DOLFIN_EPS:
            return value
        else:
            return 0

    lmbda_calc_vec = np.vectorize(lmbda_calc, otypes=[np.float], cache=False)

    # define DoF-index set of inactive points for the obstacle, include lambda
    def active_set(lmbda, y):
        inds = np.zeros(len(lmbda.vector().get_local()))
        actives = np.where(lmbda.vector().get_local() + gamma*(y.vector().get_local() - psi_V.vector().get_local()) > 0.)
        inds[actives[0]] = 1.
        return inds

    lmbda = fe.Function(V)
    lmbda.vector()[:] = lmbda_calc_vec(y.vector().get_local(), psi_V.vector().get_local(), lmbda.vector().get_local())

    indicator_A = fe.Function(V)
    indicator_A_vals = active_set(lmbda, y)
    indicator_A.vector()[:] = indicator_A_vals

    # convergence condition || (y - psi)*Ind_A ||_L2 <= eps_tol
    abort_crit = fe.Function(V)
    abort_crit.vector()[:] = (1./gamma)*lmbda.vector().get_local()

#    lmbda_bar = fe.project(initialize_lambda_bar(parameters, meshdata, psi), V)

    i = 0.
    while fe.norm(abort_crit, 'L2', meshdata.mesh) > eps_tol_abs:
        # Iterative projecting onto set of variational inequality
        # respecting functions (K).
        if not obstacle:
            break

        if i == 0:
            lmbda = fe.Function(V)

        y_trial = fe.TrialFunction(V)
        v = fe.TestFunction(V)

        # compute new approximate solution
        a_loc = (fe.inner(fe.grad(y_trial), fe.grad(v)) + fe.inner(gamma*y_trial, indicator_A*v))*dx('everywhere')
        b_loc = f1*v*dx(1) + f2*v*dx(2) - fe.inner(lmbda, indicator_A*v)*dx('everywhere') + fe.inner(gamma*psi_V, indicator_A*v)*dx('everywhere')

        # compute solution
        y_loc = fe.Function(V)
        fe.solve(a_loc == b_loc, y_loc, bcs)

        # compute approximate dual variable lambda
        lmbda.vector()[:] = lmbda_calc_vec(y_loc.vector().get_local(), psi_V.vector().get_local(), lmbda.vector().get_local())

        # compute new indicator function for active set
        indicator_A_vals = active_set(lmbda, y_loc)
        indicator_A.vector()[:] = indicator_A_vals

        y.vector()[:] = y_loc.vector().get_local()

        # update convergence criterion
        abort_crit.vector()[:] = indicator_A_vals*(y.vector().get_local() - psi_V.vector().get_local())

#        print("VI semi-smooth Newton step:", str(i), str(fe.norm(lmbda, 'L2', meshdata.mesh)), str(fe.norm(abort_crit, 'L2', meshdata.mesh)))
        i += 1.
        if i > 300:
            break

    print(fe.norm(abort_crit, 'L2', meshdata.mesh))
    print("PDAS iterations state:", i-1)
    # Rueckgabe der Loesungen
    y.rename("state_sol", "label")

    return y


def solve_state_c_regu(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, psi: fe.Function) -> fe.Function:
    """
    Computes solution of the regularized, non-linear and unsmoothed state
    equation coming from the VI. The system if given by

    a(y, v) + max(0, lambda_bar + c_regu_VI*(y - vi_obstacle)) = source_term.

    Solver parameters can be adjusted below, where a preconditioned semi-smooth
    Newton method is used.


    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        psi: FEniCS expression or function of the variational inequality obstacle.


    Returns:
        y: FEniCS function of the solution to the regularized VI.
    """
    # parameters unpacking
    f1 = fe.Constant(float(parameters["shp_main"]["pde_source_term_1"]))
    f2 = fe.Constant(float(parameters["shp_main"]["pde_source_term_2"]))
    c_regu_VI = float(parameters["shp_VI"]["c_regu_VI"])
    eps_tol_abs = float(parameters["shp_VI"]["eps_tol_abs_VI"])
    eps_tol_rel = float(parameters["shp_VI"]["eps_tol_rel_VI"])

    V = fe.FunctionSpace(meshdata.mesh, "P", 1)

    v = fe.TestFunction(V)
    y = fe.Function(V)

    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)

    bcs = [fe.DirichletBC(V, 0., meshdata.boundaries, i) for i in range(1, 5)]
#    lmbda_bar = fe.project(initialize_lambda_bar(parameters, meshdata, psi), V, solver_type='cg', preconditioner_type='icc')
    lmbda_bar = fe.interpolate(initialize_lambda_bar(parameters, meshdata, psi), V)

    # define variational problem
    F = fe.inner(fe.grad(y), fe.grad(v))*dx + fe.max_value(0., lmbda_bar + c_regu_VI*(y - psi))*v*dx \
        - f1*v*dx(1) - f2*v*dx(2)

    # compute solution
    fe.solve(F == 0, y, bcs, solver_parameters={"newton_solver":
                                                {"linear_solver": "cg",
                                                 "preconditioner": "icc",
                                                 "absolute_tolerance": eps_tol_abs,
                                                 "relative_tolerance": eps_tol_rel,
                                                 "maximum_iterations": 50000
                                                 }})
    return y


def solve_adjoint_gamma_c(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, y: fe.Function, z: fe.Function, psi: fe.Function) -> tp.Tuple[fe.Function, fe.Function]:
    """
    Solves the adjoint equation to the regularized and smoothed problem coming
    from the variational inequality state equation. It is given by

    a(y, v) + sign_gamma(lambda_bar + c(y-psi))*p*v*dx = -(y-z)*v*dx.

    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        y: State equation solution on meshdata of the smoothed and regularized
            variational inequality. Return of solve_state_gamma_regu.
        z: Projection of FEniCS function representing target data onto meshdata.
        psi: FEniCS expression or function of the variational inequality obstacle.


    Returns:
        (p_cgamma, sign_gamma)
        p_cgamma: FEniCS function representing the adjoint solution.
        sign_gamma: FEniCS function representing the zero order term coming
            from the non-linearity of the smoothed and regularized VI.
    """
    # parameters unpacking
    c = float(parameters["shp_VI"]["c_regu_VI"])
    gamma = float(parameters["shp_VI"]["gamma_regu_VI"])

    dx = fe.Measure("dx", domain=meshdata.mesh, subdomain_data=meshdata.subdomains)
    V = fe.FunctionSpace(meshdata.mesh, "CG", 1)
    bcs = [fe.DirichletBC(V, fe.Constant(0.), meshdata.boundaries, i) for i in range(1, 5)]

    p = fe.TrialFunction(V)
    v = fe.TestFunction(V)
    psi_V = fe.project(psi, V)
    lambda_bar = initialize_lambda_bar(parameters, meshdata, psi)
    affine_comb = fe.project(lambda_bar + c*(y - psi_V), V)
    sign_gamma = fe.conditional(fe.le(affine_comb, -(1./gamma)), 0.,
                                fe.conditional(fe.lt(affine_comb, (1./gamma)), 0.5*gamma*affine_comb + 0.5, 1.)
                                )
    sign_gamma = fe.project(sign_gamma, V)

    lhs = fe.inner(fe.grad(p), fe.grad(v))*dx + c*sign_gamma*p*v*dx
    rhs = -(y-z)*v*dx

    p_cgamma = fe.Function(V)

    fe.solve(lhs == rhs, p_cgamma, bcs)

    return p_cgamma, sign_gamma


def shape_deriv_gamma_c(parameters: tp.Dict[str, tp.Dict[str, str]], meshdata: MeshData, p: fe.Function, y: fe.Function, z: fe.Function, psi: fe.Function, V: fe.dolfin.Argument) -> tp.Tuple[ufl.Form, ufl.Form]:
    """
    Computes the shape derivative to the smoothly regularized version of the
    VI constrained tracking type shape optimization problem with perimeter
    regularization.

    Inputs:
        parameters: Dictionary of parameter values extracted from config.ini.
        meshdata: Instance of MeshData class with shape (tags: {5, 6}).
        p: FEniCS function of the solution to the smoothly regularized VI.
            Return of solve_adjoint_gamma_c.
        y: State equation solution on meshdata of the smoothed and regularized
            variational inequality. Return of solve_state_gamma_regu.
        z: Projection of FEniCS function representing target data onto meshdata.
        psi: FEniCS expression or function of the variational inequality obstacle.
        V: FEniCS test function to assemble shape derivative.


    Returns:
        (fe.Constant(alpha_main)*Dj, Dj_reg)
        fe.Constant(alpha_main)*Dj: ufl form of shape derivative of tracking type functional.
        Dj_reg: ufl form of shape derivative of perimeter regularization.
    """
    # parameters unpacking
    f1 = fe.Constant(float(parameters["shp_main"]["pde_source_term_1"]))
    f2 = fe.Constant(float(parameters["shp_main"]["pde_source_term_2"]))
    c = float(parameters["shp_VI"]["c_regu_VI"])
    gamma = float(parameters["shp_VI"]["gamma_regu_VI"])
    alpha_main = float(parameters["prshp_main"]["alpha_main"])
    perimeter_regu = float(parameters["shp_main"]["perimeter_regu"])

    dx = fe.Measure('dx', domain=meshdata.mesh, subdomain_data=meshdata.subdomains)
    dS = fe.Measure('dS', subdomain_data=meshdata.boundaries)

    V_space = fe.FunctionSpace(meshdata.mesh, "CG", 1)

    epsilon_V = fe.sym(fe.nabla_grad(V))
    n = fe.FacetNormal(meshdata.mesh)
    psi_V = fe.project(psi, V_space)
    lmbda_bar = initialize_lambda_bar(parameters, meshdata, psi)

    # compute max_gamma and sign_gamma
    affine_comb = lmbda_bar + c*(y-psi)
    max_gamma = fe.conditional(fe.le(affine_comb, -(1./gamma)), 0,
                               fe.conditional(fe.lt(affine_comb, (1./gamma)), 0.25*gamma*(affine_comb)**2
                                              + 0.5*affine_comb + 1./(4.*gamma), affine_comb)
                               )
    max_gamma = fe.project(max_gamma, V_space)
    sign_gamma = fe.conditional(fe.le(affine_comb, -(1./gamma)), 0.,
                                fe.conditional(fe.lt(affine_comb, (1./gamma)), 0.5*gamma*affine_comb + 0.5, 1.)
                                )
    sign_gamma = fe.project(sign_gamma, V_space)

    # Gradient f is 0, since f is piecewise constant
    Dj = (-(y-z)*fe.inner(V, fe.grad(z)) - fe.inner(fe.grad(y), fe.dot(epsilon_V, fe.grad(p))))*dx('everywhere') \
        + sign_gamma*p*fe.inner(fe.grad(lmbda_bar) - c*fe.grad(psi_V), V)*dx \
        - fe.nabla_div(V)*f1*p*dx(1) - fe.nabla_div(V)*f2*p*dx(2) \
        + fe.nabla_div(V)*(0.5*(y-z)*(y-z) + fe.inner(fe.grad(y), fe.grad(p)) + max_gamma*p)*dx

    Dj_reg = fe.Constant(perimeter_regu)*((fe.nabla_div(V('+'))
                                          - fe.inner(fe.dot(fe.nabla_grad(V('+')), n('+')), n('+')))*dS(5)
                                          + (fe.nabla_div(V('+')) - fe.inner(fe.dot(fe.nabla_grad(V('+')), n('+')), n('+')))*dS(6)
                                          )

    return fe.Constant(alpha_main)*Dj, Dj_reg
